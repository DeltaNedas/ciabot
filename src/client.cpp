#include <client.h>

#include <stdio.h>
#include <stdlib.h>

#include <console.h>
#include <elections.h>
#include <events.h>
#include <feeds.h>
#include <filters.h>
#include <info.h>
#include <message.h>
#include <parser.h>
#include <permissions.h>
#include <powerline.h>
#include <userdata.h>
#include <util.h>
#include <util/generics.h>
#include <util/net.h>
#include <util/strings.h>

namespace CIABot {
	void Client::onReady(AwokenDiscord::Ready readyData) {
		this->offline = false;
		if (console != nullptr) {
			console->start();
		}
		if (tasks != nullptr) {
			tasks->start();
		}

		printInfo(InfoLevel::START, "[%sSTART%s]\t%s► CIABOT IS NOW READY ◄%s\n",
			Colours::YELLOW, Colours::RESET, Colours::BOLD, Colours::RESET);
	}

	void Client::onMessage(AwokenDiscord::Message message) {
		messages.set(message);
		if (message.author.ID != this->getID()) {
			std::string user(message.author.ID);
			printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tMessage sent from %s%s%s (%s%s%s) in %s/%s: %s%s%s\n",
				Colours::GREEN, Colours::RESET,
				Colours::YELLOW, user.c_str(), Colours::RESET,
				Colours::GREEN, message.author.username.c_str(), Colours::RESET,
				std::string(message.serverID).c_str(), std::string(message.channelID).c_str(),
				Colours::BOLD, json(message).dump(1, '\t').c_str(), Colours::RESET);
			size_t old = timestamp();
			bool finished = false;
			int code = 0;
			size_t commandStart = 0;

			UserData userData(&message, false);
			std::map<std::string, Filter*> serverFilters = filters.get(userData.server);

			size_t filtersStart = timestamp();
			for (auto& it : serverFilters) {
				Filter* filter = it.second;
				if (filter != nullptr) {
					if (filter->process(message, userData)) {
						break;
					}
				}
			}
			printInfo(InfoLevel::MESSAGE | InfoLevel::TIME, "[%sMESSAGE.TIME%s]\t-> Filters took %luμs to process.\n",
				Colours::GREEN, Colours::RESET, (timestamp() - filtersStart) / (size_t) 1e3);

			if (!message.content.empty()) {
				commandStart = timestamp();
				code = parseCommand(message, userData, &finished);

				if (!code) {
					while (!finished) {
						sleep(1);
					}
					printInfo(InfoLevel::COMMAND | InfoLevel::TIME, "[%sMESSAGE.TIME%s]\t-> Command took %luμs to process.\n",
						Colours::GREEN, Colours::RESET,  (timestamp() - commandStart) / (size_t) 1e3);
				}
			}
			printInfo(InfoLevel::MESSAGE | InfoLevel::TIME, "[%sMESSAGE.TIME%s]\tMessage took %luμs to process.\n",
				Colours::GREEN, Colours::RESET, (timestamp() - old) / (size_t) 1e3);
		}
	}

	void Client::onEditMessage(AwokenDiscord::MessageRevisions revisions) {
		AwokenDiscord::Message messageObj = this->getMessage(revisions.channelID, revisions.messageID);
		Message* message = messages.set(messageObj);
		if (messageObj.author.ID != this->getID()) {
			printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tMessage %s by %s was edited to %s%s%s\n",
				Colours::GREEN, Colours::RESET,
				std::string(revisions.messageID).c_str(), message->user.c_str(),
				Colours::GREEN, messageObj.content.c_str(), Colours::RESET);
		}
	}

	void Client::onDeleteMessages(AwokenDiscord::Snowflake<AwokenDiscord::Channel> channelID,
		std::vector<AwokenDiscord::Snowflake<AwokenDiscord::Message>> messages) {
		std::string channel(channelID);
		auto registered = CIABot::messages.get(channel);
		for (auto message : messages) {
			std::string id(message);
			Message* m = CIABot::messages.get(id, registered);
			std::string content = "<unknown content>";

			if (m != nullptr) {
				if (!m->history.empty()) {
					MessageHistory* history = m->history[m->history.size() - 1];
					if (history != nullptr) {
						content = history->content;
					}
				}

				if (m->user != std::string(this->getID())) {
					printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tMessage %s by %s was deleted: %s%s%s\n",
						Colours::GREEN, Colours::RESET, id.c_str(), m->user.c_str(), Colours::GREEN,
						content.c_str(), Colours::RESET);
				}
				for (std::string response : m->responses) {
					try {
						CIABot::deleteMessage(response, channel);
						printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tResponse %s deleted.\n",
							Colours::GREEN, Colours::RESET, response.c_str());
					} catch (AwokenDiscord::ErrorCode& e) {}
				}
				delete m;
				registered.erase(id);
			} else {
				printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tMessage %s was deleted.\n",
					Colours::GREEN, Colours::RESET, id.c_str());
			}
		}
		CIABot::messages.set(registered, channel);
	}


	void Client::onServer(AwokenDiscord::Server server) {
		printInfo(InfoLevel::START, "[%sSTART%s]\tFound server %s (%s).\n",
			Colours::YELLOW, Colours::RESET, server.name.c_str(), std::string(server.ID).c_str());
		this->servers[server.ID] = Cached(server);
		//CIABot::updateStatus();
		std::string owner(server.ownerID);
		auto levels = executionLevels.get(server.ID, false);
		auto it = levels.find(owner);
		if (it == levels.end()) {
			AwokenDiscord::User user;
			if (CIABot::getUser(owner, &user)) {
				levels[owner] = OWNER_EXECUTION_LEVEL;
				executionLevels.set(levels, server.ID);

				AwokenDiscord::Embed embed;
				embed.title = "🎉 Welcome to **CIABot**! 🎉";
				embed.color = Colours::Discord::DELTA;
				std::string prefix = prefixes.get(server.ID);
				std::vector<AwokenDiscord::EmbedField> fields = {};
				addField(fields, "About Me", "Thanks for adding me to **" + server.name + "**!\nI can help you out in many ways like with message moderation, an extensive permission system and various fun commands.");
				addField(fields, "Getting Started", replace(R"(You can use `$help` followed by a command for more info on it.
For example, if you want to look at information on `filters`, use `$help filters`.
You can list all permissions with `$permissions`.
You can then modify specific permissions, like `$permissions set help.run
none
0
0
10`, which will make `$help`'s output be deleted after 10 seconds.
Play around and see what you can do. :))", "\\$", prefix));
				addField(fields, "Extra Help", replace(R"(If `$help` doesn't contain enough information, feel free to ask my developer a question with `$question`.)", "\\$", prefix));
				embed.fields = fields;
				CIABot::sendMessage(owner, json({{"embed", embed}}));
				printInfo(InfoLevel::START, "[%sSTART%s]\tWelcomed %s's owner, %s (%s).\n",
					Colours::YELLOW, Colours::RESET,
					server.name.c_str(), user.username.c_str(), owner.c_str());
			}
		}
	}

	void Client::onChannel(AwokenDiscord::Channel channel) {
		this->channels[channel.ID] = Cached(channel);
	}

	void Client::onMember(AwokenDiscord::Snowflake<AwokenDiscord::Server> serverID, AwokenDiscord::ServerMember member) {
		std::string server(serverID);
		std::string user(member.user.ID);
		auto map = get(this->members, server);
		CIABot::ServerMember structure = {server, member};
		Cached cached(structure);
		map[user] = cached;


		this->members[server] = map;
		// Insert member sorted by join position, owner should be sortedMembers[server][0]
		this->sortedMembers[server] = insertSorted(get(this->sortedMembers, server), &cached, [](auto& lhs, auto& rhs){
			return dateToUnix(lhs->get(-1).member.joinedAt) > dateToUnix(rhs->get(-1).member.joinedAt);
		});
	}

	void Client::onUser(AwokenDiscord::User user) {
		this->users[user.ID] = Cached(user);
	}

	void Client::onRemoveMember(AwokenDiscord::Snowflake<AwokenDiscord::Server> serverID, AwokenDiscord::User user) {
		/*std::string server(serverID);
		std::string id(user.ID);
		auto it = this->members.find(server);
		if (it != this->members.end()) {
			it->second.erase(id);
		}

		size_t deb = 0;
		printf("%lu\n", deb++);
		auto sortedIt = this->sortedMembers.find(server);
		if (sortedIt != this->sortedMembers.end()) {
			pritnf("%lu\n", deb++);
			auto members = sortedIt->second;
			for (size_t i = 0; i < this->sortedMembers.size(); i++) {
				if (members[i]->get(-1).member.user.ID == user.ID) { // Ignore cache because users cant change IDs
					members.erase(members.begin() + i);
					this->sortedMembers[server] = members;
					break;
				}
			}
		}*/
	}

	void Client::onReaction(AwokenDiscord::Snowflake<AwokenDiscord::User> userID, AwokenDiscord::Snowflake<AwokenDiscord::Channel> channelID, AwokenDiscord::Snowflake<AwokenDiscord::Message> messageID, AwokenDiscord::Emoji emoji) {
		if (userID != this->getID()) {
			Message* m = messages.get(messageID, channelID);
			std::string user(userID);
			// Delete message when creator, or ring-2 user, reacts with "delete" emojis
			if (compare(emoji.name, {"🗑️", "❌"})) {
				AwokenDiscord::Message message;
				if (CIABot::getMessage(messageID, channelID, &message) && message.author.ID == this->getID()) {
					UserData userData(&message);
					userData.user = user;
					userData.rupees = rupees.get(user, (m == nullptr) ? "" : m->server);
					userData.executionLevel = executionLevels.get(user, userData.server);
					if (permissions.has("delete.from_reaction", userData, "", true) || (m != nullptr && m->creator == user)) {
						CIABot::deleteMessage(messageID, channelID);
					}
				}
			} else if (m != nullptr) {
				// Handle election votes.
				AwokenDiscord::Channel channel;
				if (!CIABot::getChannel(channelID, &channel)) {
					return;
				}

				std::map<std::string, Election*> elecs = elections.get(channel.serverID);
				std::string mID(messageID);
				Election* e = nullptr;
				for (auto& it : elecs) {
					if (it.second->message == mID) {
						e = it.second;
						break;
					}
				}

				if (e != nullptr) {
					UserData userData;
					userData.user = user;
					userData.rupees = rupees.get(user, m->server);
					userData.executionLevel = executionLevels.get(user, m->server);
					if (e->canVote() /*&& permissions.has("elections." + e->name + ".vote", userData, "", false)*/) {
						// Users may vote once, and only once!
						if (!contains(user, e->voters)) {
							for (auto& it : e->candidates) {
								ElectionCandidate c = it.second;
								if (compare(c.emoji, {emoji.name, emoji.ID})) { // If emoji is the same custom or built in reaction
									printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\t%s in %s voted for %s (%s) in election %s.\n",
										Colours::CYAN, Colours::RESET,
										user.c_str(), m->server.c_str(), c.emoji.c_str(), c.name.c_str(), e->name.c_str());
									e->voters.push_back(user);
									e->candidates[it.first].votes++;

									AwokenDiscord::Embed pm;
									pm.title = "⚖️ Vote locked!";
									pm.description = "> " + e->title + "\n\nYou voted for **" + c.name + "** (" + emojiToString(c.emoji) + ")";
									CIABot::sendMessage(user, json({{"embed", pm}})); // DM their vote
									break;
								}
							}
						}
						try {
							this->removeReaction(channelID, messageID, emojiToString(emoji), userID);
						} catch (AwokenDiscord::ErrorCode& ignored) {}
					}
				}
			}
		}
	}

	size_t Client::ping(size_t times) {
		size_t delay = 0;
		for (size_t i = 0; i < times; i++) {
			size_t old = timestamp();
			downloadFile("gateway.discordapp.com/?v=6"); // Measure network latency only, not API delay like heartbeat.
			delay += (timestamp() - old);
		}
		return delay / times;
	}

	Client* client = nullptr;
	size_t Client::startedAt = 0;
	std::string rootPath = "";

	void startClient(bool createConsole, bool offline) {
		loadDefaultPermissions();
		if (createConsole) {
			const char* usernameC = getenv("USER");
			if (usernameC == NULL) { // If USER is not set in environment
				usernameC = "CIA";
			}
			console = new Console(generateStatus(std::string(usernameC)));
		}
		tasks = new Tasks();
		if (!offline) {
			tasks->add(feedsLoop, FEED_CHECK_INTERVAL);
		}

		{
			std::string token = tokens.get("discord");
			if (token.empty()) {
				fprintf(stderr, "[%sERROR%s]\tInvalid tokens.json file!\n",
					Colours::RED, Colours::RESET);
				quit(1);
			}

			printInfo(InfoLevel::START, "[%sSTART%s]\t%s► CIABOT IS STARTING%s... ◄%s\n",
				Colours::YELLOW, Colours::RESET, Colours::BOLD,
				offline ? " IN OFFLINE MODE" : "",
				Colours::RESET);
			Client::startedAt = timestamp();

			if (offline) {
				client = new Client();
			} else {
				client = new Client(token.c_str(), AwokenDiscord::USER_CONTROLLED_THREADS);
			}
		}; // Keep token out of core dumps and stuff

		client->debugLevel = (infoLevel & InfoLevel::DISCORD) ? -1 : 0;
		if (offline) {
			AwokenDiscord::Ready ready;
			ready.v = 6; // Invalid gateway obv
			client->onReady(ready);
		} else {
			client->run();
		}
	}
}
