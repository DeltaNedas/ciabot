#include <powerline.h>

#include <options.h>

namespace CIABot {
	std::string generateStatus(std::string username) {
	#if USE_POWERLINE
		// Powerline-style status, requires a powerline glyph font.
		return "[38;5;250m[48;5;240m " + username + " [48;5;238m[38;5;240m[38;5;250m[48;5;238m CIABot [48;5;166m[38;5;238m[38;5;15m[48;5;166m CLI [48;5;236m[38;5;166m[38;5;15m[48;5;236m $ [0m[38;5;236m[0m";
	#else
		return username + "@CIABot CLI $";
	#endif
	}
}