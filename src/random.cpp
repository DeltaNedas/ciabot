#include <random.h>
#include <random>

namespace CIABot {
	std::random_device randomDevice; // read from /dev/random to set the seed
	std::mt19937 randomEngine(randomDevice()); // seed the generator

	long randomInt(long min, long max) {
		std::uniform_int_distribution<> distribution(min, max);
		return distribution(randomEngine);
	}
}