#include <filters.h>

#include <events.h>
#include <info.h>
#include <permissions.h>
#include <util/json.h>
#include <util/net.h>
#include <util/strings.h>

namespace CIABot {
	Filter::Filter(std::string input, std::string output, unsigned char flags, std::string channel, std::string urlInput, std::string urlOutput) {
		this->input = input;
		try {
			this->regex = std::regex(input);
		} catch (std::exception& e) {
			this->input = "invalid regex";
			this->regex = std::regex(this->input);
		}
		this->output = output;
		this->flags = flags;
		this->channel = channel;
		this->urlInput = urlInput;
		this->urlOutput = urlOutput;
		try {
			this->urlRegex = std::regex(urlInput);
		} catch (std::exception& e) {
			this->urlInput = "invalid regex";
			this->urlRegex = std::regex(this->urlInput);
		}
	}

	bool Filter::process(const AwokenDiscord::Message& message, UserData userData) {
		std::string permissionName = "filters." + this->name + ".trigger";
		if (!userData.fake && permissions.has(permissionName, userData, "", false)) {
			auto user = message.author;
			bool bot = user.bot;
			bool fBot = this->flags & FilterFlags::BOT;
			bool fUser = this->flags & FilterFlags::USER;
			if ((fBot && !bot) && (fUser && bot)) {
				return false;
			}

			std::smatch matched;
			json content = serialiseMessage(message);
			std::string matching = this->replaceInput(content.dump());
			if (std::regex_search(matching, matched, this->regex)) {
				// TODO
				if (this->flags & FilterFlags::FORCE_ASCII || this->flags & FilterFlags::FORCE_LOWER) {

				}
				printInfo(InfoLevel::FILTER, "[%sFILTER%s]\tFilter %s matched with %s%s%s!\n",
					Colours::YELLOW, Colours::RESET,
					this->name.c_str(),
					Colours::BOLD, content.dump(1, '\t').c_str(), Colours::RESET);
				if (this->flags & FilterFlags::DELETE) {
					deleteMessage(message.ID, message.channelID);
				}

				permissions.has(permissionName, userData, "", true); // Add any bonuses
				Permission* perm = permissions.get(permissionName, userData.server, false);

				if (perm) {
					if (perm->getCooldown(user.ID) > 0) {
						return false;
					}
					perm->setCooldown(user.ID);
				}


				if (this->output.size()) {
					std::string channelID = this->channel;
					if (channelID.empty()) {
						channelID = userData.channel;
					}

					std::string newOutput = this->replaceOutput(this->output, matched, user, userData.server, userData.rupees);

					if (this->urlInput.size()) {
						std::string url = newOutput;
						std::string data = "";
						int code = downloadFile(url, &data);
						if (code) {
							newOutput = "";
						} else {
							std::smatch urlMatched;
							if (std::regex_search(data, urlMatched, this->urlRegex)) {
								newOutput = replaceOutput(this->urlOutput, urlMatched, user, userData.server, userData.rupees);
							}
						}
					}

					std::string response = "";
					std::string creator = message.ID;
					//newOutput = replace(newOutput, "\\\\n", "\n");
					if (this->flags & FilterFlags::DELETE) {
						response = "";
					}

					response = sendMessage(channelID, newOutput, response);
					if (perm != nullptr) {
						if (!response.empty() && perm->deleteAfter) {
							tasks->add([=](size_t delta){
								deleteMessage(response, channelID);
							}, perm->deleteAfter);
						}
					}
					return true;
				}
				return true;
			} else {
				printInfo(InfoLevel::FILTER, "\n");
			}
		}
		return false;
	}

	std::string Filter::replaceOutput(std::string raw, std::smatch matched, const AwokenDiscord::User& user, std::string server, rupees_t rupeesBefore) {
		for (size_t i = 0; i < matched.size(); i++) {
			raw = replace(raw, "\\{" + std::to_string(i) + "\\}", matched[i].str());
		}

		std::string id(user.ID);
		return replace(raw, {
			{"\\{ID\\}", id},
			{"\\{PING\\}", "<@" + id + ">"},
			{"\\{USERNAME\\}", user.username},
			{"\\{DISCRIMINATOR\\}", user.discriminator},
			{"\\{RUPEES\\}", std::to_string(rupeesBefore)},
			{"\\{RUPEES_AFTER\\}", std::to_string(rupees.get(id, server))},
			{"\\{TIME\\}", timestampStr()}
		});
	}

	std::string Filter::replaceInput(std::string input) {
		if (this->flags & FilterFlags::FORCE_ASCII) {
			input = toAscii(input);
		}
		if (this->flags & FilterFlags::FORCE_LOWER) {
			input = lower(input);
		}
		return input;
	}
}
