#include <console.h>
#include <awoken_discord/embed.h>

#include <stdio.h>

#include <client.h>
#include <options.h>
#include <util/strings.h>

namespace CIABot {
	Console::Console(std::string status) {
		this->status = status;
	}

	bool Console::start() {
		if (this->running || !this->closed) {
			return false;
		}
		this->closed = false;

		deleteThread(consoleThread);
		consoleThread = new std::thread(consoleLoop, this);
		if (client->offline) {
			consoleThread->join(); // Client will not run in offline mode, so console is needed.
		} else {
			consoleThread->detach();
		}
		return true;
	}

	bool Console::stop() {
		if (!this->running || this->closed) {
			return false;
		}
		this->closed = true;
		// TODO: Use non blocking reads for console
		/* Add EOF to stdin instead, this needs an enter press
		while (this->running) {
			sleep(10);
		} */
		this->running = false;
		deleteThread(consoleThread);
		return true;
	}

	bool Console::parse(std::string input) {
		if (input.rfind(CONSOLE_COMMAND_PREFIX, 0) == 0) {
			input = input.substr(strlen(CONSOLE_COMMAND_PREFIX));
			std::vector<std::string> args = split(input);
			std::string command = args[0];

			if (compare(command, {"quit", "exit", "q", "e"})) {
				printf(">> %sCIABot can only be controlled with ^C and in-server commands now.%s\n",
					Colours::BLUE, Colours::RESET);
				return false;
			} else if (command == "shutdown") {
				if (args.size() > 0) {
					try {
						sleep(stoi(args[0]) * 1000);
					} catch (std::exception& e) {}

					this->running = false;
					quit(0);
				}
			} else if (command == "pwd") {
				printf(">> Current channel: %s%s%s/%s#%s%s\n>> Current user: %s%s#%s%s\n",
					Colours::GREEN, this->server.name.c_str(), Colours::RESET,
					Colours::BLUE, this->channel.name.c_str(), Colours::RESET,
					Colours::YELLOW, this->user.username.c_str(), this->user.discriminator.c_str(), Colours::RESET);
			} else if (command == "cd") {
				if (args.size() < 2) {
					fprintf(stderr, "!> %sRun cd with a channel id.%s\n",
						Colours::RED, Colours::RESET);
					return true;
				}
				std::string id = args[1];
				if (!(getChannel(id, &this->channel) && getServer(std::string(this->channel.serverID), &this->server))) {
					fprintf(stderr, "!> %sInvalid channel.%s\n",
						Colours::RED, Colours::RESET);
					return true;
				}
				this->userData = UserData("", this->user.ID, this->channel.ID, this->server.ID, true);
			} else if (command == "su") {
				if (args.size() < 2) {
					fprintf(stderr, "!> %sRun su with a user id.%s\n",
						Colours::RED, Colours::RESET);
					return true;
				}

				std::string id = args[1];
				if (!getUser(id, &this->user)) {
					fprintf(stderr, "!> %sInvalid user.%s\n",
						Colours::RED, Colours::RESET);
					return true;
				}
				this->userData = UserData("", this->user.ID, this->channel.ID, this->server.ID, true);
			} else if (compare(command, {"ls", "list"})) {
				if (args.size() > 1) {
					std::string id = args[1];
					for (auto& it : client->servers) {
						auto cached = it.second; cached.value.ID = it.first;
						auto server = cached.get();
						if (std::string(server.ID) == id) {
							printf(">> Available channels in %s:\n", server.name.c_str());
							for (AwokenDiscord::Channel channel : server.channels) {
								printf("\t%s%s - #%s%s\n",
									Colours::GREEN, std::string(channel.ID).c_str(), channel.name.c_str(), Colours::RESET);
							}
							return true;
						}
					}
					printf("!> %sInvalid server.%s\n",
						Colours::RED, Colours::RESET);
				} else {
					printf(">> Available servers:\n");
					for (auto& it : client->servers) {
						auto cached = it.second; cached.value.ID = it.first;
						auto server = cached.get();
						printf("\t%s%s - %s%s\n",
							Colours::GREEN, std::string(server.ID).c_str(), server.name.c_str(), Colours::RESET);
					}
				}
			} else if (command == "info") {
				if (args.size() == 1) {
					printf(">> Info level is currently %d.\n", infoLevel);
					return true;
				}

				try {
					infoLevel = (unsigned short) stoi(args[1]);
				} catch (std::exception& e) {
					fprintf(stderr, "!> Info level must be a number!\n");
					return true;
				}
			} else if (compare(command, {"help", "man", "?"})) {
				printf(">> Available commands:\n" \
				"\t%shelp/man/?%s - show this info\n" \
				"\t%squit/exit%s - quit the console, same as %s^D%s\n" \
				"\t%sshutdown %s[secs = 0]%s - shut down CIABot after seconds, same as %s^C%s\n" \
				"\t%spwd%s - prints your working channel, server and user\n" \
				"\t%scd %s<channel id>%s - change working channel and server\n" \
				"\t%ssu %s<user id>%s - change working user\n" \
				"\t%sls/list %s[server id = list servers]%s - list available servers or channels\n" \
				"\t%sinfo %s[info level = get]%s - set the info level, or print it\n",
					Colours::BOLD, Colours::RESET,
					Colours::BOLD, Colours::RESET, Colours::BOLD, Colours::RESET,
					Colours::BOLD, Colours::CYAN, Colours::RESET, Colours::BOLD, Colours::RESET,
					Colours::BOLD, Colours::RESET,
					Colours::BOLD, Colours::YELLOW, Colours::RESET,
					Colours::BOLD, Colours::YELLOW, Colours::RESET,
					Colours::BOLD, Colours::CYAN, Colours::RESET,
					Colours::BOLD, Colours::CYAN, Colours::RESET);
				printf(">> Console commands are ran with a prefix of %s#%s.\n",
					Colours::BOLD, Colours::RESET);
				printf(">> Normal commands are ran without any prefix.\n");
			}
		} else {
			int returnCode = 0;

			bool finished = false;
			returnCode = parseCommand(prefixes.get(this->server.ID) + input, this->userData, AwokenDiscord::Message(), &finished);
			if (!returnCode) { // Command wont finish if it doesn't start!
				while (!finished) {
					sleep(10);
				}
			}

			switch(returnCode) {
				case 0: case 4:
					// Output is done by the parser
					break;
				case 2:
					printf(">> %sUnknown command, use $help or #help.%s\n",
						Colours::RED, Colours::RESET);
					break;
				default: // 3
					printf(">> %sYou cannot run that command.%s\n",
						Colours::RED, Colours::RESET);
					break;
			}
		}
		return true;
	}


	void consoleLoop(Console* console) {
		console->running = true;

		if (!(getChannel("607705335553327117", &console->channel, 0) &&
			getServer("607703734339895315", &console->server, 0) &&
			getUser("542742543478292480", &console->user)), 0) {
			fprintf(stderr, "!> %sFailed to get default working channel or user!%s\n",
				Colours::RED, Colours::RESET);
		}
		console->userData = UserData("", console->user.ID, console->channel.ID, console->server.ID, true);

		printf(">> %sWelcome to CIABot CLI! Use #help for more info.%s\n",
			Colours::YELLOW, Colours::RESET);
		while (true) {
			printf("%s ", console->status.c_str());
			fflush(stdout); // Don't mess up order of printed messages

			if (console->closed) {
				console->closed = false;
				break;
			}

			std::string input;
			std::getline(std::cin, input);

			if (std::cin.eof()) { // ^D
				printf("\n>> %sCIABot can only be controlled with ^C and in-server commands now.%s\n",
					Colours::BLUE, Colours::RESET);
				break;
			}

			if (input.empty()) {
				continue;
			}

			if (!console->parse(input)) {
				break;
			}
		}
		console->running = false;
	}

	Console* console = nullptr;
}