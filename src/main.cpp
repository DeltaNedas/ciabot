#include <libgen.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdexcept>
#include <filesystem>

#include <client.h>
#include <commands.h>
#include <console.h>
#include <colours.h>
#include <events.h>
#include <feeds.h>
#include <info.h>
#include <powerline.h>
#include <threads.h>
#include <util/strings.h>
#include <version.h>

using namespace CIABot;

namespace fs = std::filesystem;

int main(int argc, char* argv[]) {
	if (signal(SIGINT, handleSignal) == SIG_ERR
		|| signal(SIGABRT, handleSignal) == SIG_ERR
		/*|| signal(SIGSEGV, handleError) == SIG_ERR*/) {
		fprintf(stderr, "%sCan't handle signals!%s\n", Colours::RED, Colours::RESET);
		return 1;
	}

	bool createConsole = true;
	bool offline = false;

	if (argc > 1) {
		size_t skip = 0;
		for (int i = 1; i < argc; i++) {
			if (skip) {
				skip--;
				continue;
			}

			std::string arg(argv[i]);
#if COMPILE_INFO_MESSAGES
			if (compare(arg, {"-i", "--info-level"})) {
				if (i == argc - 1) {
					fprintf(stderr, "[%sERROR%s]\tExpected an info level number after argument #%d (%s)!\n",
						Colours::RED, Colours::RESET, i, arg.c_str());
					return EINVAL;
				}

				try {
					infoLevel = (unsigned short) std::stoi(argv[i + 1]);
				} catch (std::exception& e) {
					fprintf(stderr, "[%sERROR%s]\tInfo level for argument #%d (%s) is not a number!\n",
						Colours::RED, Colours::RESET, i, argv[i + 1]);
				}

				skip++;
			} else
#endif
			if (compare(arg, {"-r", "--root-path"})) {
				if (i == argc - 1) {
					fprintf(stderr, "[%sERROR%s]\tExpected a directory after argument #%d (%s)!\n",
						Colours::RED, Colours::RESET, i, arg.c_str());
					return EINVAL;
				}

				rootPath = std::string(argv[i + 1]);
				skip++;
			} else if (compare(arg, {"--no-console"})) {
				createConsole = false;
			} else if (compare(arg, {"-O", "--offline"})) {
				offline = true;
			} else if (compare(arg, {"-h", "-?", "--help"})) {
				std::string helpMessage = R"(CIABot %s is a utility bot for Discord, written in C++.
Data is stored in JSON files in the config directory where it's ran from.
Usage: %s [options]
Options for running are as follows:
%s-h/-?/--help%s: Prints this text.
%s-r/--root-path <path>%s: Sets the root directory of CIABot, where config files are stored and modules are loaded from.
	Defaults to the executable's path name.
%s--copyright%s: Print copyright information
%s--no-console%s: Stops the CIABot CLI from starting
%s-O/--offline%s: Removes most functionality, requires no connection to discord.
	Incompatible with %s--no-console%s.
)";
#if COMPILE_INFO_MESSAGES
				helpMessage += R"(%s-i/--info-level%s <number>: Sets the info level to a number.
	The info level is made up of binary numbers.
	Add the levels you want for a final number:
	-> 0   : No extra messages
	-> 1   : Data loading messages
	-> 2   : Data saving messages
	-> 4   : Data modification messages
	-> 8   : Bot start messages
	-> 16  : Bot stop messages
	-> 32  : Discord messages sended/received
	-> 64  : Time things take, needs others like 32 or 1.
	-> 128 : Commands being ran
	-> 256 : Module-related messages
	-> 512 : Feed checking messages
	-> 1024: Questions being asked
	-> 2048: Filters being processed
	-> 4096: Caches being updated
	-> 8192: Discord library debug info

	Default is 3 for loading and saving messages.
	Set to -1 for all messages.
)";
#endif
				helpMessage += R"(%sCIABot Copyright (C) DeltaNedas 2019%s
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/
)";
				printf(helpMessage.c_str(), version.c_str(), argv[0],
#if COMPILE_INFO_MESSAGES
					Colours::BLUE, Colours::RESET,
#endif
					Colours::BLUE, Colours::RESET,
					Colours::BLUE, Colours::RESET,
					Colours::BLUE, Colours::RESET,
					Colours::BLUE, Colours::RESET,
					Colours::BLUE, Colours::RESET, Colours::BOLD, Colours::RESET,
					Colours::GREEN, Colours::RESET);
				return 0;
			} else if (compare(arg, {"--copyright"})) {
				printf(R"(%sCIABot Copyright (C) DeltaNedas 2019%s
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.
)", Colours::GREEN, Colours::RESET);
				return 0;
			} else {
				fprintf(stderr, "[%sERROR%s]\tInvalid argument \"%s\", see %s --help.\n",
					Colours::RED, Colours::RESET, argv[i], argv[0]);
				return EINVAL;
			}
		}
	}

	if (offline && !createConsole) {
		fprintf(stderr, "[%sERROR%s]\tOffline requires a console.\n",
			Colours::RED, Colours::RESET);
		return EINVAL;
	}

	if (rootPath.empty()) {
		char buffer[PATH_MAX] = ".";
		if (readlink("/proc/self/exe", buffer, sizeof(buffer) - 1) == -1) {
			fprintf(stderr, "[%sWARN%s]\tFalling back to current directory for executable path.\n",
				Colours::YELLOW, Colours::RESET);
			rootPath = "."; // Fallback to current directory, requires `cd build; ./ciabot` to function properly
		} else {
			rootPath = std::string(dirname(buffer));
		}
	}

	if (rootPath[rootPath.size() - 1] != '/') {
		rootPath.push_back('/');
	}


	std::error_code code;
	if (!fs::is_directory(rootPath), code) {
		fprintf(stderr, "[%sERROR%s]\tCIABot Root is not a directory!\n",
			Colours::RED, Colours::RESET);
		return ENOTDIR;
	}

	try {
		fs::create_directories(rootPath + "config/");
		fs::create_directory(rootPath + "modules/");
	} catch (std::exception& e) {
		fprintf(stderr, "[%sERROR%s]\tFailed to create CIABot data directories!\n",
			Colours::RED, Colours::RESET);
		return EINVAL;
	}

	if (!loadData()) {
		fprintf(stderr, "[%sERROR%s]\tFailed to load data!\n",
			Colours::RED, Colours::RESET);
		return 2;
	}
	printInfo(InfoLevel::LOAD, "[%sLOAD%s]\tLoading commands...\n",
		Colours::YELLOW, Colours::RESET);
	loadCommands();
	printInfo(InfoLevel::LOAD, "[%sLOAD%s]\tLoading CIABot...\n",
		Colours::YELLOW, Colours::RESET);

	try {
		if (clientThread != nullptr) {
			delete(clientThread);
		}
		clientThread = new std::thread(startClient, createConsole, offline); clientThread->join();

		quit(); // Hopefully shouldn't be called
	} catch (std::exception& e) {
		printf("[%sERROR%s]\tCIABot crashed: %s%s%s - Stack Trace:\n%s\n",
			Colours::RED, Colours::RESET,
			Colours::RED, e.what(), Colours::RESET,
			getBacktrace().c_str());
	}
	return 0;
}
