#include <version.h>

#include <commands.h>

namespace CIABot {
	CHANGELOG changelog = {
		{"0", {
			{"1", {
				{"0", "Added a changelog.\nPermissions for commands and stuff coming soon."}
			}},
			{"2", {
				{"0", "Added `$permissions`, `$ping`, `$prefix`."},
				{"1", "Fixed a bunch of bugs."}
			}},
			{"3", {
				{"0", "Added `$modules` and the modules system.\nFixed a potential error in the parser."},
				{"1", "Server owners are now welcomed" + Command::messageRing(OWNER_EXECUTION_LEVEL, " and given an execution level of **Ring-__{RING}__**") + "."},
				{"2", "`$modules` now displays times in microseconds and not nanoseconds."},
				{"3", "Added `$ring` and fixed some issues."},
				{"4", "Added `$uptime`."},
				{"5", "Added `$info` and `$user`."},
				{"6", "Fixed some issues with `$user` and added URL data to filters, ring-0 only for now."},
				{"7", "React with a wastebasket or x emoji to delete unwanted messages."},
				{"8", "Added `$retry` and fixed filters."}
			}},
			{"4", {
				{"0", "Added caching system to speed everything up."}
			}},
			{"5", {
				{"0", "Upgraded Discord web API library to latest spec."},
				{"1", "Fixed all known issues with web API library."}
			}},
			{"6", {
				{"0", "Updated console and made it fancier."}
			}},
			{"7", {
				{"0", "Added offline mode."}
			}},
			{"8", {
				{"0", "Added (broken) zlib compression for data."},
				{"1", "Fixed embeds not being sent."},
				{"2", "`$say` now deletes the command."}
			}},
			{"9", {
				{"0", "Added `$toilet` in honour of new superpower."},
				{"1", "Extra error checking on startup."}
			}},
			{"10", {
				{"0", "Added `$minesweeper`."},
				{"1", "Fixed some bugs"}
			}}
		}},
		{"1", {
			{"0", {
				{"0", "Redesigned filters to use filter names and improved code overall."},
				{"1", "Fixed a silly bug in permissions."},
				{"2", "Filters can now force lowercase and ascii."}
			}},
			{"1", {
				{"0", "Added `$elections` and fixed bugs."},
				{"1", "Fixed random permission bug."}
			}},
			{"2", {
				{"0", "Added permission cooldowns"}
			}}
		}}
	};
	std::string version = "1.2.0";
}
