#include <events.h>

#include <threads.h>
#include <types.h>
#include <util.h>

#include <sched.h>
#include <algorithm>

namespace CIABot {
	Task::Task(TASK_CALLBACK callback, size_t time, bool remove) {
		this->callback = callback;
		this->time = time;
		this->remove = remove;
	}

	bool Tasks::start() {
		if (this->running || !this->closed) {
			return false;
		}
		this->closed = false;

		deleteThread(tasksThread);
		tasksThread = new std::thread(tasksLoop, this);
		tasksThread->detach();
		return true;
	}

	bool Tasks::stop() {
		if (!this->running || this->closed) {
			return false;
		}
		this->closed = true;
		while (this->running) {
			sleep(10);
		}
		this->running = false;
		deleteThread(tasksThread);
		return true;
	}

	void Tasks::process() {
		this->tasks.erase(
			std::remove_if(this->tasks.begin(), this->tasks.end(), [=](const Task& t) {
				size_t now = timestamp();
				if (now >= t.time) {
					if (t.callback != nullptr) {
						t.callback(now - t.time);
					}
					return t.remove;
				}
				return t.toRemove;
			}),
			this->tasks.end());
	}

	size_t Tasks::add(Task task) {
		this->tasks.push_back(task);
		return this->tasks.size();
	}
	size_t Tasks::add(TASK_CALLBACK callback, size_t after) {
		return this->add(Task(callback, timestamp() + after));
	}

	void Tasks::remove(size_t index) {
		this->tasks[index].toRemove = true;
	}

	void tasksLoop(Tasks* tasks) {
		tasks->running = true;
		while (true) {
			if (tasks->closed) {
				tasks->closed = false;
				break;
			}

			tasks->process();
			if (sched_yield()) {
				fprintf(stderr, "[%sERROR%s]\tFailed to set task thread priority: %s%s%s\n",
					Colours::RED, Colours::RESET,
					Colours::RED, strerror(errno), Colours::RESET);
			}
			sleep(1); // Dont eat the cpu 24/7
		}
		tasks->running = false;
	}

	Tasks* tasks = nullptr;
}