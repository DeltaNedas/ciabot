#include <userdata.h>

#include <info.h>
#include <modules.h>
#include <util.h>
#include <util/compression.h>

namespace CIABot {
	UserData::UserData(std::string user, std::string message, std::string channel, std::string server, bool fake) {
		this->user = user;
		this->message = message;
		this->channel = channel;
		this->server = server;
		this->fake = fake;

		this->executionLevel = executionLevels.get(user, server);
		this->rupees = CIABot::rupees.get(user, server);
	}


	UserData::UserData(AwokenDiscord::Message* message, bool fake) : UserData(
		message->author.ID, message->ID, message->channelID, message->serverID, fake) {
		AwokenDiscord::Channel c;
		if (getChannel(message->channelID, &c)) {
			this->dm = c.type == AwokenDiscord::Channel::DM;
		}
	}

	Tokens tokens;
	Prefixes prefixes;
	ExecutionLevels executionLevels;
	Rupees rupees;
	Filters filters;
	Permissions permissions;
	Messages messages;
	Modules modules;
	Elections elections;

	int readData(std::string filename, JsonData* data, bool silent) {
		std::string content;
		if (!readFile(rootPath + "config/" + filename, &content)) {
#if USE_COMPRESSION
			std::string decompressed = decompress(content);
			printf("Content: %s (%lu) - %s\n", content.c_str(), content.size(), decompressed.c_str());
			if (decompressed.size()) { // Decompress it only if it is a valid zlib file
				content = decompressed;
			}
#endif

			json newData;
			try {
				newData = json::parse(content);
			} catch (std::exception& e) {
				if (!silent) {
					fprintf(stderr, "[%sERROR%s]\tFailed to parse %s : %s%s%s\n",
						Colours::RED, Colours::RESET, filename.c_str(),
						Colours::BOLD, e.what(), Colours::RESET);
					quit(-1, false);
				}
				return -1;
			}

			try {
				int code = data->deserialise(newData);
				if (!silent && code) {
					quit(-3, false);
				}
				return 0;
			} catch (std::exception& e) {
				fprintf(stderr, "[%sERROR%s]\tFailed to deserialise data for %s: %s%s%s\n",
					Colours::RED, Colours::RESET,
					filename.c_str(),
					Colours::RED, e.what(), Colours::RESET);
				return 1;
			}
		} else {
			if (errno == ENOENT) {
				data->fallback();
				writeData(filename, data);
				return 1; // Default data written, file doesnt exist
			}

			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to read file %s%s%s: %s%s%s!\n",
					Colours::RED, Colours::RESET,
					Colours::BOLD, filename.c_str(), Colours::RESET,
					Colours::RED, strerror(errno), Colours::RESET);
				quit(errno, false);
			}
		}
		return -3;
	}

	bool loadData() {
		try {
			size_t old = timestamp();
			printInfo(InfoLevel::LOAD, "[%sLOAD%s]\tLoading core data... ",
				Colours::YELLOW, Colours::RESET);
			int code = readData("tokens.json", &tokens);
			if (code) {
				if (code == 1) {
					fprintf(stderr, "\n[%sERROR%s]\tNo tokens found, created default file. %sPlease fill it in with the correct values!%s\n",
						Colours::RED, Colours::RESET, Colours::BOLD, Colours::RESET);
					exit(0);
				} else {
					exit(code);
				}
			}

			readData("prefixes.json", &prefixes);
			readData("execution_levels.json", &executionLevels);
			readData("rupees.json", &rupees);
			readData("filters.json", &filters);
			readData("permissions.json", &permissions);
			readData("messages.json", &messages);
			readData("modules.json", &modules);
			readData("elections.json", &elections);
			printInfo(InfoLevel::LOAD, "done!\n");
			printInfo(InfoLevel::LOAD | InfoLevel::TIME, "[%sLOAD.TIME%s]\tCore data took %luμs to load.\n",
				Colours::YELLOW, Colours::RESET, (timestamp() - old) / (size_t) 1e3);
			if (loadModules()) {
				saveData();
				exit(1);
			}
			return true;
		} catch (std::exception& e) {
			return false;
		}
	}

	int writeData(std::string filename, JsonData* data, bool silent) {
		std::string out = data->serialise().dump();
#if USE_COMPRESSION
		std::string compressed = compress(out);
		printf("Out: %s - %s\n", out.c_str(), compressed.c_str());
		if (compressed.size()) {
			out = compressed;
		}
#endif
		if (writeFile(rootPath + "config/" + filename, out)) {
			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to write file %s: %s%s%s\n",
					Colours::RED, Colours::RESET, filename.c_str(), Colours::BOLD, strerror(errno), Colours::RESET);
				quit(errno, false); // Don't save, there will be an error loop
			}
			return errno;
		}
		return 0;
	}

	bool savingData = false;
	bool saveData() {
		if (!savingData) {
			bool ret = true;
			if (unloadModules()) {
				ret = false;
			}
			size_t old = timestamp();
			printInfo(InfoLevel::SAVE, "[%sSAVE%s]\tSaving data... ",
				Colours::YELLOW, Colours::RESET);

			//writeData("tokens.json", &tokens);
			writeData("prefixes.json", &prefixes);
			writeData("execution_levels.json", &executionLevels);
			writeData("rupees.json", &rupees);
			writeData("filters.json", &filters);
			writeData("permissions.json", &permissions);
			writeData("messages.json", &messages);
			writeData("modules.json", &modules);
			writeData("elections.json", &elections);
			savingData = false;
			printInfo(InfoLevel::SAVE, "done!\n");
			printInfo(InfoLevel::SAVE | InfoLevel::TIME, "[%sSAVE.TIME%s]\tData took %luμs to save.\n",
				Colours::YELLOW, Colours::RESET, (timestamp() - old) / (size_t) 1e3);
			return ret;
		}
		return false;
	}
}
