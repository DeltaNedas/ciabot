#include <parser.h>

#include <algorithm>

#include <client.h>
#include <info.h>
#include <util/strings.h>

namespace CIABot {
	void runCommand(size_t index, std::string commandName, Command* command, CommandInput input, UserData userData, bool* after, std::string* data) {
		size_t old = timestamp();
		std::string prefix = prefixes.get(userData.server);
		printInfo(InfoLevel::COMMAND, "[%sCOMMAND%s]\tRunning %s%s%s%s%s...\n",
			Colours::CYAN, Colours::RESET,
			Colours::CYAN, prefix.c_str(), Colours::BOLD, commandName.c_str(), Colours::RESET);
		try {
			CommandOutput output = command->run(input);
			if (data) {
				*data = output.data;
			} else {
				if (userData.fake) {
					if (!output.data.empty()) {
						printf(">> %s\n",
							json(output).dump(1, '\t').c_str());
					}
				} else {
					sendMessage(userData.channel, output, userData.message);
				}
			}
		} catch (std::exception& e) {
			fprintf(stderr, "[%sERROR%s]\tCommand %s%s%s%s%s crashed: %s%s%s\n",
				Colours::RED, Colours::RESET,
				Colours::CYAN, prefix.c_str(),
				Colours::BOLD, commandName.c_str(), Colours::RESET,
				Colours::RED, e.what(), Colours::RESET);
			fprintf(stderr, "[%sERROR%s]\tEnvironment:\n",
				Colours::RED, Colours::RESET);
			fprintf(stderr, "\tUser: %s\n\tMessage: %s\n\tChannel: %s\n\tServer: %s\n\tCurrent time: %s\n",
				userData.user.c_str(), userData.message.c_str(), userData.channel.c_str(), userData.server.c_str(), timestampStr().c_str());
			fprintf(stderr, "[%sERROR%s]\tStack trace:\n%s\n",
				Colours::RED, Colours::RESET, getBacktrace().c_str());
			if (!userData.fake) {
				sendMessage(userData.channel, command->error(), userData.message);
			}
		}

		auxiliaryThreads.erase(auxiliaryThreads.begin() + index); // Remove this threads pointer
		if (after != nullptr) {
			*after = !*after;
		}

		printInfo(InfoLevel::COMMAND | InfoLevel::TIME, "[%sCOMMAND.TIME%s]\tCommand %s%s%s%s%s took %s to run.\n",
			Colours::CYAN, Colours::RESET,
			Colours::CYAN, prefix.c_str(), Colours::BOLD, commandName.c_str(), Colours::RESET,
			timeToDate(timestamp() - old, 2).c_str());
	}

	int parseCommand(std::string content, UserData userData, AwokenDiscord::Message message, bool* after, std::string* data) {
		std::string prefix = prefixes.get(userData.server);

		if (content.rfind(prefix, 0)) {
			return 1; // Not a command
		}

		content = content.substr(prefix.size());

		// Parse any subcommands
		std::smatch submatch; match(content, "\\$\\(.+?\\)", "", &submatch);
		for (size_t i = 0; i < submatch.size(); i++) {
			std::string subcommand = submatch[i];
			size_t start = submatch.position(i), end = subcommand.size();
			subcommand = prefix + subcommand.substr(2, end - 3);
			printf("Subcommand is %s\n", subcommand.c_str());

			bool subAfter = false;
			auto subMessage = message;
			subMessage.content = subcommand;
			std::string subData = "";
			if (!parseCommand(subcommand, userData, subMessage, &subAfter, &subData)) {
				while (!subAfter) {
					sleep(10);
				}
				content.replace(start, end, subData);
				printf("New content is %s\n\tSubbed in %s\n", content.c_str(), subData.c_str());
			}
		}

		auto args = split(content, " ");
		std::string commandName = args[0];
		args.erase(args.begin());
		auto iter = commands.find(commandName);
		if (iter == commands.end()) {
			/*std::string alias = aliases.find(commandName, userData.server);
			if (alias.empty()) {*/
				return 2; // Unknown command
			//}
		}

		CommandInput input(args, message, userData, content);
		std::string creator = "";
		if (!userData.fake) {
			creator = userData.message;
		}

		Command* command = iter->second;
		if (command == nullptr) {
			if (!userData.fake) {
				sendMessage(userData.channel, Command::error(commandName, "That command is currently broken, ***sorry***!"), creator);
			} else {
				printf(">> That command is broken!!!\n");
			}
			return 4; // Broken command
		}

		long cooldown = command->getCooldown(input);
		if (!userData.fake && cooldown > 0) {
			sendMessage(userData.channel, command->cooldownError(cooldown), creator);
			return 5; // Cooldown
		}
		command->setCooldown(input);

		if (!command->canRun(input)) {
			if (!userData.fake) {
				sendMessage(userData.channel, command->permissionError(), creator);
			} else {
				printf(">> Permission... DENIED!\n");
			}
			return 3; // Permission denied
		}

		std::thread* commandThread = new std::thread(runCommand,
			auxiliaryThreads.size(),
			commandName,
			command,
			input,
			userData,
			after,
			data);
		auxiliaryThreads.push_back(commandThread);
		commandThread->detach();
		return 0; // Ran it
	}

	int parseCommand(AwokenDiscord::Message message, UserData userData, bool* after) {
		return parseCommand(message.content, userData, message, after);
	}

	int parseCommand(AwokenDiscord::Message message, bool* after) {
		return parseCommand(message.content, UserData(&message, false), message, after);
	}
}
