#include <permissions.h>

#include <events.h>
#include <util.h>
#include <util/generics.h>

namespace CIABot {
	Permission::Permission(execution_level_t executionLevel, rupees_t rupees,
			rupees_t rupeesCost, size_t deleteAfter, cooldown_t cooldown,
			size_t created, size_t joined,
			std::map<std::string, std::vector<std::string>> blacklists,
			std::map<std::string, std::vector<std::string>> whitelists) {
		this->executionLevel = executionLevel;
		this->rupees = rupees;
		this->rupeesCost = rupeesCost;
		this->deleteAfter = deleteAfter;
		this->created = created;
		this->joined = joined;
		this->blacklists = blacklists;
		this->whitelists = whitelists;
	}

	Permission::Permission(const Permission& rhs) {
		this->executionLevel = rhs.executionLevel;
		this->rupees = rhs.rupees;
		this->rupeesCost = rhs.rupeesCost;
		this->deleteAfter = rhs.deleteAfter;
		this->cooldown = rhs.cooldown;
		this->created = rhs.created;
		this->joined = rhs.joined;
		this->blacklists = rhs.blacklists;
		this->whitelists = rhs.whitelists;
	}

	std::vector<std::string> Permission::getList(bool white, std::string type) {
		auto lists = white ? this->whitelists : this->blacklists;

		auto it = lists.find(type);
		if (it == lists.end()) {
			return {};
		}

		return it->second;
	}

	cooldown_t Permission::getCooldown(std::string user) {
		cooldown_t last = get(this->cooldowns, user);
		if (!last) {
			return 0;
		}
		printf("Cooldown last at %li\n", last - timestamp());
		return last - timestamp();
	}

	void Permission::setCooldown(std::string user) {
		this->cooldowns[user] = timestamp() + this->cooldown;
		tasks->add([this, user](size_t delta){
			this->cooldowns.erase(user);
		}, this->cooldown);
	}

	void loadDefaultPermissions() {
		Permission none;
		Permission ring0(0);
		Permission ring1(1);
		Permission ring2(2);
		Permission interject(DEFAULT_EXECUTION_LEVEL, 10, 10);

		defaultPermissions["delete.from_reaction"] = ring2;

		defaultPermissions["elections.default.vote"] = none;
		// Used as a default for election voting.
		// Elections copy this to permissions["server_id"]["elections.NAME.vote"]

		defaultPermissions["elections.run"] = none;
		defaultPermissions["elections.list"] = none;
		defaultPermissions["elections.create"] = ring2;
		defaultPermissions["elections.start"] = ring2;
		defaultPermissions["elections.end"] = ring2;
		defaultPermissions["elections.remove"] = ring2;
		defaultPermissions["elections.run"] = ring2;
		defaultPermissions["elections.ban"] = ring2;
		defaultPermissions["elections.info"] = ring2;

		defaultPermissions["filters.default.trigger"] = none;
		// Used as a default for filters.
		// Filters copy this to permissions["server_id"]["filters.NAME.trigger"]

		defaultPermissions["filters.run"] = none;
		defaultPermissions["filters.list"] = ring2;
		defaultPermissions["filters.list.external"] = ring0;
		defaultPermissions["filters.set"] = ring2;
		defaultPermissions["filters.set.external"] = ring0;
		defaultPermissions["filters.remove"] = ring2;
		defaultPermissions["filters.remove.external"] = ring0;
		defaultPermissions["filters.info"] = ring2;
		defaultPermissions["filters.info.external"] = ring0;

		defaultPermissions["help.run"] = none;
		defaultPermissions["help.command"] = none;
		defaultPermissions["help.category"] = none;

		defaultPermissions["modules.run"] = none;
		defaultPermissions["modules.list"] = none;
		defaultPermissions["modules.list.enabled"] = none;
		defaultPermissions["modules.list.disabled"] = ring0;
		defaultPermissions["modules.list.all"] = ring0;
		defaultPermissions["modules.load"] = ring0;
		defaultPermissions["modules.unload"] = ring0;
		defaultPermissions["modules.reload"] = ring0;

		defaultPermissions["permissions.run"] = none;
		defaultPermissions["permissions.list"] = ring2;
		defaultPermissions["permissions.list.external"] = ring0;
		defaultPermissions["permissions.set"] = ring2;
		defaultPermissions["permissions.set.rupees_cost"] = ring0;
		defaultPermissions["permissions.set.external"] = ring0;
		defaultPermissions["permissions.reset"] = ring2;
		defaultPermissions["permissions.reset.external"] = ring0;
		defaultPermissions["permissions.info"] = ring2;
		defaultPermissions["permissions.info.external"] = ring0;

		defaultPermissions["prefix.run"] = ring2;

		defaultPermissions["ping.run"] = none;
		defaultPermissions["ping.precision"] = none;

		defaultPermissions["ring.run"] = none;
		defaultPermissions["ring.get"] = none;
		defaultPermissions["ring.get.others"] = ring0;
		defaultPermissions["ring.set"] = ring2;
		defaultPermissions["ring.set.external"] = ring0;

		defaultPermissions["timeouts.run"] = none;
		defaultPermissions["timeouts.list"] = ring2;
		defaultPermissions["timeouts.list.external"] = ring0;
		defaultPermissions["timeouts.create"] = ring2;
		defaultPermissions["timeouts.create.external"] = ring0;
		defaultPermissions["timeouts.remove"] = ring2;
		defaultPermissions["timeouts.remove.external"] = ring0;
	}

	std::map<std::string, Permission> defaultPermissions = {};
}
