#include <util.h>

#include <chrono>
#include <thread>
#ifdef linux // Symlink resolving
	#include <linux/limits.h>
#endif
#include <time.h>

#include <client.h>
#include <util/strings.h>

namespace CIABot {
	int execute(std::string* output, std::string command) {
		FILE* pipe = popen(command.c_str(), "r");
		if (pipe == NULL) {
			fprintf(stderr, "[%sERROR%s]\tFailed to run command %s: %s%s%s\n",
				Colours::RED, Colours::RESET, command.c_str(),
				Colours::RED, strerror(errno), Colours::RESET);
			return errno;
		}

		readFile(pipe, output);
		//int ret = pclose(pipe);
		return 0;
	}

	void sleep(int milliseconds) {
		std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
	}


#ifdef linux
	int resolvePath(std::string path, std::string* resolved) {
		char buffer[PATH_MAX];
		size_t len = readlink(buffer, sizeof(buffer) - 1);
		if (len == -1) {
			if (errno == EINVAL) { // Not a symlink, or it just doesnt exist at all.
				*resolved = path;
				return 0;
			}
			return errno;
		}

		buffer[len] = '\0'
		*resolved = std::string(buffer);
		return 0;
	}
#endif

	int readFile(FILE* file, std::string* output) {
		char* buffer = NULL;
		size_t dataSize, readSize;

		// Get file size
		fseek(file, 0, SEEK_END);
		dataSize = ftell(file);
		rewind(file);

		// Allocate a string of file size + null terminator
		buffer = (char*) malloc(sizeof(char) * (dataSize + 1));
		if (buffer == NULL) {
			fclose(file);
			return errno;
		}
		readSize = fread(buffer, sizeof(char), dataSize, file);

		// Terminate it
		buffer[dataSize] = '\0';
		if (dataSize != readSize) {
			// I/O Error
			free(buffer);
			buffer = NULL;
			return errno;
		}

		// Could check at start but stuff like checking for I/O errors would break.
		if (output != nullptr) {
			*output = std::string(buffer);
		}
		free(buffer);
		return 0;
	}

	int readFile(std::string path, std::string* output) {
#ifdef linux
		if (resolvePath(path, &path)) {
			return errno;
		}
#endif

		FILE* file = fopen(path.c_str(), "r");
		if (file == NULL) {
			return errno;
		}
		int ret = readFile(file, output);
		fclose(file);
		return ret;
	}

	int writeFile(FILE* file, std::string data) {
		int code = fprintf(file, "%s", data.c_str());
		if (code < 0 || code != (int) data.size()) { // TODO: This won't care about not all data being read, fix it
			return errno;
		}

		return 0;
	}

	int writeFile(std::string path, std::string data, bool append) {
		errno = 0;
#ifdef linux
		if (resolvePath(path, &path) != 0 && errno != ENOENT) {
			return errno;
		}
#endif

		FILE* file = fopen(path.c_str(), append ? "a" : "w");
		if (file == NULL) {
			return errno;
		}

		int ret = writeFile(file, data);
		fclose(file);
		return ret;
	}

	// Time stuff

	size_t timestamp() { // In nanoseconds
		struct timespec spec;
		clock_gettime(CLOCK_REALTIME, &spec);

		return spec.tv_sec * 1e9 + spec.tv_nsec;
	}

	std::string timestampStr(std::string format, size_t timestamp, size_t length) { // UTC
		time_t rawTime;
		struct tm timeInfo;
		char* buffer = (char*) malloc(sizeof(char) * length);

		if (timestamp) {
			rawTime = (time_t) timestamp / (time_t) 1e9;
		} else {
			time(&rawTime);
		}

		if (gmtime_r(&rawTime, &timeInfo) == NULL) {
			fprintf(stderr, "[%sERROR%s]\tFailed to get time: %s%s%s\n",
				Colours::RED, Colours::RESET,
				Colours::RED, strerror(errno), Colours::RESET);
			return "";
		}

		if (format.empty()) {
			format = "%Y/%m/%d - %I:%M:%S %p";
		}
		strftime(buffer, length, format.c_str(), &timeInfo);
		std::string output = "";
		if (buffer != NULL) {
			output = std::string(buffer);
			free(buffer);
		}
		return output;
	}

	size_t dateToUnix(std::string date) { // Turn Discord joined_at into a workable timestamp.
		struct tm time;
		int year, month, day, hour, min, sec, us, tz1, tz2;
		int found = sscanf(date.c_str(), "%d-%d-%dT%d:%d:%d.%d+%d:%d",
			&year, &month, &day, &hour, &min, &sec, &us, &tz1, &tz2);
		if (found != 9) {
			return 0;
		}

		time.tm_year = year - 1900;
		time.tm_mon = month - 1;
		time.tm_mday = day;
		time.tm_hour = hour + tz1; // Currently timezone is ignored by discord? Just in case...
		time.tm_min = min + tz2;
		time.tm_sec = sec;
		time_t ret = mktime(&time);
		return ((size_t) ret * 1000000 + us) * 1000;
	}

	// Would use 1e9 but (size_t) everywhere sucks
	// Fuck you modern seplesples.
	const std::vector<size_t> timeFactors = {
		31536000000000000,
		2678400000000000,
		604800000000000,
		86400000000000,
		3600000000000,
		60000000000,
		1000000000,
		1000000,
		1000,
		1
		//, 0.001
	};

	const std::vector<std::string> timeNames = {
		"Year",
		"Month",
		"Week",
		"Day",
		"Hour",
		"Minute",
		"Second",
		"Millisecond",
		"Microsecond",
		"Nanosecond"
		//, "Picosecond"
	};

	void stepTime(std::vector<std::string>& ret, size_t& place, size_t& time, int i) {
		size_t amount = time / timeFactors[i];
		if (amount) {
			time %= timeFactors[i];
			ret.push_back(count(amount, timeNames[i]));
			place--;
		}
	}

	std::string timeToDate(size_t time, size_t place) {
		std::vector<std::string> ret = {};
		for (int i = 0; (place > 0) && (i < (int) timeFactors.size()); i++) {
			stepTime(ret, place, time, i);
		}

		return concat(ret, ", ");
	}

	void deleteThread(std::thread* pointer) {
		if (pointer != nullptr) {
			delete(pointer);
		}
	}
}
