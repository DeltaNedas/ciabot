#include <modules.h>

#include <dirent.h>
#include <dlfcn.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>

#include <client.h>
#include <colours.h>
#include <commands.h>
#include <info.h>
#include <userdata.h>
#include <threads.h>
#include <util.h>
#include <util/generics.h>
#include <util/strings.h>

namespace CIABot {
	std::string Module::getName() {
		return this->name;
	}
	std::string Module::getTitle() {
		return this->stringCall("getTitle");
	}

	int Module::load() {
		auto oldCommands = commands;
		int code = this->intCall("load");

		this->registeredCommands = difference(commands, oldCommands);
		printInfo(InfoLevel::MODULE, "[%sMODULE%s]\tModule %s%s%s registered %lu commands.\n",
			Colours::MAGENTA, Colours::RESET,
			Colours::BOLD, this->getName().c_str(), Colours::RESET,
			this->registeredCommands.size());
		return code;
	}
	int Module::unload() {
		auto it = this->intFuncs.find("unload");
		if (it != this->intFuncs.end()) {
			return it->second();
		}

		for (std::string name : this->registeredCommands) {
			unloadCommand(name);
		}
		this->registeredCommands = {};
		return 0;
	}
	int Module::reload() {
		int code = this->unload();
		if (code) {
			return code;
		}

		return this->load();
	}

	std::string Module::stringCall(std::string type) {
		auto it = this->stringFuncs.find(type);
		if (it != this->stringFuncs.end()) {
			return std::string(it->second());
		}
		return "";
	}
	int Module::intCall(std::string type) {
		auto it = this->intFuncs.find(type);
		if (it != this->intFuncs.end()) {
			return it->second();
		}
		return 0;
	}

	std::map<std::string, Module*> loadedModules;

	template <typename T>
	int addFunction(std::string name, void* handle, std::string type, std::map<std::string, T>& map, bool optional) {
		T func;
		*(void**)(&func) = dlsym(handle, (name + "_" + type).c_str());
		if (func == NULL) {
			if (!optional) {
				fprintf(stderr, "[%sERROR%s]\tModule %s failed to load %s%s_%s%s(): %s%s%s\n",
					Colours::RED, Colours::RESET, name.c_str(),
					Colours::BOLD, name.c_str(), type.c_str(), Colours::RESET,
					Colours::RED, dlerror(), Colours::RESET);
				return ENOSYS;
			}
		} else {
			map[type] = func;
			return 0;
		}
		return ENOENT;
	}

	int loadModule(std::string name) {
		std::string path = rootPath + "modules/" + name + ".so";

		if (modules.get(name)) {
			// Load library
			void* handle = dlopen(path.c_str(), RTLD_NOW);
			if (handle == NULL) {
				fprintf(stderr, "[%sERROR%s]\tFailed to open module %s%s%s: %s%s%s\n",
					Colours::RED, Colours::RESET,
					Colours::BOLD, name.c_str(), Colours::RESET,
					Colours::RED, dlerror(), Colours::RESET);
				return 1;
			}

			Module* m = new Module();
			m->name = name;

			// Load functions of the module
			int code = addFunction(name, handle, "getTitle", m->stringFuncs, false);
			if (code == ENOSYS) {
				return 1;
			}
			code = addFunction(name, handle, "load", m->intFuncs, true);
			if (code == ENOSYS) {
				return 1;
			}
			code = addFunction(name, handle, "unload", m->intFuncs, true);
			if (code == ENOSYS) {
				return 1;
			}

			m->handle = handle;
			loadedModules[name] = m;
			printInfo(InfoLevel::MODULE, "[%sMODULE%s]\tEnabled module %s%s%s.\n",
				Colours::MAGENTA, Colours::RESET,
				Colours::MAGENTA, name.c_str(), Colours::RESET);
			m->load();
		} else {
			printInfo(InfoLevel::MODULE, "[%sMODULE%s]\tModule %s%s%s is disabled, skipped.\n",
				Colours::MAGENTA, Colours::RESET,
				Colours::MAGENTA, name.c_str(), Colours::RESET);
			return -1;
		}
		return 0;
	}

	int loadModules() {
		size_t old = timestamp();
		std::string modulesPath = rootPath + "modules/";
		DIR* dir = opendir(modulesPath.c_str());
		int code = 0;

		if (dir == NULL) {
			fprintf(stderr, "[%sERROR%s]\tFailed to list directory %s%s%s: %s%s%s\n",
				Colours::RED, Colours::RESET,
				Colours::BOLD, modulesPath.c_str(), Colours::RESET,
				Colours::RED, strerror(errno), Colours::RESET);
			quit(1);
		}

		struct dirent* entry;
		while ((entry = readdir(dir)) != NULL) {
			std::string file = std::string(entry->d_name);
			std::string name = match(file, "([^/]+)\\.so$");
			if (!name.empty()) {
				code = loadModule(name);
				if (code == 1) {
					break;
				}
			}
		}
		closedir(dir);

		if (!code) {
#if NEW_MODULES_REQUIRE_RESTART
		if (modules.wereNewModulesFound()) {
			printf("\n\n[%sWARN%s]\tNew modules were found.\nPlease check %sconfig/modules.json%s and make sure your desired modules are enabled.\nYou may then start CIABot and load them.\n",
				Colours::YELLOW, Colours::RESET,
				Colours::BOLD, Colours::RESET);
			return -1;
		}
#endif

		printInfo(InfoLevel::MODULE | InfoLevel::TIME, "[%sMODULE.TIME%s]\t%s took %luμs to enable.\n",
			Colours::MAGENTA, Colours::RESET,
			count(loadedModules.size(), "module").c_str(),
			(timestamp() - old) / (size_t) 1e3);
		}
		return code;
	}

	int unloadModule(std::string name) {
		auto it = loadedModules.find(name);
		if (it == loadedModules.end()) {
			return ENOENT;
		}

		Module* m = it->second;
		m->unload();
		void* handle = m->handle;

		int code = dlclose(handle);
		delete m;
		loadedModules.erase(it);
		if (code) {
			fprintf(stderr, "[%sERROR%s]\tFailed to unload module %s%s%s: %s%s%s\n",
				Colours::RED, Colours::RESET,
				Colours::BOLD, name.c_str(), Colours::RESET,
				Colours::RED, dlerror(), Colours::RESET);
			return 1;
		}
		return 0;
	}

	int unloadModules() {
		for (auto& it : loadedModules) {
			std::string name = it.first;
			Module* m = it.second;
			m->unload();
			void* handle = m->handle;

			int code = dlclose(handle);
			if (code) {
				fprintf(stderr, "[%sERROR%s]\tFailed to unload module %s%s%s: %s%s%s\n",
					Colours::RED, Colours::RESET,
					Colours::BOLD, name.c_str(), Colours::RESET,
					Colours::RED, dlerror(), Colours::RESET);
				return 1;
			}
		}
		loadedModules = {};
		return 0;
	}
}
