#include <util/discord.h>

#include <commands.h>
#include <info.h>
#include <message.h>
#include <util/generics.h>
#include <util/strings.h>
#include <util.h>

namespace CIABot {
	using namespace AwokenDiscord;

	CommandOutput helperHelp(std::string usage, std::string info) {
		Embed embed;
		embed.description = info;
		embed.title = "**Usage**: " + usage;
		embed.color = Colours::Discord::ANTI_DELTA;
		return CommandOutput("", usage, embed);
	}

	CommandOutput helperOutput(std::string title, std::string text, std::string data, unsigned int colour) {
		Embed embed;

		if (data.empty()) {
			if (text.empty()) {
				data = title;
			}
			data = text;
		}

		embed.color = colour;
		embed.title = title;
		embed.description = text;
		return CommandOutput("", data, embed);
	}


	bool getChannel(std::string id, Channel* channel, bool silent, size_t cached) {
		if (client->offline) {
			return false;
		}

		try {
			Channel newChannel = client->getChannel(id.c_str());
			*channel = newChannel;
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to get channel %s: %s%d%s\n",
					Colours::RED, Colours::RESET,
					id.c_str(),
					Colours::RED, e, Colours::RESET);
			}
		}
		return false;
	}
	bool getDirectMessages(std::string id, Channel* channel, bool silent, size_t cached) {
		if (client->offline) {
			return false;
		}

		Channel dm;
		try {
			if (cached) {
				dm = get(client->dms, id).get(cached);
			}
			if (dm.empty()) {
				dm = client->createDirectMessageChannel(id);
				if (client->dms.find(id) == client->dms.end()) {
					client->dms[id] = Cached(dm);
				}
			}
			*channel = dm;
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to get DM with %s: %s%d%s\n",
					Colours::RED, Colours::RESET, id.c_str(),
					Colours::RED, e, Colours::RESET);
			}
		}
		return false;
	}
	bool getServer(std::string id, Server* server, bool silent, size_t cached) {
		if (client->offline) {
			return false;
		}

		Server newServer;
		try {
			if (cached) {
				auto cache = get(client->servers, id);
				if (!cache.value.empty()) {
					newServer = cache.get(cached);
				}
			}
			if (newServer.empty()) {
				newServer = client->getServer(id.c_str());
				if (client->servers.find(id) == client->servers.end()) {
					client->servers[id] = Cached(newServer);
				}
			}
			*server = newServer;
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to get server %s: %s%d%s\n",
					Colours::RED, Colours::RESET,
					id.c_str(),
					Colours::RED, e, Colours::RESET);
			}
		}
		return false;
	}
	bool getUser(std::string id, User* user, bool silent, size_t cached) {
		if (client->offline) {
			return false;
		}

		try {
			User newUser = client->getUser(id.c_str());
			*user = newUser;
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to get user %s: %s%d%s\n",
					Colours::RED, Colours::RESET,
					id.c_str(),
					Colours::RED, e, Colours::RESET);
			}
		}
		return false;
	}
	bool getMessage(std::string id, std::string channel, AwokenDiscord::Message* message, bool silent, size_t cached) {
		if (client->offline) {
			return false;
		}

		try {
			AwokenDiscord::Message newMessage = client->getMessage(channel.c_str(), id.c_str());
			*message = newMessage;
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to get message %s in %s: %s%d%s\n",
					Colours::RED, Colours::RESET,
					id.c_str(), channel.c_str(),
					Colours::RED, e, Colours::RESET);
			}
		}
		return false;
	}
	bool getMember(std::string id, std::string server, AwokenDiscord::ServerMember* member, bool silent, size_t cached) {
		if (client->offline) {
			return false;
		}

		AwokenDiscord::ServerMember newMember;
		try {
			auto members = get(client->members, server);
			if (cached) {
				auto cache = get(members, id);
				if (cache.value.server.size()) {
					newMember = cache.get(cached).member;
				}
			}

			if (newMember.empty()) {
				newMember = client->getMember(server, id);

				if (members.find(id) == members.end()) {
					members[id] = Cached(ServerMember{id, newMember});
					client->members[server] = members;
				}
			}
			*member = newMember;
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			if (!silent) {
				fprintf(stderr, "[%sERROR%s]\tFailed to get member %s of server %s: %s%d%s\n",
					Colours::RED, Colours::RESET,
					id.c_str(), server.c_str(),
					Colours::RED, e, Colours::RESET);
			}
		}
		return false;
	}

	std::string sendMessage(CreateMessageParams& params, const std::string& creator) {
		std::string id(params.channelID);
		if (id.empty()) { // Console commands
			printf(">> %s\n", json(params).dump(1, '\t').c_str());
			return "true";
		}

		if (params.empty() || client->offline) {
			return "";
		}

		/*Channel dm;
		if (getDirectMessages(id, &dm, true, -1)) { // Don't update cache if not needed
			id = dm.ID;
		}*/

		try {
			AwokenDiscord::Message response = client->sendMessage(id, params);
			printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tSent message %s%s%s in %s.\n",
				Colours::GREEN, Colours::RESET,
				Colours::CYAN, json(params).dump(1, '\t').c_str(), Colours::RESET,
				id.c_str());
			if (!creator.empty()) {
				Message* message = messages.get(creator, "");
				if (message != nullptr) {
					message->responses.push_back(std::string(response.ID));
				}
			}

			messages.set(response, creator);
			return response.ID;
		} catch (AwokenDiscord::ErrorCode& e) {
			fprintf(stderr, "[%sERROR%s]\tFailed to send message %s%s%s: %s%d%s\n",
				Colours::RED, Colours::RESET,
				Colours::CYAN, json(params).dump(1, '\t').c_str(), Colours::RESET,
				Colours::RED, e, Colours::RESET);
		}
		return "";
	}
	std::string sendMessage(std::string id, const json& message, const std::string& creator) {
		CreateMessageParams params;
		try {
			message.get_to(params);
		} catch (std::exception& e) { // If it is not a valid message, treat is as just content.
			params.content = message.dump();
		}
		params.channelID = id;
		return sendMessage(params, creator);
	}
	std::string sendMessage(std::string id, const std::string& message, const std::string& creator) {
		CreateMessageParams params;
		try {
			return sendMessage(id, json::parse(message), creator);
		} catch (std::exception& e) { // If it is not a valid JSON object, treat is as just content.
			params.content = message;
		}
		params.channelID = id;
		return sendMessage(params, creator);
	}

	bool editMessage(std::string id, CreateMessageParams& params, AwokenDiscord::Message* ptr) {
		std::string channel(params.channelID);
		if (channel.empty()) { // Console commands
			printf(">> (edited) %s\n", json(params).dump(1, '\t').c_str());
			return true;
		}

		if (params.empty() || client->offline) {
			return false;
		}

		Channel dm;
		if (getDirectMessages(channel, &dm, true, -1)) { // Don't update cache if not needed
			channel = dm.ID;
		}

		try {
			AwokenDiscord::Message edited = client->editMessage(channel, id, params.content, params.embed);
			printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tEdited message %s to %s%s%s in %s.\n",
				Colours::GREEN, Colours::RESET, id.c_str(),
				Colours::CYAN, json(params).dump(1, '\t').c_str(), Colours::RESET,
				channel.c_str());

			// TODO: update messages

			if (ptr) {
				*ptr = edited;
			}
			return true;
		} catch (ErrorCode& e) {
			fprintf(stderr, "[%sERROR%s]\tFailed to edit message %s/%s to %s%s%s: %s%d%s\n",
				Colours::RED, Colours::RESET, id.c_str(), channel.c_str(),
				Colours::CYAN, json(params).dump(1, '\t').c_str(), Colours::RESET,
				Colours::RED, e, Colours::RESET);
		}
		return false;
	}
	bool editMessage(std::string id, std::string channel, const json& message, AwokenDiscord::Message* ptr) {
		CreateMessageParams params;
		try {
			message.get_to(params);
		} catch (std::exception& e) { // If it is not a valid message, treat is as just content.
			params.content = message.dump();
		}
		params.channelID = channel;
		return editMessage(id, params, ptr);
	}
	bool editMessage(std::string id, std::string channel, const std::string& message, AwokenDiscord::Message* ptr) {
		CreateMessageParams params;
		try {
			return editMessage(id, channel, json::parse(message), ptr);
		} catch (std::exception& e) { // If it is not a valid JSON object, treat is as just content.
			params.content = message;
		}
		params.channelID = channel;
		return editMessage(id, params, ptr);
	}

	bool deleteMessage(std::string id, std::string channel) {
		try {
			client->deleteMessage(channel, id);

			Message* m = CIABot::messages.get(id, CIABot::messages.get(channel));
			std::string content = "";
			if (m != nullptr) {
				" - " + std::string(Colours::BOLD) + m->history[m->history.size() - 1]->content + Colours::RESET;
			}
			printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s]\tDeleted message %s%s in %s.\n",
				Colours::GREEN, Colours::RESET,
				id.c_str(), content.c_str(), channel.c_str());
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			if (e == 404) {
				return true; // Already deleted.
			}

			fprintf(stderr, "[%sERROR%s]\tFailed to delete message %s in %s: %s%d%s\n",
				Colours::RED, Colours::RESET,
				id.c_str(), channel.c_str(),
				Colours::RED, e, Colours::RESET);
		}
		return false;
	}

	bool updateStatus() {
		return updateStatus(replace("with %0 | %1help", {
			{"%0", count(client->servers.size(), "server")},
			{"%1", DEFAULT_PREFIX}}));
	}

	bool updateStatus(std::string message) {
		try {
			Activity activity;
			activity.type = Activity::GAME;
			activity.name = message;
			StatusUpdate status;
			status.idleSince = timestamp() / (size_t) 1e6;
			status.game = activity;
			client->updateStatus(status);
			return true;
		} catch (AwokenDiscord::ErrorCode& e) {
			fprintf(stderr, "[%sERROR%s]\tFailed to update status to %s%s%s: %s%d%s\n",
				Colours::RED, Colours::RESET,
				Colours::BOLD, message.c_str(), Colours::RESET,
				Colours::RED, e, Colours::RESET);
		}
		return false;
	}

	void addField(std::vector<EmbedField>& fields, std::string name, std::string value, bool inLine) {
		EmbedField field;
		field.name = name;
		field.value = value;
		field.isInline = inLine;
		fields.push_back(field);
	}

	size_t createdAt(std::string snowflake) {
		return ((std::stoul(snowflake, NULL, 10) >> 22) / 1000 + 1420070400) * (size_t) 1e9;
	}

	json serialiseMessage(const AwokenDiscord::Message& message) {
		json data = {
			{"content", message.content},
			{"tts", message.tts}
		};
		if (message.embeds.size()) {
			data["embed"] = message.embeds[0];
		}
		return data;
	}
}
