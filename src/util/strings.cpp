#include <util/strings.h>

namespace CIABot {
	bool compare(std::string base, std::vector<std::string> compare) {
		for (std::string str : compare) {
			if (str == base) {
				return true;
			}
		}
		return false;
	}

	std::vector<std::string> split(std::string string, std::string seperator) {
		std::vector<std::string> result = {};

		while (string.size()) {
			size_t index = string.find(seperator);
			if (index != std::string::npos) {
				std::string word = string.substr(0, index);
				if (word.size() > 0) {
					result.push_back(word);
				}
				string = string.substr(index + seperator.size());
			} else {
				result.push_back(string);
				break;
			}
		}

		return result;
	}

	std::string concat(std::vector<std::string> strings, std::string by) {
		std::string result = "";

		if (strings.size() > 0) {
			for (std::string string : strings) {
				result += string + by;
			}
			result.erase(result.end() - by.size(), result.end());
		}
		return result;
	}

	// Clamp where n > 0
	inline size_t clamp(size_t n, size_t max) {
		return n > max ? max : n;
	}

	std::string clamp(std::string string, size_t len) {
		if (string.size() > len) {
			string.resize(len - clamp(string.size() - len, 3));
			string.append("...");
		}
		return string;
	}


	std::string escape(std::string unsafe) {
		std::string buffer = "";

		for (size_t pos = 0; pos != unsafe.size(); ++pos) {
			switch (unsafe[pos]) {
				case '"':  buffer += "\\\"";       break;
				default:   buffer += unsafe[pos]; break;
			}
		}
		return buffer;
	}

	std::string replace(std::string string, std::string match, std::string replacement) {
		return std::regex_replace(string, std::regex(match), replacement);
	}

	std::string replace(std::string string, std::vector<std::pair<std::string, std::string>> matches) {
		for (auto& pair : matches) {
			string = replace(string, pair.first, pair.second);
		}

		return string;
	}

	std::vector<std::string> replace(std::vector<std::string> strings, std::string match, std::string replacement) {
		for (size_t i = 0; i < strings.size(); i++) {
			strings[i] = replace(strings[i], match, replacement);
		}
		return strings;
	}

	std::vector<std::string> replace(std::vector<std::string> strings, std::vector<std::pair<std::string, std::string>> matches) {
		for (size_t i = 0; i < strings.size(); i++) {
			strings[i] = replace(strings[i], matches);
		}
		return strings;
	}

	std::string match(std::string string, std::string pattern, std::string fallback, std::smatch* matched) {
		std::smatch m;
		std::string ret = fallback;
		if (std::regex_search(string, m, std::regex(pattern)) && m.size()) {
			ret = m[m.size() - 1];
		}
		if (matched) {
			*matched = m;
		}
		return ret;
	}

	std::string toBinary(size_t data) {
		std::string ret = "";
		if (data) {
			ret += toBinary(data >> 1);
			ret += (data & 1) ? '1' : '0';
		}
		return ret;
	}
	std::string upper(std::string string) {
		std::string output;
		output.reserve(string.size());
		for (const char c : string) {
			if (c >= 'a' && c <= 'z') {
				output.push_back(c - 32);
			} else {
				output.push_back(c);
			}
		}

		return output;
	}
	std::string lower(std::string string) {
		std::string output;
		output.reserve(string.size());
		for (const char c : string) {
			if (c >= 'A' && c <= 'Z') {
				output.push_back(c + 32);
			} else {
				output.push_back(c);
			}
		}

		return output;
	}
	std::string count(size_t count, std::string thing, std::string format, std::string single) {
		if (count == 1) {
			return replace(
				replace(single, "\\{COUNT\\}", "1"),
				"\\{THING\\}", thing);
		}
		return replace(
			replace(format, "\\{COUNT\\}", std::to_string(count)),
			"\\{THING\\}", thing);
	}
	std::string toAscii(std::string string) {
		std::string ret = "";
		for (char c : string) {
			if ((c & 0x8) == 0) { // ASCII never starts with a 1
				ret.push_back(c);
			}
		}
		return ret;
	}
}