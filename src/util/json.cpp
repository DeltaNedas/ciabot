#include <util/json.h>

namespace CIABot {
	json emptyObject() {
		return json::parse("{}");
	}
	json emptyArray() {
		return json::parse("[]");
	}
}