#include <util/compression.h>

#include <stdio.h>
#include <string.h>

#include <zlib.h>

#include <colours.h>

namespace CIABot {
	const size_t CHUNK = 32768; // 2^15 bytes

	// Adapted from https://panthema.net/2007/0328-ZLibString.html
	// License is http://www.boost.org/LICENSE_1_0.txt
	std::string compress(const std::string& data) {
		std::string ret = "";

		// Initialise
		z_stream stream;
		stream.zalloc = Z_NULL;
		stream.zfree = Z_NULL;
		stream.opaque = Z_NULL;
		if (deflateInit(&stream, Z_BEST_COMPRESSION)) {
			return "";
		}
		stream.next_in = (unsigned char*) data.data();
		stream.avail_in = data.size();
		int code = Z_OK;
		char buffer[CHUNK];

		// Compress
		do {
			stream.next_out = reinterpret_cast<unsigned char*>(buffer);
			stream.avail_out = sizeof(buffer);
			printf("%u / %lu - %u / %lu\n", stream.avail_in, stream.total_in, stream.avail_out, stream.total_out);
			code = deflate(&stream, Z_FINISH);
			printf("> %u / %lu - %u / %lu\n", stream.avail_in, stream.total_in, stream.avail_out, stream.total_out);

			if (ret.size() < stream.total_out) {
				ret.append((char*) buffer, stream.total_out - ret.size());
			}
			printf("> Code is %d\n", code);
		} while (code == Z_OK);

		// Clean up
		deflateEnd(&stream);
		if (code != Z_STREAM_END) {
			fprintf(stderr, "[%sERROR%s]\tFailed to compress %lu bytes of data: %s%s (%d)%s\n",
				Colours::RED, Colours::RESET,
				data.size(),
				Colours::RED, stream.msg, code, Colours::RESET);
			return "";
		}
		return ret;
	}

	std::string decompress(const std::string& data) {
		std::string ret = "";
		printf("Decompressing %s - %lu bytes.\n", data.c_str(), data.size());

		// Initialise
		z_stream stream;
		memset(&stream, 0, sizeof(stream));
		if (inflateInit(&stream)) {
			return "";
		}
		stream.next_in = (unsigned char*) data.data();
		stream.avail_in = data.size();
		int code = Z_OK;
		char buffer[CHUNK];

		// Decompress
		do {
			stream.next_out = reinterpret_cast<unsigned char*>(buffer);
			stream.avail_out = sizeof(buffer);
			printf("%u / %lu - %u / %lu\n", stream.avail_in, stream.total_in, stream.avail_out, stream.total_out);
			code = inflate(&stream, Z_NO_FLUSH);
			printf("> %u / %lu - %u / %lu\n", stream.avail_in, stream.total_in, stream.avail_out, stream.total_out);

			if (ret.size() < stream.total_out) {
				ret.append(buffer, stream.total_out - ret.size());
			}
			printf("> Code is %d\n", code);
		} while (code == Z_OK);

		// Clean up
		inflateEnd(&stream);
		if (code != Z_STREAM_END) {
			fprintf(stderr, "[%sERROR%s]\tFailed to decompress %lu bytes of data: %s%s (%d)%s\n",
				Colours::RED, Colours::RESET,
				data.size(),
				Colours::RED, stream.msg, code, Colours::RESET);
			return "";
		}
		return ret;
	}
}