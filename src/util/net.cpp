#include <util/net.h>

#include <client.h>
#include <version.h>

namespace CIABot {
	using AwokenDiscord::HeaderPair;
	using AwokenDiscord::Route;
	using AwokenDiscord::Session;

	int downloadFile(std::string url, std::string* output, Method m, Response* ptr) {
		Session session;
		session.setUrl(url);
		std::vector<HeaderPair> header = {
			{"User-Agent", "CIABot/" + version + "x86_64 Udindu Lenugz"}, // Mostly BS
			{"Content-Length", "0"}
		};

		session.setHeader(header);

		Response response = session.request(m);
		if (ptr != nullptr) {
			*ptr = response;
		}
		if (output != nullptr) {
			*output = response.text;
		}

		return response.statusCode == 200 ? 0 : response.statusCode; // int code = downloadFile(...); if (code) { printf("Code was %d\n", code); }
	}
}