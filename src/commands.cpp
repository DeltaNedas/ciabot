#include <commands.h>

#include <regex>
#include <stdexcept>

#include <commands/elections.h>
#include <commands/filters.h>
#include <commands/help.h>
#include <commands/modules.h>
#include <commands/permissions.h>
#include <commands/ping.h>
#include <commands/prefix.h>
#include <commands/ring.h>
#include <info.h>
#include <permissions.h>
#include <util/strings.h>

namespace CIABot {
	using AwokenDiscord::addJsonField;
	using AwokenDiscord::getJsonField;

	CommandInput::CommandInput(std::vector<std::string> args, AwokenDiscord::Message message, UserData userData, std::string raw) {
		this->args = args;
		this->message = message;
		this->userData = userData;
		this->raw = raw;
	}
	CommandOutput::CommandOutput(std::string content, std::string data, AwokenDiscord::Embed embed, bool tts) {
		this->content = content;
		this->data = data;
		this->embed = embed;
		this->tts = tts;
	}
	void to_json(json& to, const CommandOutput& from) {
		// Data is ignored for simple content parsing
		//if (from.content.size() && from.embed.empty() && !from.tts) {
			addJsonField(to, "content", from.content);
			addJsonField(to, "data", from.data);
			addJsonField(to, "embed", from.embed);
			addJsonField(to, "tts", from.tts);
		//}
		//to = from.content;

	}
	void from_json(const json& from, CommandOutput& to) {
		//if (from.is_object()) {
			getJsonField(from, "content", to.content);
			getJsonField(from, "data", to.data);
			getJsonField(from, "embed", to.embed);
			getJsonField(from, "tts", to.tts);
			return;
		//}
		//from.get_to(to.content);
	}

	// Defaults
	CommandOutput Command::run(CommandInput& input) {
		return CommandOutput("This command is WIP.", "WIP");
	}

	bool Command::canRun(CommandInput& input) {
		return this->can(input);
	}

	std::string Command::getName() {
		return "wip_command";
	}

	std::string Command::getUsage() {
		return this->getName();
	}

	CommandOutput Command::getHelp(std::string serverID) {
		return helperHelp(this->getUsage(), "This is a WIP command.");;
	}

	std::set<std::string> Command::getTypes() {
		return {"wip"};
	}

	bool Command::can(CommandInput& input, std::string action, bool apply) {
		std::string name = this->getName() + "." + action;

		return permissions.has(name, input.userData, "", apply);
	}

	// Server is ignored for command cooldowns, unlike filters
	size_t Command::getCooldown(CommandInput& input, std::string action) {
		std::string name = this->getName() + "." + action;
		Permission* perm = permissions.get(name, "global", true);
		if (perm == nullptr) {
			return -1;
		}
		return perm->getCooldown(input.userData.user);
	}
	void Command::setCooldown(CommandInput& input, std::string action) {
		std::string name = this->getName() + "." + action;
		Permission* perm = permissions.get(name, "global", true);
		if (perm && perm->cooldown) {
			perm->setCooldown(input.userData.user);
		}
	}

	CommandOutput Command::error(std::string error) {
		return Command::error(this->getName(), error);
	}

	CommandOutput Command::error(std::string name, std::string error) {
		AwokenDiscord::Embed embed;
		embed.color = Colours::Discord::RED;

		embed.title = "🚨 Error encountered while running `" + name + "`!";
		if (!error.empty()) {
			embed.description = error;
		}
		return CommandOutput("", error, embed);
	}

	CommandOutput Command::permissionError(std::string action) {
		action = this->getName() + "." + action;
		AwokenDiscord::Embed embed;
		embed.title = "🚫 Permission... **DENIED**";
		embed.description = "You are lacking the `" + action + "` permission.";
		embed.color = Colours::Discord::RED;
		return CommandOutput("", action, embed);
	}

	CommandOutput Command::cooldownError(size_t wait) {
		AwokenDiscord::Embed embed;
		embed.title = "⌛  Cooldown";
		embed.description = "Please wait **" + timeToDate(wait, 2) + "** until running this command again.";
		embed.color = Colours::Discord::RED;
		return CommandOutput("", std::to_string(wait), embed);
	}

	std::string Command::messageRing(std::string server, std::string action, std::string format, std::string none) {
		Permission* perm = permissions.get(this->getName() + "." + action, server);
		std::string ret = none;
		if (perm != nullptr) {
			ret = Command::messageRing(perm->executionLevel, format, none);
		}
		return ret;
	}

	std::string Command::messageRing(execution_level_t level, std::string format, std::string none) {
		if (level != DEFAULT_EXECUTION_LEVEL) {
			return replace(format, "\\{RING\\}", std::to_string(level));
		}
		return none;
	}

	void Command::parseFlag(unsigned char* flags, unsigned char flag, std::vector<std::string> args, size_t index, bool fallback) {
		bool condition = fallback;
		Command::parseBool(&condition, args, index);

		if (condition) {
			*flags |= flag; // Add the bit
		} else {
			*flags &= ~flag; // Remove it
		}
	}
	void Command::parseBool(bool* value, std::vector<std::string> args, size_t index, bool fallback) {
		if (args.size() > index) {
			std::string str = lower(args[index]);
			if (compare(str, {"true", "1"})) {
				fallback = true;
			} else if (compare(str, {"false", "0"})) {
				fallback = false;
			} else if (str != "-") {
				throw std::runtime_error("`true` or `false` expected.");
			}
		}
		*value = fallback;
	}
	void Command::parseDouble(double* value, std::vector<std::string> args, size_t index, double fallback) {
		if (args.size() > index) {
			std::string str = args[index];
			if (str != "-") {
				try {
					fallback = stod(str);
				} catch (std::exception& e) {
					throw std::runtime_error("a number expected.");
				}
			}
		}
		*value = fallback;
	}
	void Command::parseLong(long* value, std::vector<std::string> args, size_t index, long fallback) {
		if (args.size() > index) {
			std::string str = args[index];
			if (str != "-") {
				try {
					fallback = stol(str);
				} catch (std::exception& e) {
					throw std::runtime_error("a whole number expected.");
				}
			}
		}
		*value = fallback;
	}
	void Command::parseString(std::string* value, std::vector<std::string> args, size_t index, std::string fallback) {
		if (args.size() > index) {
			fallback = args[index];
		}
		*value = fallback;
	}
	void Command::parseRing(execution_level_t* value, std::vector<std::string> args, size_t index, execution_level_t fallback) {
		if (args.size() > index) {
			std::string tmp = lower(args[index]);
			std::string try1 = match(tmp, "none");
			if (try1.size()) {
				*value = DEFAULT_EXECUTION_LEVEL;
			} else {
				std::string try2 = match(tmp, "ring-(\\d+)");
				if (try2.size()) {
					*value = (execution_level_t) stoi(try2);
				} else {
					std::string try3 = match(tmp, "([\\+-]?\\d+)");
					if (try3.size()) {
						*value = (execution_level_t) stoi(try2);
					} else {
						throw std::runtime_error("a number, ring-*number* or \"none\" expected.");
					}
				}
			}
		} else {
			*value = fallback;
		}
	}
	void Command::parseFilterName(std::string* value, std::vector<std::string> args, size_t index) {
		if (args.size() > index) {
			std::string ret = match(args[index], "^\\s*(\\w+)\\s*$");
			if (ret.size()) {
				*value = ret;
			} else {
				throw std::runtime_error("a valid filter name expected (a-Z, 0-9 and underscores only!)");
			}
		}
	}
	void Command::parseEmoji(std::string* value, std::vector<std::string> args, size_t index, std::string fallback) {
		if (args.size() > index) {
			// Match an /*emoji or an */emoji id
			//std::string tmp = match(args[index], R"(^\s*(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])\s*$)");
			//if (tmp.empty()) {
				std::string tmp = match(args[index], R"(^\s*<:.+:(\d{18})>\s*$)");
				if (tmp.empty()) {
					throw std::runtime_error("an emoji expected.");
				}
			//}
			fallback = tmp;
		}
		*value = fallback;
	}

	void Command::parseSnowflake(std::string* value, std::vector<std::string> args, size_t index, std::string fallback) {
		if (args.size() > index) {
			std::string tmp = lower(args[index]);
			if (!compare(tmp, {"-", "global"})) {
				std::string try1 = match(tmp, "^\\s*(\\d{18})\\s*$");
				if (try1.empty()) {
					throw std::runtime_error("a valid id expected.");
				}
				tmp = try1;
			}
			*value = tmp;
		} else {
			*value = fallback;
		}
	}
	void Command::parseChannel(std::string* value, std::vector<std::string> args, size_t index, std::string fallback) {
		if (args.size() > index) {
			std::string tmp = args[index];
			if (tmp != "-") {
				std::string try1 = match(tmp, "^\\s*<#(\\d{18})>\\s*$");
				if (try1.empty()) {
					std::string try2 = match(tmp, "^\\s*(\\d{18})\\s*$");
					if (try2.empty()) {
						throw std::runtime_error("a channel id expected.");
					}
					tmp = try2;
				} else {
					tmp = try1;
				}
			}
			*value = tmp;
		} else {
			*value = fallback;
		}
	}
	void Command::parseChannels(std::vector<std::string>* value, std::vector<std::string> args, size_t index, std::vector<std::string> fallback, std::string channelFallback) {
		if (args.size() > index) {
			if (args[index] == "-") {
				*value = fallback;
			}

			auto words = split(args[index], ",");
			for (size_t i = 0; i < words.size(); i++) {
				std::string str = "";
				Command::parseChannel(&str, words, i, channelFallback);
				value->push_back(str);
			}
		} else {
			*value = fallback;
		}
	}
	void Command::parseUser(std::string* value, std::vector<std::string> args, size_t index, std::string fallback) {
		if (args.size() > index) {
			std::string tmp = lower(args[index]);
			if (compare(tmp, {"-", "me", "myself"})) {
				tmp = "-";
			} else {
				std::string try1 = match(tmp, "^\\s*<@!?(\\d{17,18})>\\s*$");
				if (try1.empty()) {
					// It also accepts 17 because I've occasionally seen users with 17-long ids. Spooky.
					std::string try2 = match(tmp, "^\\s*(\\d{17,18})\\s*$");
					if (try2.empty()) {
						throw std::runtime_error("a user id expected.");
					}
					tmp = try2;
				} else {
					tmp = try1;
				}
			}
			*value = tmp;
		} else {
			*value = fallback;
		}
	}
	void Command::parseUsers(std::vector<std::string>* value, std::vector<std::string> args, size_t index, std::vector<std::string> fallback, std::string userFallback) {
		if (args.size() > index) {
			if (args[index] == "-") {
				*value = fallback;
			}

			auto words = split(args[index], ",");
			for (size_t i = 0; i < words.size(); i++) {
				std::string str = "";
				Command::parseUser(&str, words, i, userFallback);
				value->push_back(str);
			}
		} else {
			*value = fallback;
		}
	}

	void Command::parseTime(size_t* seconds, std::vector<std::string> args, size_t index, size_t fallback) {
		if (args.size() > index) {
			std::string str = lower(args[index]);

			addTime(seconds, 60 * 60 * 24 * 365, str, "(\\d+)\\s+y(?:ear)?s?");
			// Please note that a month is treated as 30 days and is only provided for convenience, not accuracy.
			addTime(seconds, 60 * 60 * 24 * 30, str, "(\\d+)\\s+mo(?:nth)?s?");
			addTime(seconds, 60 * 60 * 24 * 14, str, "(\\d+)\\s+f(?:ortnight)?s?");
			addTime(seconds, 60 * 60 * 24 * 7, str, "(\\d+)\\s+w(?:eek)?s?");
			addTime(seconds, 60 * 60 * 24, str, "(\\d+)\\s+d(?:ay)?s?");
			addTime(seconds, 60 * 60, str, "(\\d+)\\s+h(?:our)?s?");
			addTime(seconds, 60, str, "(\\d+)\\s+mi(?:nute)?s?");
			addTime(seconds, 1, str, "(\\d+)\\s+s(?:econd)?s?");
		} else {
			*seconds = fallback;
		}
	}

	void Command::addTime(size_t* ptr, size_t seconds, std::string match, std::string regex) {
		std::smatch matched;
		if (std::regex_search(match, matched, std::regex(regex)) && matched.size() > 1) {
			for (size_t i = 1; i < matched.size(); i++) {
				*ptr += (size_t) ((double) seconds * std::stod(matched[i]));
			}
		}
	}

	// Registry stuff

	std::map<std::string, Command*> commands = {};

	void unloadCommand(std::string name) {
		auto it = commands.find(name);
		if (it != commands.end()) {
			Command* command = it->second;
			if (command != nullptr) {
				delete command;
				commands.erase(it);
			}
		}
	}

	void loadCommands() {
		loadCommand<CommandElections>();
		loadCommand<CommandFilters>();
		loadCommand<CommandHelp>();
		loadCommand<CommandModules>();
		loadCommand<CommandPermissions>();
		loadCommand<CommandPing>();
		loadCommand<CommandPrefix>();
		loadCommand<CommandRing>();

		printInfo(InfoLevel::LOAD | InfoLevel::COMMAND, "[%sLOAD.COMMAND%s]\tLoaded %lu commands.\n",
			Colours::CYAN, Colours::RESET, commands.size());
	}

	void unloadCommands() {
		size_t count = commands.size();
		for (auto it : commands) {
			Command* command = it.second;
			if (command != nullptr) {
				delete command;
			}
		}
		commands = {};

		printInfo(InfoLevel::SAVE | InfoLevel::COMMAND, "[%sSAVE.COMMAND%s]\tUnloaded %lu commands.\n",
			Colours::CYAN, Colours::RESET, count);
	}
}
