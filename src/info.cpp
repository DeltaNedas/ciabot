#include <info.h>

#include <stdarg.h>
#include <stdio.h>

#include <options.h>

namespace CIABot {
	void printInfo(unsigned short condition, const char* format, ...) {
		#if COMPILE_INFO_MESSAGES
			va_list args;
			va_start(args, format);
			if (infoLevel & condition) {
				vprintf(format, args);
				fflush(stdout);
			}
			va_end(args);
		#endif
	}

	// Default info level when -i/--info-level <number> is not passed.
	unsigned short infoLevel = InfoLevel::NONE | InfoLevel::LOAD | InfoLevel::SAVE | InfoLevel::COMMAND;
}