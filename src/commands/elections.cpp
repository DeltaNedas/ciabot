#include <commands/elections.h>

#include <elections.h>
#include <info.h>
#include <permissions.h>
#include <util/discord.h>
#include <util/generics.h>
#include <util/strings.h>
#include <util.h>

namespace CIABot {
	AwokenDiscord::Embed buildElection(Election* e, std::string title = "⚖️ New Election!") {
		AwokenDiscord::Embed ret;
		ret.title = title;
		ret.description = e->title;
		ret.color = Colours::Discord::YELLOW;

		std::vector<AwokenDiscord::EmbedField> fields = {};
		size_t cand = 0;
		for (auto& it : e->candidates) {
			ElectionCandidate c = it.second;
			addField(fields, "Candidate #**" + std::to_string(++cand) + "**", emojiToString(c.emoji) + " - " + c.name);
		}
		ret.fields = fields;
		AwokenDiscord::EmbedFooter footer;
		footer.text = "Vote by reacting once, and only once.";
		if (e->endsAfter) {
			footer.text += "\nVoting will end in " + timeToDate((e->endsAfter + e->started + 1) - timestamp(), 2);
		}
		ret.footer = footer;

		return ret;
	}

	CommandOutput CommandElections::run(CommandInput& input) {
		std::string mode = "list";
		if (!input.args.empty()) {
			mode = lower(input.args[0]);
			input.args.erase(input.args.begin());
		}

		if (compare(mode, {"list", "create", "start", "end", "remove", "run", "ban"}) && !this->can(input, mode)) {
			return this->permissionError(mode);
		}

		size_t argc = input.args.size();
		size_t argn = 0;
		if (mode == "list") {
			bool running = true, created = false, ended = false;
			if (argc > 0) {
				try {
					this->parseBool(&running, input.args, argn++, true);
					this->parseBool(&created, input.args, argn++, false);
					this->parseBool(&ended, input.args, argn++, false);
				} catch (std::exception& e) {
					return this->error(e.what());
				}
			}

			this->can(input, "list", true); // Potentially pay rupees for it, if it was set up to do so.
			auto serverElections = elections.get(input.userData.server);

			std::vector<std::string> output = {};
			for (auto& it : serverElections) {
				Election* election = it.second;
				if (election != nullptr) {
					if ((election->canVote() && running) || (election->ended && ended) || (!election->started && created)) {
						output.push_back("`" + it.first + "` - *" + election->title + "*");
					}
				}
			}
			if (output.empty()) {
				return helperOutput("⚖️ Election list", "There are no elections.", "0");
			}

			return helperOutput("⚖️ Election list", concat(output, "\n"), std::to_string(output.size()));
		} else if (mode == "create") {
			// Resplit args by newline for readibility and to prevent pulling out of hair with escaping
			// Use \n to use newline in argument
			input.args = replace(
				split(
					concat(input.args, " "),
					"\n"),
				"\\n",
				"\n");
			argc = input.args.size();
			if (argc < 2) {
				return this->error("Run with a election name, title and optionally, a time limit.\n*(separated by newlines)*");
			}

			std::string name = "", title = "";
			size_t endsAfter = 0;

			try {
				this->parseFilterName(&name, input.args, argn++);
				this->parseString(&title, input.args, argn++);
				this->parseTime(&endsAfter, input.args, argn++); // TODO: Fix
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}
			endsAfter *= 1e9; // Convert seconds to nanoseconds

			// Vladimir Putin
			Election* old = elections.get(name, input.userData.server);
			if (old != nullptr && old->canVote()) {
				return this->error("You can't rig elections!");
			}

			this->can(input, "create", true);

			// literal botnet wow
			printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tCreated election %s for server %s:\n\tTitle: %s\n\tEnds after %s\n",
				Colours::BLUE, Colours::RESET,
				name.c_str(),
				input.userData.server.c_str(),
				title.c_str(),
				timeToDate(endsAfter, 2).c_str());

			// No memory leak because it's deleted on shutdown
			elections.set(new Election("", "", name, title, endsAfter, 0, 0, {}, {}), input.userData.server);
			return helperOutput("⚖️ Create Election", "Created Election `" + name + "` with title `" + title + "`.");
		} else if (mode == "start") {
			if (!argc) {
				return this->error("Run with an election name and optionally a channel.");
			}

			std::string name = "", channel = "", message = "";
			try {
				this->parseFilterName(&name, input.args, argn++);
				this->parseChannel(&channel, input.args, argn++, input.userData.channel);
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			AwokenDiscord::Channel c;
			if (!getChannel(channel, &c) || c.serverID != input.userData.server) {
				return this->error("Invalid channel.");
			}

			Election* e = elections.get(name, input.userData.server);
			if (e->canVote()) {
				return this->error("This election has already started.");
			}

			if (e->candidates.empty()) {
				return this->error("This election has no candidates.");
			}

			e->started = timestamp();
			e->ended = 0;
			e->voters = {};
			for (auto& it : e->candidates) {
				it.second.votes = 0;
			}
			e->channel = channel;
			message = sendMessage(channel, json({{"embed", buildElection(e)}}));
			if (message.empty()) {
				e->started = 0;
				e->channel = "";
				return this->error("Failed to send election message!");
			}
			e->message = message;
			e->addReactions();

			this->can(input, "start", true);
			if (channel == input.userData.channel) {
				return CommandOutput();
			}
			return helperOutput("⚖️ Start Election", "Started Election `" + name + "` with title ***" + e->title + "*** in <#" + channel + ">.");
		} else if (mode == "end") {
			if (!argc) {
				return this->error("Run with a election name obtained from `elections list`, and optionally `true` or `false`.");
			}

			std::string name = "";
			bool results = true;
			try {
				this->parseFilterName(&name, input.args, argn++);
				this->parseBool(&results, input.args, argn++);
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			Election* e = elections.get(name, input.userData.server);
			if (e == nullptr) {
				return this->error("Election `" + name + "` not found.");
			}
			if (!e->canVote()) {
				return this->error("Election `" + name + "` not running.");
			}

			if (!e->finish()) {
				return this->error("Failed to post results in <#" + e->channel + ">!");
			}
			this->can(input, "end", true);

			return helperOutput("⚖️ End Election", "Election `" + name + "` has ended" + (results ? " and the results have been posted in <#" + e->channel + ">." : "."));
		} else if (mode == "remove") {
			if (!argc) {
				return this->error("Run with a election name obtained from `elections list false true true`.");
			}

			std::string name = "";

			try {
				this->parseFilterName(&name, input.args, argn++);
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			if (elections.remove(name, input.userData.server)) {
				this->can(input, "remove", true);
				return helperOutput("⚖️ Remove Election", "Removed election `" + name + "`.");
			}
			return this->error("Unused election `" + name +  "` not found.");
		} else if (mode == "run") {
			if (argc < 3) {
				return this->error("Run with a election name obtained from `elections list false true false`, an emoji and a candidate name.");
			}

			std::string elec = "", emoji = "", name = "";

			try {
				this->parseFilterName(&elec, input.args, argn++);
				this->parseEmoji(&emoji, input.args, argn++);
				input.args.erase(input.args.begin(), input.args.begin() + argn);
				name = concat(input.args, " ");
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			Election* e = elections.get(elec, input.userData.server);
			if (e == nullptr) {
				return this->error("Election `" + name + "` not found.");
			}
			if (e->started && !e->ended) {
				return this->error("Nice try Putin.");
			}

			// Update the existing candidate if possible.
			e->candidates[emoji] = ElectionCandidate(name, emoji, 0);
			return helperOutput("⚖️Election - Run", "Set candidate `" + name + "` for emoji " + emojiToString(emoji) + ".");
		} else if (mode == "ban") {
			if (argc < 2) {
				return this->error("Run with a election name obtained from `elections list false true false`, and an emoji.");
			}

			std::string name = "", emoji = "";

			try {
				this->parseFilterName(&name, input.args, argn++);
				this->parseEmoji(&emoji, input.args, argn++);
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			return this->error("wip"); // TODO
		} else if (mode == "info") {
			if (!argc) {
				return this->error("Run with a election name obtained from `elections list`.");
			}

			std::string name = "";

			try {
				this->parseFilterName(&name, input.args, argn++);
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			Election* election = elections.get(name, input.userData.server);
			if (election == nullptr) {
				return this->error("Election `" + name + "` not found.");
			}
			this->can(input, "info", true);

			AwokenDiscord::Embed embed;
			embed.title = "⚖️ Election `" + name + "` info";
			std::vector<AwokenDiscord::EmbedField> fields;

			embed.description = election->title;

			if (election->canVote()) {
				addField(fields, "Running", "This election can be voted on!");
			}

			if (election->started) {
				addField(fields, "Start", "Started at " + timestampStr("", election->started) + " UTC in <#" + election->channel + ">");
			}

			if (election->ended) {
				addField(fields, "End", "Ended at " + timestampStr("", election->ended) + " UTC");
			} else if (election->endsAfter) {
				addField(fields, "End", election->started
					? "Ends at " + timestampStr("", election->endsAfter + election->started) + " UTC"
					: "Ends " + timeToDate(election->endsAfter, 2) + " after it starts");
			}

			if (election->candidates.size()) {
				addField(fields, "Candidates", election->getWinners());
			}

			addField(fields, "Permissions", "`elections." + name + ".vote`");

			embed.fields = fields;
			embed.color = Colours::Discord::YELLOW;
			return CommandOutput("", "nice try obama", embed);
		}
		return this->error("Usage: " + this->getUsage());
	}

	std::string CommandElections::getName() {
		return "elections";
	}

	std::string CommandElections::getUsage() {
		return this->getName() + " *[__list__|create|start|end|remove|run|ban|info]* ...";
	}

	CommandOutput CommandElections::getHelp(std::string serverID) {
		AwokenDiscord::Embed embed;
		embed.title = this->getUsage();
		std::vector<AwokenDiscord::EmbedField> fields;

		addField(fields, "List", R"(__Usage: **list** *[running = true] [created = false] [ended = false]*__
Lists all elections, with names for `elections start|remove|finish`.)" + this->messageRing(serverID, "list", "\nListing requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "Create", R"(__Usage: **create <name> <title>** *[ends = never]*__
__**Arguments for this are seperated by newlines, not spaces!**__
*(If you want to use a newline in an argument, write `\n` instead.)*
Creates a new election.
Title is displayed in the embed created with `elections start`.
If *ends* is specified, the election will automatically finish <ends> after it's started.)");
		addField(fields, "Start", R"(__Usage: **start <name>** *[channel = current]*__
Creates the message for an election and lets users vote on it.)" + this->messageRing(serverID, "start", "\nStarting elections requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "End", R"(__Usage: **end <name>** *[results = true]*__
Locks voting and ends the election.
Sends an info message in the elections channel for results if *results* is true.)" + this->messageRing(serverID, "end", "\nEnding elections requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "Remove", R"(__Usage: **remove <name>**__
Removes a finished election by its name.)" + this->messageRing(serverID, "remove", "\nRemoving elections requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "Run", R"(__Usage: **run <name> <emoji> <candidate name>**__
Creates a candidate for an election that hasn't started yet.
Emoji is reacted with to vote for the candidate.)" + this->messageRing(serverID, "run", "\nCreating candidates requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "Ban", R"(__Usage: **ban <name> [emoji = all]**__
Removes candidate(s) from an election that hasn't started yet.
If no emoji is specified, all candidates will be removed.)" + this->messageRing(serverID, "ban", "\nRemoving candidates requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "Info", "Prints all info on a election.\nTakes the same arguments as `remove`.");

		AwokenDiscord::EmbedFooter footer;
		footer.text = R"(Elections can only be voted on **once** by real humans.
The permission to vote on an election is `election.`**`NAME`**`.vote`.)";
		embed.footer = footer;
		embed.fields = fields;
		embed.description = "(*Default mode: __list__*";
		embed.color = Colours::Discord::DELTA;
		return CommandOutput("", "good luck :)", embed);
	}

	std::set<std::string> CommandElections::getTypes() {
		return {"admin", "utility"};
	}
}
