#include <commands/prefix.h>

#include <util/strings.h>

namespace CIABot {
	CommandOutput CommandPrefix::run(CommandInput& input) {
		std::string prefix = "";
		if (input.args.empty()) {
			prefix = prefixes.get("global", true);
		} else {
			prefix = concat(input.args, " ");
			if (prefix.size() > 64) {
				return this->error("Prefix must be at most 64 characters long.");
			}
		}

		prefixes.set(prefix, input.userData.server);
		return helperOutput("🤖 Server Prefix", "Server prefix set to `" + prefix + "`.");
	}

	std::string CommandPrefix::getName() {
		return "prefix";
	}

	std::string CommandPrefix::getUsage() {
		return this->getName() + " *[prefix]*";
	}

	CommandOutput CommandPrefix::getHelp(std::string serverID) {
		return helperHelp(this->getUsage(), R"(Sets CIABot's command prefix for this server.
If no prefix is specified it will be reset.)" + this->messageRing(serverID));
	}

	std::set<std::string> CommandPrefix::getTypes() {
		return {"admin"};
	}
}