#include <commands/help.h>

#include <regex>

#include <util/strings.h>

namespace CIABot {
	CommandOutput CommandHelp::run(CommandInput& input) {
		std::map<std::string, size_t> categories = {};
		std::map<std::string, std::set<Command*>> c2Commands = {};
		size_t commandsCount = 0;

		for (const auto& it : commands) {
			commandsCount++;
			Command* command = it.second;
			std::set<std::string> types = command->getTypes();
			types.insert("all");

			for (std::string type : types) {
				auto existing = categories.find(type);
				if (existing == categories.end()) {
					categories[type] = 1;
					c2Commands[type] = {command};
				} else {
					categories[type]++;
					c2Commands[type].insert(command);
				}
			}
		};

		AwokenDiscord::Embed embed;
		embed.color = Colours::Discord::ANTI_DELTA;

		if (input.args.size() > 0) {
			std::string helping = concat(input.args);
			const auto& commandIt = commands.find(helping);

			if (commandIt != commands.end()) {
				return commandIt->second->getHelp(input.userData.server);
			} else {
				std::string category = match(helping, "^\\((.+)\\)$");
				if (!category.empty()) {
					const auto& categoryIt = categories.find(category);
					if (categoryIt != categories.end()) {
						std::string ret = "__*" + count(categoryIt->second, "", "{COUNT}* commands", "The* command") + " in **" + category + "**__:";

						for (Command* command : c2Commands[category]) {
							ret += "\n`" + command->getName() + "` - " + command->getUsage();
						}

						embed.title = "📜 Commands in **" + category + "**:";
						embed.description = ret;
						return CommandOutput("", ret, embed);
					}
				}
			}
			return this->error("Invalid command or category.\n*Did you forget to enclose a category in **(**brackets**)**?*");
		}

		std::string ret = "**all**: " + count(c2Commands["all"].size(), "command") + ".\n";
		categories.erase("all"); // It's not what it looks like!!!
		for (auto it : categories) {
			ret += "\n- **" + it.first + "**: " + count(it.second, "command") + ".";
		}

		AwokenDiscord::EmbedFooter footer;
		footer.text = "Use help (category) to list its commands.";

		embed.title = "📜 Command categories:";
		embed.description = ret;
		embed.footer = footer;
		return CommandOutput("", ret, embed);
	}

	std::string CommandHelp::getName() {
		return "help";
	}

	std::string CommandHelp::getUsage() {
		return this->getName() + " *[command or **(**category**)**]*";
	}

	CommandOutput CommandHelp::getHelp(std::string serverID) {
		return helperHelp(this->getUsage(), R"(Returns help for a command or lists all available command categories for __CIABot__.
		Replace an optional field with **-** for its default value.
		If the command is surrounded by brackets it will be treated as a category.
		A category will list all commands in it. Commands can have multiple categories.
		You can also use **(all)** to list all commands.
		You can run a command inside **$()** to use its output as an argument to another command.
		Adding **[]** to the **$()** will turn it into a loop, put a number inside to make the command loop over.
		What usage means:
		- __<value>__: a value is required
		- *[value]*: a value is optional
		- `{`value`}`: a special execution level is required to set this value)");
	}

	std::set<std::string> CommandHelp::getTypes() {
		return {"info", "utility"};
	}
}