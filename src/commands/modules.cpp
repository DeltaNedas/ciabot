#include <commands/modules.h>

#include <modules.h>
#include <util/strings.h>

namespace CIABot {
	CommandOutput CommandModules::run(CommandInput& input) {
		std::string mode = "list";
		if (!input.args.empty()) {
			mode = lower(input.args[0]);
			input.args.erase(input.args.begin());
		}

		size_t argc = input.args.size();

		if (compare(mode, {"list", "load", "unload", "reload"})) {
			if (!this->can(input, mode)) {
				return this->permissionError(mode);
			}

			if (mode == "list") {
				char type = 0x01; // 0x01: Enabled, 0x02: Disabled, 0x03: All
				std::string typeS = "enabled";
				if (argc > 0) {
					typeS = input.args[0];
					if (compare(typeS, {"-", "enabled"})) {
					} else if (typeS == "disabled") {
						type = 0x02;
					} else if (typeS == "all") {
						type = 0x03;
					} else {
						return this->error("Module types must be `enabled`, `disabled` or `all`.");
					}
				}
				if (!this->can(input, "list." + typeS)) {
					return this->permissionError("list." + typeS);
				}

				AwokenDiscord::Embed embed;
				embed.color = Colours::Discord::DELTA;
				embed.title = "🔧 Modules list";
				std::vector<AwokenDiscord::EmbedField> fields;

				std::vector<std::string> enabledModules = {};
				std::vector<std::string> disabledModules = {};

				for (auto it : modules.values) {
					if (it.second) {
						auto mIt = loadedModules.find(it.first);
						std::string mod = it.first + " - *not loaded*";
						if (mIt != loadedModules.end()) {
							mod = it.first + " - " + mIt->second->getTitle();
						}
						enabledModules.push_back(mod);
					} else {
						disabledModules.push_back(it.first);
					}
				}

				if (enabledModules.empty() && disabledModules.empty()) {
					return helperOutput("🔧 Modules list", "There are no modules.");
				}

				if (!enabledModules.empty() && (type & 0x01) != 0) {
					addField(fields, count(enabledModules.size(), "Enabled Module"), concat(enabledModules, "\n"));
				}

				if (!disabledModules.empty() && (type & 0x02) != 0) {
					addField(fields, count(disabledModules.size(), "Disabled Module"), concat(disabledModules, "\n"));
				}

				embed.fields = fields;
				return CommandOutput("", std::to_string(enabledModules.size() + disabledModules.size()), embed);
			} else if (mode == "load") {
				if (argc == 0) {
					return this->error("Expected a module name.");
				}

				bool enable = true;
				if (argc > 1) {
					try {
						this->parseBool(&enable, input.args, argc - 1);
						input.args.erase(input.args.end());
					} catch (std::exception& e) {
						// It's part of a module name.
					}
				}
				std::string name = concat(input.args);

				bool enabled = false;
				auto it = modules.values.find(name);
				if (it != modules.values.end()) {
					enabled = it->second;
				}

				if (!enabled) {
					if (!enable) {
						return this->error("Module `" + name + "` is disabled.");
					}
					modules.set(true, name);
				}

				size_t old = timestamp();
				if (loadModule(name)) {
					this->error("Module `" + name + "` failed to load!");
				}
				return helperOutput("🔧 Module Loaded", "Loaded module in **" + std::to_string((timestamp() - old) / (size_t) 1e3) + "μs**.");
			} else if (mode == "unload") {
				if (argc == 0) {
					return this->error("Expected a module name.");
				}

				bool disable = false;
				if (argc > 1) {
					try {
						this->parseBool(&disable, input.args, argc - 1);
						input.args.erase(input.args.end());
					} catch (std::exception& e) {
						// It's part of a module name.
					}
				}
				std::string name = concat(input.args);

				auto it = modules.values.find(name);
				if (it == modules.values.end()) {
					return this->error("Module `" + name + "` not found.");
				}
				bool enabled = it->second;

				if (!enabled) {
						return this->error("Module `" + name + "` is already disabled.");
				} else if (disable) {
					modules.set(false, name);
				}

				size_t old = timestamp();
				if (unloadModule(name)) {
					this->error("Module `" + name + "` failed to unload!");
				}
				return helperOutput("🔧 Module Unloaded", "Unloaded module in **" + std::to_string((timestamp() - old) / (size_t) 1e3) + "μs**.");
			}
			if (argc == 0) {
				return this->error("Expected a module name.");
			}

			bool enable = true;
			if (argc > 1) {
				try {
					this->parseBool(&enable, input.args, argc - 1);
					input.args.erase(input.args.end());
				} catch (std::exception& e) {
					// It's part of a module name.
				}
			}
			std::string name = concat(input.args);

			bool enabled = false;
			auto it = modules.values.find(name);
			if (it != modules.values.end()) {
				enabled = it->second;
			}

			if (!enabled) {
				if (!enable) {
					return this->error("Module `" + name + "` is disabled.");
				}
				modules.set(true, name);
			}

			size_t old = timestamp();
			if (unloadModule(name)) {
				this->error("Module `" + name + "` failed to unload!");
			}
			if (loadModule(name)) {
				this->error("Module `" + name + "` failed to load!");
			}
			return helperOutput("🔧 Module Reloaded", "Reloaded module in **" + std::to_string((timestamp() - old) / (size_t) 1e3) + "μs**.");
		}
		return this->error("Usage: " + this->getUsage());
	}

	std::string CommandModules::getName() {
		return "modules";
	}

	std::string CommandModules::getUsage() {
		return this->getName() + " *[list|load|unload|reload = list]* ...";
	}

	CommandOutput CommandModules::getHelp(std::string serverID) {
		AwokenDiscord::Embed embed;
		embed.title = this->getUsage();
		embed.description = "*Default mode: __list__*";
		embed.color = Colours::Discord::DELTA;
		std::vector<AwokenDiscord::EmbedField> fields;

		addField(fields, "**List**", R"(__Usage: list *[enabled|disabled|all = enabled]*__
Lists modules with their titles.)" + this->messageRing(serverID, "list", "\nListing modules requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "**Load**", R"(__Usage: load **<module name> *[enable = true]***__
Loads a module by its name (not title).
Set enable to true to load a new module.
If a module is not enabled it will not be loaded.
Errors if a module is already loaded or fails to load.)" + this->messageRing(serverID, "load", "\nLoading modules requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "**Unload**", R"(__Usage: unload **<module name>** *[disable = false]*__
Unloads a module by its name (not title).
Set disable to true to stop it from being loaded on restart.
Errors if a module is already unloaded or fails to unload.)" + this->messageRing(serverID, "unload", "\nUnloading modules requires an execution level of **Ring-__{RING}__**."));
		addField(fields, "**Reload**", R"(__Usage: reload **<module name>**__
Unloads and loads a module by its name (not title).
If the module is not already loaded it won't be reloaded.
Errors if a module is not loaded or fails to reload.)" + this->messageRing(serverID, "reload", "\nReloading modules requires an execution level of **Ring-__{RING}__**."));
		return CommandOutput("", "good luck :)", embed);
	}

	std::set<std::string> CommandModules::getTypes() {
		return {"info", "owner"};
	}
}