#include <commands/ping.h>

#include <util/strings.h>

namespace CIABot {
	CommandOutput CommandPing::run(CommandInput& input) {
		std::string unitS = "millisecond";
		size_t unit = 1e6;

		// Parse first arg for ping precision
		if (!input.args.empty()) {
			if (!this->can(input, "precision")) {
				return this->permissionError("precision");
			}

			unitS = lower(input.args[0]);
			if (compare(unitS, {"s", "seconds"})) {
				unit = 1e9;
				unitS = "second";
			} else if (compare(unitS, {"ms", "milli", "milliseconds"})) {
				unit = 1e6;
				unitS = "milisecond";
			} else if (compare(unitS, {"us", "μs", "micro", "microseconds"})) {
				unit = 1e3;
				unitS = "microsecond";
			} else if (compare(unitS, {"ns", "nano", "nanoseconds"})) {
				unit = 1;
				unitS = "nanosecond";
			} else if (compare(unitS, {"ps", "pico", "picoseconds"})) { // ebin easter egg will be ruined by seplesples 29 if we're lucky
				// unit = 1e-3;
				// unitS = "picosecond";
				return this->error("Woah calm down, my clock isn't *that* precise!");
			} else {
				return this->getHelp(input.userData.server);
			}
		}

		size_t old = timestamp();
		system("/usr/bin/ping -c 5 www.discordapp.com");
		size_t ping = (timestamp() - old) / (5 * unit); // Convert 5x avg ping in nanoseconds to desired unit
		if (!ping) {
			return this->error("Failed to get ping!\n");
		}

		std::string ret = count(ping, unitS);
		return helperOutput("🏓 Pong!", "Ping to discord is **" + ret + "**.", ret);
	}

	std::string CommandPing::getName() {
		return "ping";
	}

	std::string CommandPing::getUsage() {
		return this->getName() + " *[units = s, **__ms__**, μs, ns]*";
	}

	CommandOutput CommandPing::getHelp(std::string serverID) {
		return helperHelp(this->getUsage(), R"(Returns the ping to discord, in a certain unit.
**Units** *(can be name or shorthand*:
> *s* - seconds
> *ms* - milliseconds (default)
> *μs* - microseconds
> *ns* - nanoseconds)" + this->messageRing(serverID));
	}

	std::set<std::string> CommandPing::getTypes() {
		return {"info", "utility"};
	}
}
