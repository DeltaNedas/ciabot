#include <commands/ring.h>

#include <util/strings.h>

namespace CIABot {
	CommandOutput CommandRing::run(CommandInput& input) {
		size_t argc = input.args.size();
		std::string user = "-";

		if (argc > 0) {
			try {
				this->parseUser(&user, input.args, 0, "-");
			} catch (std::exception& e) {
				return this->error("Invalid user.");
			}
		}

		if (compare(lower(user), {"-", "me"})) {
			user = input.userData.user;
		}

		if (user == input.userData.user) {
			if (!this->can(input, "get")) {
				return this->permissionError("get");
			}
			std::string ring = this->messageRing(input.userData.executionLevel, "Your execution level is **Ring-__{RING}__**.");
			if (ring.empty()) {
				ring = "You have no execution level.";
			}

			AwokenDiscord::Embed embed;
			embed.color = Colours::Discord::YELLOW;
			embed.title = "💍 Ring";
			embed.description = ring;

			this->can(input, "get", true);
			return CommandOutput("", std::to_string(input.userData.executionLevel), embed);
		}
		AwokenDiscord::User userObj;
		if (!getUser(user, &userObj)) {
			return this->error("User does not exist.");
		}

		if (argc == 1) {
			if (!this->can(input, "get.others")) {
				return this->permissionError("get.others");
			}

			execution_level_t level = executionLevels.get(user, input.userData.server, false);
			std::string ring = this->messageRing(level, "**" + userObj.username + "**'s execution level is **Ring-__{RING}__**.");
			if (ring.empty()) {
				ring = "**" + userObj.username + "** has no execution level.";
			}

			AwokenDiscord::Embed embed;
			embed.color = Colours::Discord::YELLOW;
			embed.title = "💍 Ring";
			embed.description = ring;

			this->can(input, "get.others", true);
			return CommandOutput("", std::to_string(level), embed);
		}

		execution_level_t set = DEFAULT_EXECUTION_LEVEL;
		std::string server = "-";
		try {
			this->parseRing(&set, input.args, 1);
			if (argc > 2) {
				this->parseSnowflake(&server, input.args, 2);
			}
		} catch (std::exception& e) {
			return this->error("Invalid execution level.");
		}

		std::string mode = "set";
		if (compare(lower(server), {"-", "current", "this", "here"})) {
			server = input.userData.server;
		}
		if (server != input.userData.server) {
			mode = "set.external";
		}
		if (!this->can(input, mode)) {
			return this->permissionError(mode);
		}

		executionLevels.set(set, user, server);
		std::string ring = this->messageRing(set, "**" + userObj.username + "**'s execution level is now **Ring-__{RING}__**.");
		if (ring.empty()) {
			ring = "Removed **" + userObj.username + "**'s execution level.";
		}

		AwokenDiscord::Embed embed;
		embed.color = Colours::Discord::YELLOW;
		embed.title = "💍 Ring";
		embed.description = ring;

		this->can(input, mode, true);
		return CommandOutput("", std::to_string(set), embed);
	}

	std::string CommandRing::getName() {
		return "ring";
	}

	std::string CommandRing::getUsage() {
		return this->getName() + " **<user = you>** *[execution level]* `{server = current}`";
	}

	CommandOutput CommandRing::getHelp(std::string serverID) {
		return helperHelp(this->getUsage(), "Shows your execution level" + this->messageRing(serverID, "get", ", this requires an execution level of **Ring-__{RING}__**") + ".\n"
+ "If a user is specified, it will shows their execution level" + this->messageRing(serverID, "get.others", ", this requires an execution level of **Ring-__{RING}__**") + ".\n"
+ "If a execution level is also specified it will be set" + this->messageRing(serverID, "set", ", this requires an execution level of **Ring-__{RING}__**") + ".\n"
+ "If a server is specified you can set it externally" + this->messageRing(serverID, "set.external", ", this requires an execution level of **Ring-__{RING}__**") + ".");
	}

	std::set<std::string> CommandRing::getTypes() {
		return {"admin", "info"};
	}
}