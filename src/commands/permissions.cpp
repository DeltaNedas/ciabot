#include <commands/permissions.h>

#include <info.h>
#include <permissions.h>
#include <util/discord.h>
#include <util/strings.h>
#include <util.h>

namespace CIABot {
	CommandOutput CommandPermissions::run(CommandInput& input) {
		std::string mode = "list";
		if (input.args.size()) {
			mode = lower(input.args[0]);
			input.args.erase(input.args.begin());
		}

		size_t argc = input.args.size();
		size_t argn = 0;
		if (mode == "list") {
			if (!this->can(input, "list")) {
				return this->permissionError("list");
			}

			std::string server = "-";
			std::string action = "list";
			try {
				this->parseSnowflake(&server, input.args, argn++, "-");
			} catch (std::exception& e) {
				return this->error(e.what());
			}

			if (server == "-") {
				server = input.userData.server;
			}

			if (server != input.userData.server) {
				action += ".external";
				if (!this->can(input, action)) {
					return this->permissionError(action);
				}
			}

			this->can(input, action, true); // Potentially pay rupees for it, if it was set up to do so.
			std::map<std::string, Permission*> serverPermissions = permissions.get(server, true);
			if (serverPermissions.empty()) {
				return helperOutput("🔐 Permissions list", "There are no permissions??? That can't be right!", "0");
			}

			std::string output = "`name`: *execution level* - *rupees* - *rupees cost*\n";
			for (auto& it : serverPermissions) {
				std::string name = it.first;
				Permission* perm = it.second;
				output = output + "\n`" + name + "`: " +
					std::string(perm->executionLevel == -1 ? "*None*" : ("**Ring-__" + std::to_string(perm->executionLevel) + "__**")) +
					" - " + std::to_string(perm->rupees) + " - " + std::to_string(perm->rupeesCost);
			}

			return helperOutput("🔐 Permissions list", output, std::to_string(serverPermissions.size()));
		} else if (mode == "set") {
			if (!this->can(input, "set")) {
				return this->permissionError("set");
			}
			// Resplit args by newline for readibility and to prevent pulling out of hair with escaping
			// Use \n to use newline in argument
			input.args = replace(
				split(
					concat(input.args, " "),
					"\n"),
				"\\n",
				"\n");
			size_t argc = input.args.size();
			if (argc == 0) {
				return this->error("Run with a permission name and optional settings.");
			}

			if (argc > 10) {
				if (!this->can(input, "set.external")) {
					return this->permissionError("set.external");
				}
			}

			std::string name = input.args[argn++];
			execution_level_t executionLevel = DEFAULT_EXECUTION_LEVEL;
			rupees_t rupees = 0;
			rupees_t rupeesCost = 0;
			size_t deleteAfter = 0;
			size_t cooldown = DEFAULT_COOLDOWN;
			size_t created = 0;
			size_t joined = 0;
			std::vector<std::string> userBlacklist = {};
			std::vector<std::string> userWhitelist = {};
			std::vector<std::string> channelBlacklist = {};
			std::vector<std::string> channelWhitelist = {};
			std::string server = input.userData.server;

			if (argc > 1) { // Parse optional input
				try {
					this->parseRing(&executionLevel, input.args, argn++);
					this->parseDouble((double*) &rupees, input.args, argn++);
					this->parseDouble((double*) &rupeesCost, input.args, argn++);
					this->parseTime(&deleteAfter, input.args, argn++);
					this->parseTime(&cooldown, input.args, argn++, DEFAULT_COOLDOWN);
					this->parseTime(&created, input.args, argn++);
					this->parseTime(&joined, input.args, argn++);
					this->parseUsers(&userBlacklist, input.args, argn++);
					this->parseUsers(&userWhitelist, input.args, argn++);
					this->parseChannels(&channelBlacklist, input.args, argn++);
					this->parseChannels(&channelBlacklist, input.args, argn++);
					this->parseSnowflake(&server, input.args, argn++);
					if (compare(server, {"", "-"})) {
						server = input.userData.server;
					}
				} catch (std::exception& e) {
					if (strlen(e.what())) { // Empty errors are ignored
						return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
					}
				}
			}

			Permission* old = permissions.get(name, server, true);
			if (old == nullptr) {
				return this->error("`" + name + "` does not name a valid permission.");
			}

			if (cooldown < MINIMUM_COOLDOWN || cooldown > MAXIMUM_COOLDOWN) {
				return this->error("Invalid cooldown.");
			}

			if (!permissions.has(name, UserData(), server, false)) {
				return this->error("You need to have a permission to change it.");
			}
			if (executionLevel != -1 && executionLevel < input.userData.executionLevel) {
				return this->error("That would lock you out of `" + name + "`.");
			}
			for (std::string user : userBlacklist) {
				execution_level_t userExec = executionLevels.get(user, server);
				if (input.userData.executionLevel && userExec <= input.userData.executionLevel && userExec != DEFAULT_EXECUTION_LEVEL) {
					return this->error("You cannot blacklist someone with a higher execution level.");
				}
			}
			if (input.userData.executionLevel && rupeesCost && !match(name, "^permissions\\.(?:set|reset)").empty()) {
				return this->error("That could lock you out of fixing permissions!");
			}

			// literal botnet wow
			printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tSet permissions for %s in server %s:\nExecution Level: %d\nRupees: %f\nRupees Cost: %f\n",
				Colours::BLUE, Colours::RESET,
				name.c_str(), server.c_str(), (int) executionLevel, rupees, rupeesCost);

			// Actually make the permission
			Permission* perm = new Permission(executionLevel, rupees, rupeesCost, deleteAfter, cooldown, created, joined, {
					{"users", userBlacklist},
					{"channels", channelBlacklist}
				}, {
					{"users", userWhitelist},
					{"channels", channelWhitelist}
				}); // No memory leak because it's deleted on shutdown
			permissions.set(perm, name, server);
			return helperOutput("🔐 Set Permissions", "Set " + std::string(server == input.userData.server ? "" : "external ") + "permissions for `" + name + "`.", name);
		} else if (mode == "reset") {
			if (!this->can(input, "reset")) {
				return this->permissionError("reset");
			}

			if (argc == 0) {
				return this->error("Run with a permission name.");
			} else if (argc > 1 && !this->can(input, "reset.external")) {
				return this->permissionError("reset.external");
			}

			std::string name = "";
			std::string server = input.userData.server;

			try {
				this->parseString(&name, input.args, argn++);
				this->parseSnowflake(&server, input.args, argn++);
				if (compare(server, {"", "-"})) {
					server = input.userData.server;
				}
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			Permission* old = permissions.get(name, server, true);
			if (old == nullptr) {
				return this->error("`" + name + "` does not name a valid permission.");
			}
			if (permissions.has(name, input.userData, server, false)) {
				return this->error("You need to have a permission to change it.");
			}

			if (!permissions.reset(name, server)) {
				return helperOutput("🔐 Reset Permissions", "Reset " + std::string(server == input.userData.server ? "" : "External ") + "Permissions for `" + name + "`.");
			}
			// Race condition?
			return this->error("`" + name + "` does not name a valid permission.");
		} else if (mode == "info") {
			if (!this->can(input, "info")) {
				return this->permissionError("info");
			}
			if (argc == 0) {
				return this->error("Run with a permission name.\nFor commands it is `command.action` *e.g. `help.run`*");
			}

			std::string name = "";
			std::string server = "-";
			std::string action = "info";

			try {
				this->parseString(&name, input.args, argn++);
				if (argc > 1) {
					if (!this->can(input, "info.external")) {
						return this->permissionError("info.external");
					}
					this->parseSnowflake(&server, input.args, argn++);
					action = "info.external";
				}
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			if (compare(server, {"", "-"})) {
				server = input.userData.server;
			}

			if (server == input.userData.server) {
				action = "info";
			}

			Permission* perm = permissions.get(name, server, true);
			if (perm == nullptr) {
				return this->error("`" + name + "` does not name a valid permission.");
			}

			std::string output = "Permission `" + name + "`:";
			AwokenDiscord::Embed embed;
			embed.title = "🔐 Permission info for " + std::string(server == input.userData.server ? "" : "External ") + output;
			embed.color = Colours::Discord::DELTA;
			std::vector<AwokenDiscord::EmbedField> fields;

			if (perm->executionLevel != -1) {
				std::string execStr = "**Ring-__" + std::to_string(perm->executionLevel) + "__**";
				output += "\n- Execution Level: " + execStr;
				addField(fields, "Execution Level", execStr);
			}

			if (perm->rupees) {
				output += "\n- Rupees: " + std::to_string(perm->rupees);
				addField(fields, "Rupees", std::to_string(perm->rupees));
			}

			if (perm->rupeesCost) {
				output += "\n- Rupees Cost: " + std::to_string(perm->rupeesCost);
				addField(fields, "Rupees Cost", std::to_string(perm->rupeesCost));
			};

			if (perm->cooldown) {
				output += "\n- Cooldown: " + std::to_string(perm->rupeesCost);
				addField(fields, "Cooldown", timeToDate(perm->cooldown));
			};

			if (perm->joined) {
				std::string joined = "After " + timeToDate(perm->joined);
				output += "\n- Joined: " + joined;
				addField(fields, "Joined", joined);
			}

			if (perm->created) {
				std::string created = "After " + timeToDate(perm->created);
				output += "\n- Created: " + created;
				addField(fields, "Created", created);
			}

			for (auto& it : perm->blacklists) {
				if (it.second.size() != 0) {
					std::string type = it.first + " Blacklist";
					std::string list = concat(it.second, ", ");
					output += "\n- " + type + ": " + list;
					addField(fields, type, list);
				}
			}
			for (auto& it : perm->whitelists) {
				if (it.second.size() != 0) {
					std::string type = it.first + " Whitelist";
					std::string list = concat(it.second, ", ");
					output += "\n- " + type + ": " + list;
					addField(fields, type, list);
				}
			}

			embed.fields = fields;
			this->can(input, action, true);
			return CommandOutput("", output, embed);
		}
		return this->error("Usage: " + this->getUsage());
	}

	std::string CommandPermissions::getName() {
		return "permissions";
	}

	std::string CommandPermissions::getUsage() {
		return this->getName() + " *[__list__|set|reset|info]* ...";
	}

	CommandOutput CommandPermissions::getHelp(std::string serverID) {
		return helperHelp(this->getUsage(),
		R"(**List**:
>    Lists all permissions and some values.
>    Takes 1 argument, `{server = current}`.
**Set**:
>    Sets requirements for a permission.
>    __**Arguments for this command are seperated by newlines, not spaces!**__
>    *If you want to use a newline in an argument, write `\n` instead.*
>    Takes these arguments: **<name> <execution level = none>** *[rupees = 0] [rupee cost = 0] [delete after > 0s] [created = 0] [joined = 0] [user blacklist] [user whitelist] [channel whitelist] [channel blacklist]* `{server = current}`
>    The name, for commands, is `command.action`, for example: `roll.run`.
>    If the triggering message is set to delete after less than 1 second, it will not be deleted.
>    Created and joined are minimum time requirements for accounts after being created or joing the server respectively.
>    Whitelists and blacklists are comma seperated lists of user or channel ids that the permission is automatically granted or denied in.
>    For filters the name is `filters.`**`NAME`**`.trigger`, use `default` as a name for the default filter's permissions.
>    Rupee cost is taken away from a users balance, set to a negative number to increase rupees when permission is applied.
>    *Permissions are **never** applied unless the command/filter is successfully used.*
>    **__Permissions can only be set if you can afford them!__**
**Reset**:
>    Resets a permission by its name.
>    Takes 2 arguments: **<name>** `{server = current}`
**Info**:
>    Prints all info for a permission.
>    Takes the same arguments as `permissions remove`.)");
	}

	std::set<std::string> CommandPermissions::getTypes() {
		return {"admin", "utility"};
	}
}
