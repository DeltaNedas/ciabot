#include <commands/filters.h>

#include <filters.h>
#include <info.h>
#include <permissions.h>
#include <util/discord.h>
#include <util/strings.h>
#include <util.h>

namespace CIABot {
	CommandOutput CommandFilters::run(CommandInput& input) {
		std::string mode = "list";
		if (!input.args.empty()) {
			mode = lower(input.args[0]);
			input.args.erase(input.args.begin());
		}

		if (!this->can(input, mode)) {
			return this->permissionError(mode);
		}

		size_t argc = input.args.size();
		size_t argn = 0;
		if (mode == "list") {
			std::string server = "-";
			std::string action = "list";
			if (argc > 0) {
				try {
					this->parseSnowflake(&server, input.args, argn++, "-");
				} catch (std::exception& e) {
					return this->error(e.what());
				}
			}

			if (server == "-") {
				server = input.userData.server;
			}

			if (server != input.userData.server) {
				action += ".external";
				if (!this->can(input, action)) {
					return this->permissionError(action);
				}
			}

			this->can(input, action, true); // Potentially pay rupees for it, if it was set up to do so.
			std::map<std::string, Filter*> serverFilters = filters.get(server, true);
			if (serverFilters.empty()) {
				return helperOutput("🤐 Filter list", "There are no filters set up.", "0");
			}

			std::string output = "__**`Filter name`** - `Filter input` - *Filter output*__";
			for (auto& it : serverFilters) {
				Filter* filter = it.second;
				if (filter != nullptr) {
					output = output + "\n**`" + it.first + "`** -  `" + clamp(filter->input, 15) + "` - *" + clamp(filter->output, 15) + "*";
				}
			}

			return helperOutput("🤐 Filter list", output, std::to_string(serverFilters.size()));
		} else if (mode == "set") {
			// Resplit args by newline for readibility and to prevent pulling out of hair with escaping
			// Use \n to use newline in argument
			input.args = replace(
				split(
					concat(input.args, " "),
					"\n"),
				"\\n",
				"\n");
			argc = input.args.size();
			if (argc < 2) {
				return this->error("Run with a filter name, input and optional settings.");
			}

			std::string name = "", filterInput = "", filterOutput = "",
				urlInput = "", urlOutput = "", channel = "",
				server = input.userData.server;
			unsigned char flags = FilterFlags::NONE | FilterFlags::USER;

			try { // Catch invalid argument errors for flags and stuff
				this->parseFilterName(&name, input.args, argn++);
				this->parseString(&filterInput, input.args, argn++);
				this->parseString(&filterOutput, input.args, argn++);
				this->parseChannel(&channel, input.args, argn++);
				if (channel == "-") {
					channel = "";
				}

				this->parseFlag(&flags, FilterFlags::BOT, input.args, argn++);
				this->parseFlag(&flags, FilterFlags::USER, input.args, argn++, true);
				this->parseFlag(&flags, FilterFlags::DELETE, input.args, argn++);
				this->parseFlag(&flags, FilterFlags::KICK, input.args, argn++);
				this->parseFlag(&flags, FilterFlags::BAN, input.args, argn++);

				this->parseString(&urlInput, input.args, argn++);
				this->parseString(&urlOutput, input.args, argn++);
				this->parseSnowflake(&server, input.args, argn++, "-");
			} catch (std::exception& e) {
				if (strlen(e.what())) { // Empty errors are ignored
					return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
				}
			}

			if (server == "-") {
				server = input.userData.server;
			}

			std::string action = "set";
			if (server != input.userData.server) {
				action += ".external";
				if (!this->can(input, action)) {
					return this->permissionError(action);
				}
			}

			this->can(input, action, true);

			// literal botnet wow
			printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tSet filter %s for server %s:\nInput: %s\nOutput: %s\nFlags: %02X\nOutput channel: %s\n",
				Colours::BLUE, Colours::RESET,
				name.c_str(),
				server.c_str(), filterInput.c_str(), filterOutput.c_str(), flags,
				channel.empty() ? "<current>" : channel.c_str());

			// Actually make the filter
			Filter* filter = new Filter(filterInput, filterOutput, flags, channel, urlInput, urlOutput); // No memory leak because it's deleted on shutdown
			bool updated = filters.remove(name, server);
			filters.set(filter, name, server);
			return helperOutput("🤐 Set Filter", "Set " + std::string(updated ? "" : "new ") + std::string(server == input.userData.server ? "" : "external ") + "filter with output `" + filterOutput + "`.");
		} else if (mode == "remove") {
			if (!argc) {
				return this->error("Run with a filter name obtained from `filters list`.");
			}

			std::string name = "";
			std::string server = "-";
			std::string action = "remove";

			try {
				this->parseFilterName(&name, input.args, argn++);
				this->parseSnowflake(&server, input.args, argn++, "-");
			} catch (std::exception& e) {
				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			if (server == "-") {
				server = input.userData.server;
			}

			bool external = server != input.userData.server;
			if (external) {
				action += ".external";
				if (!this->can(input, action)) {
					return this->permissionError(action);
				}
			}

			if (filters.remove(name, server)) {
				this->can(input, action, true);
				return helperOutput("🤐 Remove Filter", "Removed " + std::string(external ? "external " : "") + "filter `" + name + "`.");
			}
			return this->error("Filter `" + name +  "` not found.");
		} else if (mode == "info") {
			if (!argc) {
				return this->error("Run with a filter name obtained from `filters list`.");
			}

			std::string name = "";
			std::string server = "-";
			std::string action = "info";

			try {
				this->parseFilterName(&name, input.args, argn++);
				this->parseSnowflake(&server, input.args, argn++, "-");
			} catch (std::exception& e) {
				std::string error(e.what());

				return this->error("Invalid argument #" + std::to_string(argn) + ": " + std::string(e.what()));
			}

			if (server == "-") {
				server = input.userData.server;
			}

			bool external = server != input.userData.server;
			if (external) {
				action += ".external";
				if (!this->can(input, action)) {
					return this->permissionError(action);
				}
			}

			Filter* filter = filters.get(name, server);
			if (filter == nullptr) {
				return this->error("Filter `" + name + "` not found.");
			}
			this->can(input, action, true);

			std::string output = "Filter `" + name + "`:";
			AwokenDiscord::Embed embed;
			embed.title = "🤐 Filter Info for " + std::string(external ? "External " : "") + output;
			std::vector<AwokenDiscord::EmbedField> fields;

			output += "\n- Input: " + filter->input;
			addField(fields, "Input", filter->input);
			if (!filter->output.empty()) {
				output += "\n- Output: " + filter->output;
				addField(fields, "Output", filter->output);
			}

			std::string binary = toBinary((size_t) filter->flags);
			output += "\n- Flags: " + binary;
			addField(fields, "Flags", binary);

			std::string channel = filter->channel.empty() ? "*automatic*" : ("<#" + filter->channel + ">");
			output += "\n- Channel: " + channel;
			addField(fields, "Channel", channel);

			if (!filter->urlInput.empty()) {
				output += "\n- URL Input: " + filter->urlInput;
				addField(fields, "URL Input", filter->urlInput);
			}
			if (!filter->urlOutput.empty()) {
				output += "\n- URL Output: " + filter->urlOutput;
				addField(fields, "URL Output", filter->urlOutput);
			}

			output += "\n- Permissions: `filters." + name + ".trigger";
			addField(fields, "Permissions", "`filters." + name + ".trigger`");

			embed.fields = fields;
			embed.color = Colours::Discord::YELLOW;
			return CommandOutput("", output, embed);
		}
		return this->error("Usage: " + this->getUsage());
	}

	std::string CommandFilters::getName() {
		return "filters";
	}

	std::string CommandFilters::getUsage() {
		return this->getName() + " *[__list__|set|remove|info]* ...";
	}

	CommandOutput CommandFilters::getHelp(std::string serverID) {
		AwokenDiscord::Embed embed;
		embed.title = this->getUsage();
		std::vector<AwokenDiscord::EmbedField> fields;

		addField(fields, "List", R"(__Usage: **list** *`{server = current}`*__
Lists all filters, with names for `filters remove|list`.)" + this->messageRing(serverID, "list", "\nListing requires an execution level of **Ring-__{RING}__**.") +
"\nOptionally takes a server argument" + this->messageRing(serverID, "list.external", ", which requires an execution level of **Ring-__{RING}__**") + ".");
		addField(fields, "Set", R"(__Usage: **set <name> <input>** *[output = none] [output channel = current] [bots = false] [users = true] [delete message = false] [kick user = false] [ban user = false] `{url input} {url output} {server = current}`*__
__**Arguments for this are seperated by newlines, not spaces!**__
Sets a filter, replacing any existing one.
*If you want to use a newline in an argument, write `\n` instead.*
The input is a regular expression without any outer slashes, it is tried on a JSON representation of the message.
Message example: `{"content":"sup bros","tts":true}`
Output is a json message, can contain "content" [string] (default if not json), "embed" [object] and "tts" [bool]
Bots and users must be enabled for one to be able to trigger the filter.
Delete message will delete the message before sending the output.
Set output channel to `-` or omit it to make it output in filtered message's channel.
URL I/O pulls data from "output" and sets actual output respectively.)");
		addField(fields, "Set - Output", R"(Output can contain the following variables:
- {*number*} - *n*th capture of the input, 0 is the message, 1 is the first **(**capture**)**, etc.
- {ID}, {PING}, {USERNAME}, {DISCRIMINATOR} - the user's id, ping, username, discriminator
- {RUPEES}, {RUPEES_AFTER} - rupees before and after modification (if any)
- {TIME} The current time in 12-hour UTC, format is `YYYY/MM/DD - hh:mm:ss `*`AM/PM`*)
Delete, Kick and Ban user are obvious. Use with caution!
)" + this->messageRing(serverID, "set", "Setting filters requires an execution level of **Ring-__{RING}__**.\n")
+ "Optionally takes a server argument" + this->messageRing(serverID, "set.external", ", which requires an execution level of **Ring-__{RING}__**") + ".");
		addField(fields, "Remove", R"(__Usage: **remove <name>** *`{server = current}`*__
Removes a filter by its name.
)" + this->messageRing(serverID, "remove", "Removing filters requires an execution level of **Ring-__{RING}__**.\n")
+ "Optionally takes a server argument" + this->messageRing(serverID, "remove.external", ", which requires an execution level of **Ring-__{RING}__**"));
		addField(fields, "Info", "Prints all info on a filter.\nTakes the same arguments as `remove`.");

		embed.fields = fields;
		embed.description = "*Default mode: __list__*";
		embed.color = Colours::Discord::DELTA;
		return CommandOutput("", "good luck :)", embed);
	}

	std::set<std::string> CommandFilters::getTypes() {
		return {"admin", "utility"};
	}
}
