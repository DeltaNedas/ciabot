#include <colours.h>

namespace Colours {
	const char* RESET = "\033[0m";
	const char* BOLD = "\033[1m";
	const char* LIGHT = "\033[2m";
	const char* ITALIC = "\033[3m";

	const char* UNDERLINE = "\033[4m";
	const char* OVERLINE = "\033[53m";
	const char* SLOW_BLINK = "\033[5m";
	const char* FAST_BLINK = "\033[6m";

	const char* CONCEAL = "\033[8m";
	const char* REVEAL = "\033[28m";
	const char* CROSSED = "\033[9m";
	const char* UNCROSSED = "\033[28m";

	const char* INVERT = "\033[7m";

	const char* BLACK = "\033[30m";
	const char* RED = "\033[31m";
	const char* GREEN = "\033[32m";
	const char* YELLOW = "\033[33m";
	const char* BLUE = "\033[34m";
	const char* MAGENTA = "\033[35m";
	const char* CYAN = "\033[36m";
	const char* WHITE = "\033[37m";
	const char* CUSTOM = "\033[38;2;"; // Propend "m" yourself
	const char* DEFAULT = "\033[39m";

	const char* BLACK_BG = "\033[40m";
	const char* RED_BG = "\033[41m";
	const char* GREEN_BG = "\033[42m";
	const char* YELLOW_BG = "\033[43m";
	const char* BLUE_BG = "\033[44m";
	const char* MAGENTA_BG = "\033[45m";
	const char* CYAN_BG = "\033[46m";
	const char* WHITE_BG = "\033[47m";
	const char* CUSTOM_BG = "\033[48;2;"; // Propend "m" yourself
	const char* DEFAULT_BG = "\033[49m";

	namespace Discord {
		unsigned int BLACK = 0;
		unsigned int BLUE = 255;
		unsigned int BROWN = 10181180;
		unsigned int DARK_BLUE = 170;
		unsigned int GREEN = 65280;
		unsigned int PEPE = 5868605;
		unsigned int YELLOW = 16776960;
		unsigned int MAGENTA = 16711850;
		unsigned int CYAN = 65535;
		unsigned int ORANGE = 13412864;
		unsigned int PINK = 16755370;
		unsigned int WHITE = 16777215;
		unsigned int RED = 16666666;

		unsigned int DELTA = 43775;
		unsigned int ANTI_DELTA = 65450;
	}
}