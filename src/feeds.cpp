#include <feeds.h>

#include <colours.h>
#include <events.h>
#include <info.h>
#include <options.h>
#include <util.h>

namespace CIABot {
	void checkFeeds() {
		// For all feeds:
		// 	Check for latest
		// 	For all servers using feed, push
		// 	If channel doesnt exist, clean
	}

	void feedsLoop(size_t delta) {
		size_t old = timestamp();
		printInfo(InfoLevel::FEED, "[%sFEED%s]\tChecking feeds, delta time is %lu...\n",
			Colours::BLUE, Colours::RESET, delta);
		checkFeeds();
		printInfo(InfoLevel::FEED | InfoLevel::TIME, "[%sFEED.TIME%s]\tFeeds took %s to check.\n",
			Colours::BLUE, Colours::RESET,
			timeToDate(timestamp() - old).c_str());
		tasks->add(feedsLoop, FEED_CHECK_INTERVAL);
	}
}
