#include <threads.h>

#include <cxxabi.h>
#include <execinfo.h> // GLibC only, sorry muslboys
#include <stdlib.h>
#include <string.h>
#include <exception>

#include <commands.h>
#include <console.h>
#include <events.h>
#include <util/strings.h>

namespace CIABot {
	bool alreadySaved = false;
	void quit(int code, bool save) {
		if (save && !alreadySaved) {
			alreadySaved = true;
			saveData();
			unloadCommands();
		}

		if (client != nullptr) {
			client->quit();
		}
		if (tasks != nullptr) {
			tasks->stop();
		}
		if (console != nullptr) {
			console->stop();
		}

		for (std::thread* aux : auxiliaryThreads) {
			if (aux != nullptr) {
				if (aux->get_id() != std::this_thread::get_id()) {
					// delete(aux);
				}
			}
		}

		// deleteThread(clientThread);
		if (code == 0) {
			return;
		}
		exit(code);
	}

	int handled = 0;
	void handleSignal(int signo) {
		if (console != nullptr && console->running) {
			console->stop();
		}
		if (handled < 2) {
			handled++;
			quit(0);
		} else {
			printf("\n%sForce quitting!%s\n", Colours::RED, Colours::RESET);
			exit(-1);
		}
	}

	int errorsHandled = 0;
	void handleError(int signo) {
		if (console != nullptr && console->running) {
			console->stop();
		}

		fprintf(stderr, "[%sERROR%s]\tEncountered a %s%s%s! Stack trace: %s\n",
			Colours::RED, Colours::RESET,
			Colours::RED, strsignal(signo), Colours::RESET,
			getBacktrace().c_str());

		if (errorsHandled == 0) {
			errorsHandled++;
			quit(signo);
		} else {
			printf("\n%sForce quitting due to repeated errors!%s\n", Colours::RED, Colours::RESET);
			exit(-1);
		}
	}

	std::string getBacktrace(size_t limit) {
		char** buffers;
		void** symbols = new void*[limit];
		std::vector<std::string> ret = {};

		int level = backtrace(symbols, limit);
		buffers = backtrace_symbols(symbols, level);
		// Get symbols from the addresses, if compiled with "-g" it will show function names and line numbers.
		delete[] symbols;
		if (buffers == NULL) {
			return Colours::RED + std::string("Failed to get backtrace symbols!") + Colours::RESET;
		}

		for (int i = 0; i < level; i++) {
			int status;
			char* buff = abi::__cxa_demangle(buffers[i], 0, 0, &status);
			if (buff == NULL) {
				buff = buffers[i];
			}
			ret.push_back("\t#" + std::to_string(i + 1) + ": " + std::string(buff));
		}

		return "\t" + concat(ret, "\n");
	}

	std::thread* consoleThread;
	std::thread* clientThread;
	std::thread* tasksThread; // Empty threads at start

	std::vector<std::thread*> auxiliaryThreads = {};
}
