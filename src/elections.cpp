#include <elections.h>

#include <client.h>
#include <util.h>
#include <util/discord.h>
#include <util/generics.h>
#include <util/strings.h>

#include <awoken_discord/emoji.h>

namespace CIABot {
	ElectionCandidate::ElectionCandidate(std::string name, std::string emoji, size_t votes) {
		this->name = name;
		this->emoji = emoji;
		this->votes = votes;
	}

	Election::Election(std::string channel, std::string message, std::string name,
			std::string title, size_t endsAfter, size_t started, size_t ended,
			std::vector<std::string> voters, std::map<std::string, ElectionCandidate> candidates) {
		this->channel = channel;
		this->message = message;
		this->name = name;
		this->title = title;
		this->endsAfter = endsAfter;
		this->started = started;
		this->ended = ended;
		this->voters = voters;
		this->candidates = candidates;
	}

	bool Election::canVote() {
		return this->started && !this->ended;
	}

	bool Election::finish(bool postResults) {
		this->ended = timestamp();

		if (postResults) {
			AwokenDiscord::Embed ret;
			ret.title = "Election results";
			ret.description = this->title;
			AwokenDiscord::EmbedField field;
			field.name = "Candidates";
			field.value = this->getWinners();
			ret.fields.push_back(field);
			ret.footer.text = "Election lasted " + timeToDate(this->ended - this->started, 2);
			return sendMessage(this->channel, json({{"embed", ret}})).size();
		}
		return true;
	}

	bool Election::addReactions() {
		for (auto& it : this->candidates) {
			ElectionCandidate c = it.second;
			try {
				client->addReaction(this->channel, this->message, c.emoji);
			} catch (AwokenDiscord::ErrorCode& e) {
				fprintf(stderr, "[%sERROR%s]\tFailed to add reaction to election: %s%d%s\n",
					Colours::RED, Colours::RESET,
					Colours::RED, e, Colours::RESET);
				return false;
			}
		}
		return true;
	}

	std::string Election::getWinners() {
		std::vector<std::string> winners = {};
		size_t maxVotes = 0;
		for (auto& it : this->candidates) {
			ElectionCandidate c = it.second;
			if (c.votes) {
				if (c.votes > maxVotes) {
					maxVotes = c.votes;
					winners = {c.emoji};
				} else if (c.votes == maxVotes) {
					winners.push_back(c.emoji);
				}
			}
		}

		std::vector<std::string> candidates = {};
		std::string winner = " __**" + std::string(winners.size() == 1 ? "WINNER" : "TIED") + "**__";
		for (auto& it : this->candidates) {
			ElectionCandidate c = it.second;
			candidates.push_back(emojiToString(c.emoji) + " - " + c.name
			+ (contains(c.emoji, winners) ? winner : "")
			+ " - " + count(c.votes, "vote"));
		}

		return concat(candidates, "\n");
	}

	std::string emojiToString(std::string id) {
		if (match(id, "^\\d{18}$").empty()) {
			return id;
		}
		return "<:emoji:" + id + ">";
	}

	std::string emojiToString(AwokenDiscord::Emoji emoji) {
		return emoji.ID.empty() ? emoji.name : (emoji.name + ":" + std::string(emoji.ID));
	}
}