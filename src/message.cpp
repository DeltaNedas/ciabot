#include <message.h>

namespace CIABot {
	MessageHistory::MessageHistory(std::string content, size_t timestamp, std::vector<std::string> attachments) {
		this->content = content;
		this->timestamp = timestamp;
		this->attachments = attachments;
	}


	Message::Message(std::string id, std::string channel, std::string server, std::string user,
		std::string creator, std::vector<std::string> responses, std::vector<MessageHistory*> history) {
		this->id = id;
		this->channel = channel;
		this->server = server;
		this->user = user;
		this->creator = creator;
		this->responses = responses;
		this->history = history;
	}

	Message::~Message() {
		for (MessageHistory* history : this->history) {
			delete history;
		}
	}
}