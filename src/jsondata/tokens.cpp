#include <userdata.h>

#include <info.h>
#include <options.h>
#include <util.h>

namespace CIABot {
	json Tokens::serialise() {
		json ret = this->values;
		return ret;
	}

	int Tokens::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\ttokens.json is not a JSON object.\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto token : data.items()) {
			std::string tokenName = token.key();
			if (!token.value().is_string()) {
				fprintf(stderr, "[%sERROR%s]\tToken %s is not a string!\n",
					Colours::RED, Colours::RESET, tokenName.c_str());
				return EINVAL;
			}

			this->values[tokenName] = token.value().get<std::string>();
		}

		return 0;
	}

	void Tokens::fallback() {
		this->values = {
			{"discord", "Replace me with your discord bot's token, in discord applications > bot application > bot > copy token"},
			{"yandex_translate", "(optional) replace me with your yandex.translate token"}
		};
	}

	std::string Tokens::set(std::string set, std::string name) {
		std::string result = set;
		this->values[name] = result;
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tToken for %s changed.\n",
			Colours::BLUE, Colours::RESET, name.c_str()); // Don't print it out!
		return result;
	}

	std::string Tokens::get(std::string name) {
		auto it = this->values.find(name);
		if (it == this->values.end()) {
			return "";
		}
		return it->second;
	}
}