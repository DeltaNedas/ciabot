#include <userdata.h>

#include <info.h>
#include <options.h>

namespace CIABot {
	json Modules::serialise() {
		json ret = this->values;
		return ret;
	}

	int Modules::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\tmodules.json is not a JSON object!\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto it : data.items()) {
			std::string name = it.key();
			if (!it.value().is_boolean()) {
				fprintf(stderr, "[%sERROR%s]\tModule status for %s is not a bool!\n",
					Colours::RED, Colours::RESET, name.c_str());
				return EINVAL;
			}

			this->values[name] = it.value().get<bool>();
		}

		return 0;
	}

	void Modules::fallback() {}

	bool Modules::set(bool set, std::string name) {
		bool result = set;
		this->values[name] = result;
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tModule %s%s%s was %s.\n",
			Colours::BLUE, Colours::RESET,
			Colours::BOLD, name.c_str(), Colours::RESET,
			(result ? "enabled" : "disabled"));
		return result;
	}

	bool Modules::get(std::string name) {
		auto it = this->values.find(name);
		if (it == this->values.end()) {
			this->newModulesFound = true;
			bool fallback = false;
#if NEW_MODULES_ENABLED
			fallback = true;
#endif
			this->set(fallback, name);
			return fallback;
		}
		return it->second;
	}

	bool Modules::wereNewModulesFound() {
		return this->newModulesFound;
	}
}