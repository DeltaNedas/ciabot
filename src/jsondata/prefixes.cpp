#include <userdata.h>

#include <info.h>
#include <options.h>
#include <util.h>

namespace CIABot {
	json Prefixes::serialise() {
		json ret = emptyObject();

		for (auto& prefixIt : this->values) {
			std::string prefix = prefixIt.second;
			if (prefix == DEFAULT_PREFIX) {
				continue;
			}
			ret[prefixIt.first] = prefix;
		}
		return ret;
	}

	int Prefixes::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\tprefixes.json is not a JSON object.\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto& prefixIt : data.items()) { // {
			std::string serverName = prefixIt.key();
			json jsonPrefix = prefixIt.value();
			if (!jsonPrefix.is_string()) {
				fprintf(stderr, "[%sERROR%s]\tPrefix for server %s is not a string!\n",
					Colours::RED, Colours::RESET, serverName.c_str());
				return EINVAL;
			}
			this->values[serverName] = jsonPrefix.get<std::string>();
		}

		return 0;
	}

	void Prefixes::fallback() {}

	std::string Prefixes::set(std::string set, std::string server) {
		std::string result = set;
		this->values[server] = result;
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tPrefix for %s changed to %s.\n",
			Colours::BLUE, Colours::RESET, server.c_str(), result.c_str());
		return result;
	}

	std::string Prefixes::get(std::string server, bool serverOnly) {
		auto it = this->values.find(server);
		if (it == this->values.end()) {
			if (server == "global") {
				return DEFAULT_PREFIX;
			}
			return this->get("global", true);
		}
		return it->second;
	}
}