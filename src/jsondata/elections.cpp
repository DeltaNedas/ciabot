#include <userdata.h>

#include <info.h>
#include <elections.h>
#include <options.h>
#include <util.h>
#include <util/generics.h>

namespace CIABot {
	json Elections::serialise() {
		json ret = emptyObject();

		for (auto& serverIt : this->values) {
			json server = emptyObject();
			for (auto& it : serverIt.second) {
				Election* e = it.second;
				json obj = {
					{"title", e->title},
					{"ends_after", e->endsAfter}
				};
				if (e->started) {
					obj["channel"] = e->channel;
					obj["message"] = e->message;
					obj["started"] = e->started;
					obj["ended"] = e->ended;
				}

				json candidates = emptyObject();
				for (auto it : e->candidates) {
					ElectionCandidate c = it.second;
					candidates[c.emoji] = {
						{"name", c.name},
						{"votes", c.votes}
					};
				}
				obj["candidates"] = candidates;

				if (e->voters.size()) {
					obj["voters"] = e->voters;
				}

				server[e->name] = obj;
				delete e;
			}
			ret[serverIt.first] = server;
		}
		return ret;
	}

	int Elections::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\telections.json is not a JSON object.\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto& it : data.items()) {
			json jsonServer = it.value();
			std::string serverName = it.key();
			if (!jsonServer.is_object()) {
				fprintf(stderr, "[%sERROR%s]\tServer %s is not an object!\n",
					Colours::RED, Colours::RESET, serverName.c_str());
				return EINVAL;
			}

			std::map<std::string, Election*> server = {};
			for (auto& it : jsonServer.items()) {
				std::string name = it.key();
				if (!it.value().is_object()) {
					fprintf(stderr, "[%sERROR%s]\tElection %s of server %s is not an object!\n",
						Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
					return EINVAL;
				}
				json election = it.value();

				std::string title = "", channel = "", message = "";
				size_t ends = 0, started = 0, ended = 0;
				std::vector<std::string> voters = {};
				std::map<std::string, ElectionCandidate> candidates = {};
				try {
					getJsonField(election, "title", title, REQUIRED);
					getJsonField(election, "ends_after", ends);
					getJsonField(election, "started", started);
					getJsonField(election, "ended", ended);
					getJsonField(election, "channel", channel);
					getJsonField(election, "message", message);
					getJsonField(election, "voters", voters);
				} catch (std::exception& e) {
					fprintf(stderr, "[%sERROR%s]\tFailed to parse election %s of server %s: %s%s%s\n",
						Colours::RED, Colours::RESET, name.c_str(), serverName.c_str(),
						Colours::RED, e.what(), Colours::RESET);
					return EINVAL;
				}

				auto candidatesIt = election.find("candidates");
				if (candidatesIt == election.end() || !candidatesIt.value().is_object()) {
					fprintf(stderr, "[%sERROR%s]\tCandidates for election %s of server %s is not an object!\n",
						Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
					return EINVAL;
				}

				json jsonCandidates = candidatesIt.value();
				for (auto& it : jsonCandidates.items()) {
					std::string emoji = it.key();
					auto c = it.value();
					if (!c.is_object()) {
						fprintf(stderr, "[%sERROR%s]\tCandidate %s for election %s is not an object!\n",
							Colours::RED, Colours::RESET, emoji.c_str(), name.c_str());
						return EINVAL;
					}

					std::string cName = "";
					size_t votes = 0;
					try {
						getJsonField(c, "name", cName, REQUIRED);
						getJsonField(c, "votes", votes);
					} catch (std::exception& e) {
						fprintf(stderr, "[%sERROR%s]\tFailed to parse election %s candidate %s of server %s: %s%s%s\n",
							Colours::RED, Colours::RESET, name.c_str(), emoji.c_str(), serverName.c_str(),
							Colours::RED, e.what(), Colours::RESET);
						return EINVAL;
					}
					candidates[emoji] = ElectionCandidate(cName, emoji, votes);
				}

				server[name] = new Election(channel, message, name, title, ends, started, ended, voters, candidates);
			}

			this->values[serverName] = server;
		}

		return 0;
	}

	void Elections::fallback() {}

	Election* Elections::set(Election* set, std::string server) {
		Election* result = set;
		std::map<std::string, Election*> map = this->get(server);
		Election* old = CIABot::get(map, result->name, (Election*) NULL);
		if (old) {
			delete old; // It won't be deleted by serialise()
		}
		map[result->name] = result;
		this->set(map, server);
		return result;
	}
	std::map<std::string, Election*> Elections::set(std::map<std::string, Election*> set, std::string server) {
		auto result = set;
		this->values[server] = result;
		return result;
	}

	Election* Elections::remove(std::string name, std::string server) {
		auto map = this->get(server);
		Election* old = CIABot::get(map, name, (Election*) NULL);
		if (old && !old->canVote()) {
			map.erase(name);
			this->values[server] = map;
			return old;
		}
		return nullptr;
	}

	Election* Elections::get(std::string name, std::string server) {
		auto elections = this->get(server);
		return CIABot::get(elections, name, (Election*) NULL);
	}
	std::map<std::string, Election*> Elections::get(std::string server) {
		return CIABot::get(this->values, server, {});
	}
}
