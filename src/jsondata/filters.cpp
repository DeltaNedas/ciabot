#include <userdata.h>

#include <filters.h>
#include <info.h>
#include <options.h>
#include <util/generics.h>
#include <util/strings.h>

namespace CIABot {
	json Filters::serialise() {
		json servers = emptyObject();
		for (auto& it : this->values) {
			std::string id = it.first;
			json server = emptyObject();
			for (auto& fIt : it.second) {
				Filter* filter = fIt.second;
				json jsonFilter = emptyObject();

				// Message filtering and moderation
				json filterFlags = emptyObject();
				unsigned char flags = filter->flags;
				filterFlags["bot"] = (bool) ((flags & FilterFlags::BOT) != 0);
				filterFlags["user"] = (bool) ((flags & FilterFlags::USER) != 0);
				filterFlags["delete"] = (bool) ((flags & FilterFlags::DELETE) != 0);
				filterFlags["kick"] = (bool) (flags & FilterFlags::KICK);
				filterFlags["ban"] = (bool) (flags & FilterFlags::BAN);
				filterFlags["force_ascii"] = (bool) (flags & FilterFlags::FORCE_ASCII);
				filterFlags["force_lower"] = (bool) (flags & FilterFlags::FORCE_LOWER);
				jsonFilter["flags"] = filterFlags;

				// Parsing
				jsonFilter["input"] = filter->input;
				jsonFilter["output"] = filter->output;

				// Output channel, set to same channel as triggered message if empty.
				jsonFilter["channel"] = filter->channel;

				// URL stuff
				json url = emptyObject();
				url["input"] = filter->urlInput;
				url["output"] = filter->urlOutput;
				jsonFilter["url"] = url;

				server[fIt.first] = jsonFilter;
				delete filter; // Avoid memory leak
			}
			servers[id] = server;
		}
		return servers;
	}

	int Filters::deserialise(json data) {
		if (!data.is_object()) { // Modern json reeeeeee
			fprintf(stderr, "[%sERROR%s]\tFilters.json does not contain an object!\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto& filtersIt : data.items()) {
			std::string serverName = filtersIt.key();
			json jsonFilters = filtersIt.value();
			if (!jsonFilters.is_object()) {
				fprintf(stderr, "[%sERROR%s]\tServer %s is not an object of filters!\n",
					Colours::RED, Colours::RESET, serverName.c_str());
				return EINVAL;
			}

			std::map<std::string, Filter*> serverFilters = {};
			for (auto& filterIt : jsonFilters.items()) {
				std::string name = filterIt.key();
				json jsonFilter = filterIt.value();
				if (!jsonFilter.is_object()) {
					fprintf(stderr, "[%sERROR%s]\tFilter %s on server %s is not an object!\n",
						Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
					return EINVAL;
				}

				std::string input = "";
				{
					auto it = jsonFilter.find("input");
					if (it == jsonFilter.end() || !it.value().is_string()) {
						fprintf(stderr, "[%sERROR%s]\tInput for filter %s of server %s is missing or not a string!\n",
							Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
						return EINVAL;
					}
					input = it.value().get<std::string>();
				}

				std::string output = "";
				{
					auto it = jsonFilter.find("output");
					if (it != jsonFilter.end()) {
						if (!it.value().is_string()) {
							fprintf(stderr, "[%sERROR%s]\tOutput for filter %s of server %s is not a string!\n",
								Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
							return EINVAL;
						}
						output = it.value().get<std::string>();
					}
				}

				unsigned char flags = FilterFlags::NONE | FilterFlags::USER;
				{
					auto it = jsonFilter.find("flags");
					if (it != jsonFilter.end()) {
						if (!it.value().is_object()) {
							fprintf(stderr, "[%sERROR%s]\tFlags for filter %s of server %s are not an object!\n",
								Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
							return EINVAL;
						}

						json jsonFlags = it.value();
						try {
							this->deserialiseFlag(jsonFlags, "bot", &flags, FilterFlags::BOT);
							this->deserialiseFlag(jsonFlags, "user", &flags, FilterFlags::USER, true);
							this->deserialiseFlag(jsonFlags, "delete", &flags, FilterFlags::DELETE);
							this->deserialiseFlag(jsonFlags, "kick", &flags, FilterFlags::KICK);
							this->deserialiseFlag(jsonFlags, "ban", &flags, FilterFlags::BAN);
							this->deserialiseFlag(jsonFlags, "force_ascii", &flags, FilterFlags::FORCE_ASCII);
							this->deserialiseFlag(jsonFlags, "force_lower", &flags, FilterFlags::FORCE_LOWER);
						} catch (std::exception& e) {
							fprintf(stderr, "[%sERROR%s]\t\"%s\" flag for filter %s of server %s must be true or false!\n",
								Colours::RED, Colours::RESET, this->currentFlag.c_str(), name.c_str(), serverName.c_str());
							return EINVAL;
						}
					}
				}

				std::string urlInput = "";
				std::string urlOutput = "";
				{
					auto it = jsonFilter.find("url");
					if (it != jsonFilter.end()) {
						if (!it.value().is_object()) {
							fprintf(stderr, "[%sERROR%s]\tURL for filter %s of server %s is not an object!\n",
								Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
							return EINVAL;
						}

						json url = it.value();
						auto inputIt = url.find("input");
						if (inputIt != url.end()) {
							if (!inputIt.value().is_string()) {
								fprintf(stderr, "[%sERROR%s]\tURL input for filter %s of server %s is not a string!\n",
									Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
								return EINVAL;
							}

							urlInput = inputIt.value().get<std::string>();
						}

						auto outputIt = url.find("output");
						if (outputIt != url.end()) {
							if (!outputIt.value().is_string()) {
								fprintf(stderr, "[%sERROR%s]\tURL output for filter %s of server %s is not a string!\n",
									Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
								return EINVAL;
							}

							urlOutput = outputIt.value();
						}
					}
				}

				std::string channel = ""; // Send in current channel by default
				auto it = jsonFilter.find("channel");
				if (it != jsonFilter.end()) {
					if (!it.value().is_string()) {
						fprintf(stderr, "[%sERROR%s]\tOutput channel for filter %s of server %s is not a string!\n\tIf you want it to use the filtered message's channel, remove it or set it to an empty string\n",
							Colours::RED, Colours::RESET, name.c_str(), serverName.c_str());
						return EINVAL;
					}
					channel = jsonFilter["channel"];
				}

				Filter* filter = new Filter(input, output, flags, channel, urlInput, urlOutput);
				filter->name = name;
				serverFilters[name] = filter;
			}

			this->values[serverName] = serverFilters;
		}

		return 0;
	}

	void Filters::deserialiseFlag(json jsonFlags, std::string name, unsigned char* flags, unsigned char flag, bool fallback) {
		this->currentFlag = name;
		auto it = jsonFlags.find(name);
		if (it != jsonFlags.end()) {
			if (!it.value().is_boolean()) {
				throw EINVAL;
			}

			fallback = it.value().get<bool>();
		}
		if (fallback) {
			*flags |= flag;
		}
	}

	void Filters::fallback() {}

	bool Filters::remove(std::string name, std::string server) {
		auto filters = this->get(server, true);
		if (filters.find(name) == filters.end()) {
			return false;
		}

		filters.erase(name);
		this->set(filters, server);
		return true;
	}

	Filter* Filters::set(Filter* set, std::string name, std::string server) {
		auto filters = this->get(server, true);
		auto it = filters.find(name);
		if (it != filters.end()) {
			delete it->second; // Avoid memory leak
		}

		set->name = name;
		filters[name] = set;
		this->set(filters, server);
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tFilter %s for %s set to %s.\n",
			Colours::BLUE, Colours::RESET, name.c_str(), server.c_str(), set->output.c_str());
		return set;
	}

	std::map<std::string, Filter*> Filters::set(std::map<std::string, Filter*> set, std::string server) {
		this->values[server] = set;
		return set;
	}

	Filter* Filters::get(std::string name, std::string server) {
		auto filters = this->get(server, true);
		return CIABot::get(filters, name, (Filter*) NULL);
	}

	std::map<std::string, Filter*> Filters::get(std::string server, bool serverOnly) {
		std::map<std::string, Filter*> ret = CIABot::get(this->values, server);

		if (!(serverOnly || server == "global")) {
			ret = combine(ret, this->get("global", true));
		}

		return ret;
	}
}