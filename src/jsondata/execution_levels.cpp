#include <userdata.h>

#include <info.h>
#include <options.h>
#include <util.h>

namespace CIABot {
	json ExecutionLevels::serialise() {
		json ret = emptyObject();
		for (auto& serverIt : this->values) {
			json server = emptyObject();
			for (auto& it : serverIt.second) {
				execution_level_t level = it.second;
				if (level == DEFAULT_EXECUTION_LEVEL) {
					continue;
				}

				server[it.first] = (int) level;
			}
			ret[serverIt.first] = server;
		}
		return ret;
	}

	int ExecutionLevels::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\texecution_levels.json is not a JSON object.\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto& levelsIt : data.items()) { // {
			std::string serverName = levelsIt.key();
			json jsonLevels = levelsIt.value();
			if (!jsonLevels.is_object()) {
				fprintf(stderr, "[%sERROR%s]\tServer %s is not an object of users -> execution levels!\n",
					Colours::RED, Colours::RESET, serverName.c_str());
				return EINVAL;
			}

			std::map<std::string, execution_level_t> server = {};
			for (auto& it : jsonLevels.items()) {
				std::string userName = it.key();
				if (!it.value().is_number()) {
					fprintf(stderr, "[%sERROR%s]\tExecution level for user %s of server %s is not a number!\n",
						Colours::RED, Colours::RESET, userName.c_str(), serverName.c_str());
					return EINVAL;
				}
				server[userName] = (execution_level_t) it.value().get<int>();
			}

			this->values[serverName] = server;
		}

		return 0;
	}

	void ExecutionLevels::fallback() {
		this->values["global"] = {
			{"542742543478292480", 0} // Replace with, or just add :), your own ID for a Ring-0 execution level.
		};
	}

	execution_level_t ExecutionLevels::set(execution_level_t set, std::string user, std::string server) {
		execution_level_t result = set;
		auto map = this->get(server, true);
		map[user] = result;
		this->set(map, server);
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tUser %s's execution level set to Ring-%d\n",
			Colours::BLUE, Colours::RESET, user.c_str(), (int) result);
		return result;
	}

	std::map<std::string, execution_level_t> ExecutionLevels::set(std::map<std::string, execution_level_t> set, std::string server) {
		auto result = set;
		this->values[server] = result;
		return result;
	}

	execution_level_t ExecutionLevels::get(std::string user, std::string server, bool serverOnly) {
		auto users = this->get(server, serverOnly);
		auto it = users.find(user);
		if (it == users.end()) {
			return DEFAULT_EXECUTION_LEVEL;
		}
		return it->second;
	}

	std::map<std::string, execution_level_t> ExecutionLevels::get(std::string server, bool serverOnly) {
		auto it = this->values.find(server);
		if (it == this->values.end()) {
			if (!(serverOnly || server == "global")) {
				return this->get("global", true);
			}
			return {};
		}
		return it->second;
	}
}