#include <userdata.h>

#include <commands.h>
#include <info.h>
#include <options.h>
#include <permissions.h>
#include <util/discord.h>
#include <util/json.h>
#include <util/generics.h>
#include <util/strings.h>

namespace CIABot {
	json Permissions::serialise() {
		json ret = emptyObject();

		for (auto& serverIt : this->values) {
			json server = emptyObject();
			for (auto& it : serverIt.second) {
				json perm = emptyObject();

				Permission* p = it.second;
				if (p->executionLevel != DEFAULT_EXECUTION_LEVEL) {
					perm["execution_level"] = (int) p->executionLevel;
				}
				if (p->rupees) {
					perm["rupees"] = (double) p->rupees;
				}
				if (p->rupeesCost) {
					perm["rupees_cost"] = (double) p->rupeesCost;
				}
				if (p->deleteAfter) {
					perm["delete_after"] = (long) p->deleteAfter;
				}
				if (p->created) {
					perm["created"] = (long) p->created;
				}
				if (p->joined) {
					perm["joined"] = (long) p->joined;
				}

				perm["blacklists"] = p->blacklists;
				perm["whitelists"] = p->whitelists;

				server[it.first] = perm;
				//delete permission;
			}
			ret[serverIt.first] = server;
		}
		return ret;
	}

	int Permissions::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\tpermissions.json is not a JSON object.\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto permsIt : data.items()) {
			std::string serverName = permsIt.key();
			json jsonPermissions = permsIt.value();
			if (!jsonPermissions.is_object()) {
				fprintf(stderr, "[%sERROR%s]\tServer %s is not an object of names -> permissions!\n",
					Colours::RED, Colours::RESET, serverName.c_str());
				return EINVAL;
			}

			std::map<std::string, Permission*> server = {};
			for (auto& it : jsonPermissions.items()) {
				std::string name = it.key();
				if (!it.value().is_object()) {
					fprintf(stderr, "[%sERROR%s]\tPermissions for name %s%s of server %s is not an object!\n",
						Colours::RED, Colours::RESET, prefixes.get().c_str(), name.c_str(), serverName.c_str());
					return EINVAL;
				}

				json req = it.value();
				int executionLevel = DEFAULT_EXECUTION_LEVEL;
				rupees_t rupees = 0, rupeesCost = 0;
				size_t deleteAfter = 0, cooldown = DEFAULT_COOLDOWN, created = 0, joined = 0;

				std::map<std::string, std::vector<std::string>> blacklists;
				std::map<std::string, std::vector<std::string>> whitelists;

				try {
					getJsonField(req, "execution_level", executionLevel);
					getJsonField(req, "rupees", rupees);
					getJsonField(req, "rupees_cost", rupeesCost);
					getJsonField(req, "delete_after", deleteAfter);
					getJsonField(req, "cooldown", cooldown);
					getJsonField(req, "created", created);
					getJsonField(req, "joined", joined);
					getJsonField(req, "blacklists", blacklists);
					getJsonField(req, "whitelists", whitelists);
				} catch (std::exception& e) {
					fprintf(stderr, "[%sERROR%s]\tFailed to get requirement for permission %s on server %s: %s\n",
						Colours::RED, Colours::RESET, name.c_str(), serverName.c_str(), e.what());
					return EINVAL;
				}

				server[name] = new Permission((execution_level_t) executionLevel, rupees, rupeesCost, deleteAfter, cooldown, created, joined, blacklists, whitelists);
			}

			this->values[serverName] = server;
		}

		return 0;
	}

	void Permissions::fallback() {}

	Permission* Permissions::set(Permission* set, std::string name, std::string server) {
		Permission* result = set;
		auto map = this->get(server, true);
		map[name] = result;
		this->set(map, server);
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tPermission %s's execution level for %s set to Ring-%d\n",
			Colours::BLUE, Colours::RESET,
			name.c_str(), server.c_str(), (int) result->executionLevel);
		return result;
	}

	std::map<std::string, Permission*> Permissions::set(std::map<std::string, Permission*> set, std::string server) {
		auto result = set;
		this->values[server] = result;
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tServer %s's permissions replaced.\n",
			Colours::BLUE, Colours::RESET, server.c_str());
		return result;
	}

	Permission* Permissions::get(std::string name, std::string server, bool serverOnly) {
		std::map<std::string, Permission*> permissions = this->get(server, serverOnly);
		auto it = permissions.find(name);
		if (it == permissions.end()) {
			std::string command = match(name, "^(.+)\\.", name);
			std::string action = match(name, "\\.(.+)$", "run");
			Permission* ptr = this->getDefault(name);
			if (ptr == nullptr) {
				return nullptr;
			}
			// "help.list" -> "list", "roll" -> "run"
			return ptr;
		}
		return it->second;
	}

	std::map<std::string, Permission*> Permissions::get(std::string server, bool serverOnly) {
		auto it = this->values.find(server);
		if (it == this->values.end()) {
			if (!(serverOnly || server == "global")) {
				return this->get("global", true);
			}
			return {};
		}
		return it->second;
	}

	Permission* Permissions::getDefault(std::string name) {
		auto it = defaultPermissions.find(name);
		if (it == defaultPermissions.end()) {
			std::string type = match(name, R"(^(elections|filters)\.\w+\.\w+$)");
			if (type.size()) {
				std::string action = match(name, R"(^\w+\.\w+\.(\w+)$)");
				return this->getDefault(type + ".default." + action);
			}
			fprintf(stderr, "[%sERROR%s]\tNo default permissions found for %s%s%s!\n",
				Colours::RED, Colours::RESET,
				Colours::RED, name.c_str(), Colours::RESET);
			return nullptr;
		}
		return &it->second;
	}

	bool Permissions::has(std::string name, UserData userData, std::string server, bool apply) {
		if (server.empty()) {
			server = userData.server;
		}

		Permission* perm = permissions.get(name, server);
		if (perm == nullptr) {
			return false;
		}

		if (perm->blacklists["users"].size() > 0) {
			if (contains(userData.user, perm->getList("black", "users"))) {
				return false;
			}
			return true;
		}
		if (perm->blacklists["channels"].size() > 0) {
			if (contains(userData.channel, perm->getList("black", "channels"))) {
				return false;
			}
			return true;
		}

		if (perm->whitelists["users"].size() > 0) {
			if (contains(userData.user, perm->getList("white", "users"))) {
				return true;
			}
			return false;
		}
		if (perm->whitelists["channels"].size() > 0) {
			if (contains(userData.channel, perm->getList("white", "channels"))) {
				return true;
			}
			return false;
		}

		if (userData.executionLevel == 0) { // Ring-0 = Same as CIABot
			return true;
		}

		size_t now = timestamp();
		if (perm->created && userData.user.size()) {
			size_t created = createdAt(userData.user);
			if ((now - created) > perm->created) {
				return false;
			}
		}

		if (perm->joined && server != "global") {
			try {
				AwokenDiscord::ServerMember member = client->getMember(server, userData.user);
				size_t joined = dateToUnix(member.joinedAt);
				if (now - joined > perm->joined) { // Convert to seconds first
					return false;
				}
			} catch (AwokenDiscord::ErrorCode& e) {}
		}

		bool ret = (perm->executionLevel == -1 || perm->executionLevel >= userData.executionLevel) && perm->rupees <= userData.rupees;
		if (ret && apply) {
			rupees.sub(perm->rupeesCost, userData.user, server);
			if (perm->deleteAfter) {
				std::thread* removeThread = new std::thread([=](){
					sleep(perm->deleteAfter * 1000);
					deleteMessage(userData.message, userData.channel);
				});
				auxiliaryThreads.push_back(removeThread);
				removeThread->detach();
			}
		}
		return ret;
	}

	bool Permissions::reset(std::string name, std::string server) {
		std::map<std::string, Permission*> permissions = this->get(server, true);
		auto it = permissions.find(name);
		if (it == permissions.end()) {
			return false;
		}

		permissions.erase(it);
		this->set(permissions, server);
		return true;
	}
}
