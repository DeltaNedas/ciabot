#include <userdata.h>

#include <info.h>
#include <options.h>
#include <util.h>

namespace CIABot {
	json Rupees::serialise() {
		json ret = emptyObject();

		for (auto& it : this->values) {
			json server = emptyObject();
			for (auto& user : it.second) {
				rupees_t balance = user.second;
				if (balance != DEFAULT_RUPEES) {
					server[user.first] = (double) balance;
				}
			}
			if (server.size()) {
				ret[it.first] = server;
			}
		}
		return ret;
	}

	int Rupees::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\trupees.json is not a JSON object.\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto serverIt : data.items()) {
			std::string serverName = serverIt.key();
			json server = serverIt.value();
			if (!server.is_object()) {
				fprintf(stderr, "[%sERROR%s]\tUsers/rupees for server %s is not an object!\n",
					Colours::RED, Colours::RESET, serverName.c_str());
				return EINVAL;
			}

			this->values[serverName] = {};
			for (auto rupeesIt : server.items()) {
				std::string userName = rupeesIt.key();
				json rupees = rupeesIt.value();
				if (!rupees.is_number()) {
					fprintf(stderr, "[%sERROR%s]\tRupees for user %s is not a number!\n",
						Colours::RED, Colours::RESET, userName.c_str());
					return EINVAL;
				}

				this->values[serverName][userName] = rupees.get<rupees_t>();
			}
		}

		return 0;
	}

	void Rupees::fallback() {}

	rupees_t Rupees::add(rupees_t add, std::string user, std::string server) {
		return this->set(this->get(user, server) + add, user, server);
	}

	rupees_t Rupees::sub(rupees_t sub, std::string user, std::string server) {
		return this->set(this->get(user, server)  - sub, user, server);
	}

	rupees_t Rupees::set(rupees_t set, std::string user, std::string server) {
		rupees_t result = set;
		if (result < 0) {
			result = 0;
		}

		auto users = this->get(server);
		users[user] = result;
		this->values[server] = users;
		printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s]\tUser %s's rupees in %s set to %lu\n",
			Colours::BLUE, Colours::RESET, user.c_str(), server.c_str(), result);
		return result;
	}

	std::map<std::string, rupees_t> Rupees::get(std::string server) {
		auto it = this->values.find(server);
		if (it == this->values.end()) {
			return {};
		}
		return it->second;
	}
	rupees_t Rupees::get(std::string user, std::string server) {
		return this->get(user, this->get(server));
	}
	rupees_t Rupees::get(std::string user, std::map<std::string, rupees_t> users) {
		auto it = users.find(user);
		if (it == users.end()) {
			return DEFAULT_RUPEES;
		}
		return it->second;
	}
}