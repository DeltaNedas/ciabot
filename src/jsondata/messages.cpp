#include <userdata.h>

#include <info.h>
#include <message.h>
#include <options.h>
#include <util.h>

namespace CIABot {
	json Messages::serialise() {
		json ret = emptyObject();

		for (auto& channelIt : this->values) {
			json channel = emptyObject();
			for (auto& it : channelIt.second) {
				Message* message = it.second;
				json obj = emptyObject();

				if (!message->creator.empty()) {
					obj["creator"] = message->creator;
				}
				obj["user"] = message->user;
				obj["server"] = message->server;
				obj["responses"] = message->responses;

				json history;
				for (MessageHistory* part : message->history) {
					json partJson;
					partJson["content"] = part->content;
					partJson["timestamp"] = (long) part->timestamp;
					partJson["attachments"] = part->attachments;
					history.push_back(partJson);
				}
				obj["history"] = history;

				channel[message->id] = obj;
				delete message;
			}
			ret[channelIt.first] = channel;
		}
		return ret;
	}

	int Messages::deserialise(json data) {
		if (!data.is_object()) {
			fprintf(stderr, "[%sERROR%s]\tmessages.json is not a JSON object.\n",
				Colours::RED, Colours::RESET);
			return EINVAL;
		}

		for (auto& it : data.items()) {
			json jsonChannel = it.value();
			std::string channelName = it.key();
			if (!jsonChannel.is_object()) {
				fprintf(stderr, "[%sERROR%s]\tChannel %s is not an object!\n",
					Colours::RED, Colours::RESET, channelName.c_str());
				return EINVAL;
			}

			std::map<std::string, Message*> channel = {};
			for (auto& it : jsonChannel.items()) {
				std::string messageName = it.key();
				if (!it.value().is_object()) {
					fprintf(stderr, "[%sERROR%s]\tMessage %s of channel %s is not an object!\n",
						Colours::RED, Colours::RESET, messageName.c_str(), channelName.c_str());
					return EINVAL;
				}
				json message = it.value();

				// While they could be loaded from client->getChannel(id).serverID it would
				// add lots of network delay and not work if a channel was deleted.
				// moar botnet, ik
				std::string serverName = "", userName = "", creator = "";
				std::vector<std::string> responses = {};
				std::vector<MessageHistory*> history = {};
				try {
					getJsonField(message, "server", serverName, REQUIRED);
					getJsonField(message, "user", userName, REQUIRED);
					getJsonField(message, "creator", creator);
				} catch (std::exception& e) {
					fprintf(stderr, "[%sERROR%s]\tFailed to parse message %s of channel %s: %s%s%s\n",
						Colours::RED, Colours::RESET, messageName.c_str(), channelName.c_str(),
						Colours::RED, e.what(), Colours::RESET);
					return EINVAL;
				}

				auto responsesIt = message.find("responses");
				if (responsesIt != message.end()) {
					json jsonResponses = responsesIt.value();
					if (!jsonResponses.is_array()) {
						fprintf(stderr, "[%sERROR%s]\tResponses for message %s of channel %s is not an array!\n",
							Colours::RED, Colours::RESET, messageName.c_str(), channelName.c_str());
						return EINVAL;
					}

					for (size_t i = 0; i < jsonResponses.size(); i++) {
						json jsonResponse = jsonResponses[i];
						if (!jsonResponse.is_string()) {
							fprintf(stderr, "[%sERROR%s]\tResponse #%lu for message %s is not a string!\n",
								Colours::RED, Colours::RESET, i, messageName.c_str());
							return EINVAL;
						}

						responses.push_back(jsonResponse.get<std::string>());
					}
				}

				auto historyIt = message.find("history");
				if (historyIt == message.end() || !historyIt.value().is_array()) {
					fprintf(stderr, "[%sERROR%s]\tHistory for message %s of channel %s is not an array!\n",
						Colours::RED, Colours::RESET, messageName.c_str(), channelName.c_str());
					return EINVAL;
				}

				json jsonHistory = historyIt.value();
				for (size_t i = 0; i < jsonHistory.size(); i++) {
					json part = jsonHistory[i];
					if (!part.is_object()) {
						fprintf(stderr, "[%sERROR%s]\tHistory version %lu for message %s is not an object!\n",
							Colours::RED, Colours::RESET, i, messageName.c_str());
						return EINVAL;
					}

					auto timestampIt = part.find("timestamp");
					if (timestampIt == part.end() || !timestampIt.value().is_number()) {
						fprintf(stderr, "[%sERROR%s]\tTimestamp for message %s version %lu is not a valid timestamp!\n",
							Colours::RED, Colours::RESET, messageName.c_str(), i);
						return EINVAL;
					}
					size_t timestamp = timestampIt.value().get<size_t>();

					auto attachmentsIt = part.find("attachments");
					if (attachmentsIt == part.end() || !attachmentsIt.value().is_array()) {
						fprintf(stderr, "[%sERROR%s]\tTimestamp for message %s version %lu is not an array!\n",
							Colours::RED, Colours::RESET, messageName.c_str(), i);
						return EINVAL;
					}

					std::vector<std::string> attachments = {};
					json jsonAttachments = attachmentsIt.value();
					for (size_t aI = 0; aI < jsonAttachments.size(); aI++) {
						json urlJson = jsonAttachments[aI];
						if (!urlJson.is_string()) {
							fprintf(stderr, "[%sERROR%s]\tAttachment %lu for message %s version %lu is not a string!\n",
								Colours::RED, Colours::RESET, aI, messageName.c_str(), i);
							return EINVAL;
						}

						attachments.push_back(urlJson.get<std::string>());
					}

					auto contentIt = part.find("content");
					if (contentIt == part.end() || !contentIt.value().is_string()) {
						fprintf(stderr, "[%sERROR%s]\tContent for message %s version %lu is not a string!\n",
							Colours::RED, Colours::RESET, messageName.c_str(), i);

						for (MessageHistory* unused : history) {
							delete unused;
						}
						return EINVAL;
					}
					std::string content = contentIt.value().get<std::string>();

					history.push_back(new MessageHistory(content, timestamp, attachments));
				}

				channel[messageName] = new Message(messageName, channelName, serverName, userName, creator, responses, history);
			}

			this->values[channelName] = channel;
		}

		return 0;
	}

	void Messages::fallback() {}

	Message* Messages::set(Message* set, std::string message, std::string channel) {
		Message* result = set;
		std::map<std::string, Message*> map = this->get(channel);
		map[message] = result;
		this->set(map, channel);
		return result;
	}

	Message* Messages::set(AwokenDiscord::Message set, std::string creator) {
		std::string messageID(set.ID);
		std::string channelID(set.channelID);
		std::string serverID(set.serverID);

		if (serverID.empty()) {
			AwokenDiscord::Channel c;
			if (getChannel(channelID, &c)) {
				serverID = c.serverID; // A channels ID can't change so do not update cache.
			}
		}

		Message* result = this->get(messageID, channelID);
		if (result == nullptr) {
			result = new Message(messageID, channelID, serverID, std::string(set.author.ID), creator, {}, {});
		}

		std::vector<std::string> attachments = {};
		for (auto attach : set.attachments) {
			attachments.push_back(attach.url);
		}

		result->history.push_back(new MessageHistory(set.content, dateToUnix(set.timestamp), attachments));
		return this->set(result, messageID, channelID);
	}

	std::map<std::string, Message*> Messages::set(std::map<std::string, Message*> set, std::string channel) {
		auto result = set;
		this->values[channel] = result;
		return result;
	}

	Message* Messages::get(std::string message, std::string channel) {
		if (channel.empty()) {
			for (auto mIt : this->values) {
				Message* attempt = this->get(message, mIt.second);
				if (attempt != nullptr) {
					return attempt;
				}
			}
			return nullptr;
		}

		return this->get(message, this->get(channel));
	}

	Message* Messages::get(std::string message, std::map<std::string, Message*> messages) {
		auto it = messages.find(message);
		if (it == messages.end()) {
			return nullptr;
		}
		return it->second;
	}

	std::map<std::string, Message*> Messages::get(std::string channel) {
		auto it = this->values.find(channel);
		if (it == this->values.end()) {
			return {};
		}
		return it->second;
	}
}
