#include <cache.h>

#include <threads.h>
#include <util.h>

namespace CIABot {
	bool updateCache(AwokenDiscord::Channel* value) {
		return getChannel(value->ID, value, false, 0);
	}
	bool updateCache(AwokenDiscord::Message* value) {
		return getMessage(value->ID, value->channelID, value, false, 0);
	}
	bool updateCache(AwokenDiscord::Server* value) {
		return getServer(value->ID, value, false, 0);
	}
	bool updateCache(ServerMember* value) {
		return getMember(value->member.user.ID, value->server, &value->member, false, 0);
	}
	bool updateCache(AwokenDiscord::User* value) {
		return getUser(value->ID, value, false, 0);
	}
}