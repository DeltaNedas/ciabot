BUILDDIR ?= build
OBJECTDIR ?= objects
SOURCEDIR ?= src
INCLUDEDIR ?= include

CXX ?= g++
CXXFLAGS ?= -O3 -Wall -ansi -pedantic -std=c++2a -I$(INCLUDEDIR) -c -g -rdynamic -fPIC
LDFLAGS ?= -lawoken_discord -lssl -lcrypto -lpthread -lcurl -ldl -shared
EXECUTABLEFLAGS ?= -lawoken_discord -lssl -lcrypto -lpthread -lcurl -ldl -Lbuild -lciabot '-Wl,-rpath,$$ORIGIN'

BINARY ?= ciabot
SHARED ?= libciabot.so
STATIC ?= libciabot.a
SOURCES := $(filter-out src/main.cpp, $(shell find $(SOURCEDIR)/ -type f -name "*.c*"))
OBJECTS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.o, $(SOURCES))
DEPENDS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.d, $(SOURCES))
# Non-system #include's

DATA ?= /usr/share/$(BINARY)
BINARIES ?= /usr/bin
LIBRARIES ?= /usr/lib
HEADERS ?= /usr/include

# #define options, pass with KEY=VALUE
FEED_CHECK_INTERVAL ?= 5 * 60
#	In seconds, can be an expression or just a single number
USE_POWERLINE ?= 1
#	Use powerline-like status for CIABot CLI.
COMPILE_INFO_MESSAGES ?= 1
#	Set to 0 for a minor speed increase and binary size reduction
DEFAULT_EXECUTION_LEVEL ?= -1
#	-1 = None
#	0 = Same as CIABot!
OWNER_EXECUTION_LEVEL ?= 1
#	Execution level given to a server's owner when CIABot joins it.
DEFAULT_RUPEES ?= 0
NEW_MODULES_ENABLED ?= 1
#	Set to 0 for enhanced security.
NEW_MODULES_REQUIRE_RESTART ?= 1
#	Set to 0 and new modules will not require a restart, weakening security.
DEFAULT_PREFIX ?= $
CONSOLE_COMMAND_PREFIX ?= \#
DEFAULT_CACHE_LIFETIME ?= 30
#	By default, all discord requests will be cached for up to 30 seconds
USE_COMPRESSION ?= 0
#	Compress data in JSON files with zlib.
DEFAULT_COOLDOWN ?= 500
# Cooldown between a command for each server and user
# Cooldowns do not apply to filters.
MINIMUM_COOLDOWN ?= 500
MAXIMUM_COOLDOWN ?= 1000 * 60
# 1 min cooldown max

ifneq ($(USE_POWERLINE), 1)
	USE_POWERLINE = 0
endif
ifneq ($(COMPILE_INFO_MESSAGES), 1)
	COMPILE_INFO_MESSAGES = 0
endif
ifneq ($(NEW_MODULES_ENABLED), 1)
	NEW_MODULES_ENABLED = 0
endif
ifneq ($(NEW_MODULES_REQUIRE_RESTART), 1)
	NEW_MODULES_REQUIRE_RESTART = 0
endif
ifneq ($(USE_COMPRESSION), 1)
	USE_COMPRESSION = 0
endif

all: $(BUILDDIR)/$(SHARED) $(BUILDDIR)/$(BINARY)

configure:
	@cp -f include/options.h.in include/options.h.out
	@sed -i include/options.h.out \
		-e "s/{FEED_CHECK_INTERVAL}/$(FEED_CHECK_INTERVAL)/g" \
		-e "s/{USE_POWERLINE}/$(USE_POWERLINE)/g" \
		-e "s/{COMPILE_INFO_MESSAGES}/$(COMPILE_INFO_MESSAGES)/g" \
		-e "s/{DEFAULT_EXECUTION_LEVEL}/$(DEFAULT_EXECUTION_LEVEL)/g" \
		-e "s/{OWNER_EXECUTION_LEVEL}/$(OWNER_EXECUTION_LEVEL)/g" \
		-e "s/{DEFAULT_RUPEES}/$(DEFAULT_RUPEES)/g" \
		-e "s/{NEW_MODULES_ENABLED}/$(NEW_MODULES_ENABLED)/g" \
		-e "s/{NEW_MODULES_REQUIRE_RESTART}/$(NEW_MODULES_REQUIRE_RESTART)/g" \
		-e "s/{DEFAULT_PREFIX}/$(DEFAULT_PREFIX)/g" \
		-e "s/{CONSOLE_COMMAND_PREFIX}/$(CONSOLE_COMMAND_PREFIX)/g" \
		-e "s/{DEFAULT_CACHE_LIFETIME}/$(DEFAULT_CACHE_LIFETIME)/g" \
		-e "s/{USE_COMPRESSION}/$(USE_COMPRESSION)/g" \
		-e "s/{DEFAULT_COOLDOWN}/$(DEFAULT_COOLDOWN)/g" \
		-e "s/{MINIMUM_COOLDOWN}/$(MINIMUM_COOLDOWN)/g" \
		-e "s/{MAXIMUM_COOLDOWN}/$(MAXIMUM_COOLDOWN)/g"
	@cmp -s include/options.h include/options.h.out; if [ $$? -ne 0 ]; then \
		cp include/options.h.out include/options.h; \
		echo "Created options.h file."; \
	fi;

install:
	mkdir -p $(DATA)
	#cp -f $(BUILDDIR)/$(BINARY) $(DATA)/
	mkdir -p $(LIBRARIES)
	cp -f $(BUILDDIR)/$(SHARED) $(LIBRARIES)/
	#cp -f $(BUILDDIR)/$(STATIC) $(LIBRARIES)/
	mkdir -p $(BINARIES)
	ln -sf $(DATA)/$(BINARY) $(BINARIES)/$(BINARY)
	mkdir -p $(HEADERS)/$(BINARY)
	cp -rf $(INCLUDEDIR)/* $(HEADERS)/$(BINARY)
	rm $(HEADERS)/$(BINARY)/options.h.in
	rm $(HEADERS)/$(BINARY)/options.h.out

uninstall:
	rm -f $(BINARIES)/$(BINARY)
	rm -f $(LIBRARIES)/$(SHARED)
	rm -f $(LIBRARIES)/$(STATIC)
	rm -rf $(HEADERS)/$(BINARY)

$(OBJECTDIR)/%.o: $(SOURCEDIR)/%
	@echo "]> \033[36mBuilding "$@"...\033[0m"
	@mkdir -p `dirname $@`
	$(CXX) $(CXXFLAGS) -MMD -MP $< -o $@

-include $(DEPENDS)

$(BUILDDIR)/$(SHARED): $(OBJECTS)
	@echo "]> \033[32mLinking shared library...\033[0m"
	@mkdir -p $(BUILDDIR)
	@$(CXX) -o $(BUILDDIR)/$(SHARED) $^ $(LDFLAGS)

$(BUILDDIR)/$(STATIC): $(OBJECTS)
	@echo "]> \033[32mCreating static library...\033[0m"
	@mkdir -p $(BUILDDIR)
	@ar rcs $(BUILDDIR)/$(STATIC) $^

$(BUILDDIR)/$(BINARY): objects/main.cpp.o
	@echo "]> \033[32mLinking executable...\033[0m"
	@mkdir -p $(BUILDDIR)
	$(CXX) -o $(BUILDDIR)/$(BINARY) $^ $(EXECUTABLEFLAGS)

clean:
	rm -rf $(OBJECTDIR)
	rm -f include/options.h
	rm -f include/options.h.out
