# Compiling

```
git submodule update --init --recursive
cd awoken-discord
# compile and install it
cd ..
make configure
make -j$(nproc)
sudo make install
```

You can add some arguments in the format KEY=VALUE.
See the Makefile for examples, they start with `KEY ?= DEFAULT`.
Example: `make -j$(nproc) COMPILE_INFO_MESSAGES=0 CXX=clang`
Some of these arguments are used to fill out the [options template](include/options.h.in).

# Tutorial
When you start the bot for the first time with `ciabot` it will create a tokens.json file.
Replace "Discord":"TOKEN" with "Discord":"YOUR BOT TOKEN FROM DISCORD APPLICATIONS/BOTS/COPY TOKEN".
You can also replace the Yandex Translate token, if you don't then you won't be able to use translation modules.
After that invite your instance of CIABot to a server.
If you own the server you will automatically have Ring-1 execution level.
Use `$help` for a list of command categories.
Have fun and don't let the jannies bite!

# Server
There's a server for CIABot!
Come on down to **x4eKFbT** and join in on the fun.

# Dependencies
Requires [https://bitbucket.org/DeltaNedas/awoken-discord](Awoken Discord) (submodule included) and a C++20 compiler.
