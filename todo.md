# TODO
Permissions: Make 100% sure that the "already-have-permissions" thing is a valid check
Utils: writeFile does not care about some data not being written I think???
Filters: Filter inputs can contain graves and asterisks to break the message. Also support filter options

Add translation command with simple syntax like "$translate to english hola mi amigo"
	Add an image mode where if there are image attachments it will try to translate them with yandex's image translate thing if possible?
Console: Use non blocking reads