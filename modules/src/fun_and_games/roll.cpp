#include <fun_and_games/roll.h>

#include <ciabot/random.h>

CommandOutput CommandRoll::run(CommandInput& input) {
	long min = 1;
	long max = 6;

	size_t argn = 0;
	try {
		this->parseLong(&min, input.args, argn++, 1);
		this->parseLong(&max, input.args, argn++, 6);
	} catch (std::exception& e) {
		return this->error("Invalid argument #" + std::to_string(argn + 1) + ": " + std::string(e.what()));
	}

	std::string result = std::to_string(randomInt(min, max));
	AwokenDiscord::Embed embed;
	embed.color = 14483456;
	embed.description = "Rolled a ***" + result + "***!";
	embed.title = "🎲 Roll **" + std::to_string(min) + "**, **" + std::to_string(max) + "**";

	return CommandOutput("", result, embed);
}

std::string CommandRoll::getName() {
	return "roll";
}

std::string CommandRoll::getUsage() {
	return this->getName() + " *[min = 1] [max = 6]*";
}

CommandOutput CommandRoll::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), "Returns a random number between *min* and *max*." + this->messageRing(serverID));
}

std::set<std::string> CommandRoll::getTypes() {
	return {
		"fun",
		"script",
		"utility"
	};
}