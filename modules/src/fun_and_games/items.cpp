#include <fun_and_games/items.h>

#include <ciabot/util/generics.h>

Item::Item(std::string id, size_t amount, rupees_t price, std::string name) {
	this->id = id;
	this->amount = amount;
	this->price = price;
	this->name = name;
}

json Items::serialise() {
	json ret = emptyObject();

	for (auto& it : this->values) {
		json server = emptyObject();
		for (auto& user : it.second) {
			json items = emptyObject();
			for (auto itemIt : user.second) {
				Item item = itemIt.second;
				if (item.amount) { // Don't save deleted items
					if (user.first == "shop") {
						json jItem = {
							{"name", item.name},
							{"price", item.price}
						};
						if (item.amount > 1) {
							jItem["amount"] = item.amount;
						}
						items[item.id] = jItem;
					} else {
						items[item.id] = item.amount;
					}
				}
			}
			if (items.size()) {
				server[user.first] = items;
			}
		}
		if (server.size()) {
			ret[it.first] = server;
		}
	}
	return ret;
}

int Items::deserialise(json data) {
	if (!data.is_object()) {
		fprintf(stderr, "[%sERROR%s] items.json is not a JSON object.\n",
			Colours::RED, Colours::RESET);
		return EINVAL;
	}

	for (auto serverIt : data.items()) {
		std::string serverName = serverIt.key();
		json server = serverIt.value();
		if (!server.is_object()) {
			fprintf(stderr, "[%sERROR%s] Server %s is not an object!\n",
				Colours::RED, Colours::RESET, serverName.c_str());
			return EINVAL;
		}

		std::map<std::string, std::map<std::string, Item>> users = {};
		for (auto userIt : server.items()) {
			std::string userName = userIt.key();
			json user = userIt.value();
			if (!user.is_object()) {
				fprintf(stderr, "[%sERROR%s] Items for user %s in server %s are not an object!\n",
					Colours::RED, Colours::RESET, userName.c_str(), serverName.c_str());
				return EINVAL;
			}

			std::map<std::string, Item> items = {};
			for (auto itemIt : user.items()) {
				std::string id = itemIt.key(), name = "";
				size_t amount = 1;
				rupees_t price = 0;
				json item = itemIt.value();

				if (userName == "shop") {
					if (!item.is_object()) {
						fprintf(stderr, "[%sERROR%s] Item %s in %s's shop is not an object!\n",
							Colours::RED, Colours::RESET, id.c_str(), serverName.c_str());
						return EINVAL;
					}

					getJsonField(item, "name", name, REQUIRED);
					getJsonField(item, "price", price, REQUIRED);
					getJsonField(item, "amount", amount);
				} else {
					if (!item.is_number()) {
						fprintf(stderr, "[%sERROR%s] Item %s of %s in server %s is not a number!\n",
							Colours::RED, Colours::RESET, id.c_str(), userName.c_str(), serverName.c_str());
						return EINVAL;
					}
					item.get_to(amount);
				}
				items[id] = Item(id, amount, price, name);
			}
			users[userName] = items;
		}
		this->values[serverName] = users;
	}

	return 0;
}

void Items::fallback() {}

Item Items::add(Item add, std::string user, std::string server) {
	Item item = this->get(add.id, user, server);
	item.id = add.id;
	item.name = add.name;
	item.amount += item.amount;
	return this->set(item, user, server);
}
Item Items::sub(Item sub, std::string user, std::string server) {
	Item item = this->get(sub.id, user, server);
	item.id = sub.id;
	item.name = sub.name;
	item.amount -= sub.amount;
	return this->set(item, user, server);
}

Item Items::set(Item set, std::string user, std::string server) {
	auto items = this->get(user, server);
	items[set.id] = set;
	this->set(items, user, server);
	printInfo(InfoLevel::DATA_MODIFIED, "[%sDATA%s] Item %s%s%s set to %f for user %s in %s.\n",
		Colours::BLUE, Colours::RESET, set.amount, user.c_str(), server.c_str());
	return set;
}
std::map<std::string, Item> Items::set(std::map<std::string, Item> set, std::string user, std::string server) {
	auto users = this->get(server);
	users[user] = set;
	this->values[server] = users;
	return set;
}
std::map<std::string, std::map<std::string, Item>> Items::set(std::map<std::string, std::map<std::string, Item>> set, std::string server) {
	this->values[server] = set;
	return set;
}

Item Items::get(std::string id, std::string user, std::string server) {
	auto items = this->get(user, server);
	return CIABot::get(items, id);
}
std::map<std::string, Item> Items::get(std::string user, std::string server) {
	auto users = this->get(server);
	return CIABot::get(users, user);
}
std::map<std::string, std::map<std::string, Item>> Items::get(std::string server) {
	return CIABot::get(this->values, server);
}

Items items;