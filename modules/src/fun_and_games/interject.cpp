#include <fun_and_games/interject.h>

#include <ciabot/util/strings.h>

std::map<std::string, std::string> interjections = {
	{"linux", R"(I'd just like to interject for a moment. What you’re referring to as Linux, is in fact, GNU/Linux, or as I’ve recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called “Linux”, and many of its users are not aware that it is basically the GNU system, developed by the GNU Project. There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine’s resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called “Linux” distributions are really distributions of GNU/Linux!)"},
	{"ebin", R"(I'd jusd lige do inderjegd for a momend. Whad you’re referring do as lenugz, is in fagd, gnu/lenugz, or as i’ve regendly dagen do galling id, gnu blus lenugz. Linugs is nod an oberading sysdem undo idself, bud radher anodher free gombonend of a fully fungdioning gnu sysdem made useful by dhe gnu gorelibs, shell udilidies and vidal sysdem gombonends gombrising a full os as defined :DD by bosigs. Many gombuder users run a modified :DD version of dhe gnu sysdem every day, widhoud realising id. Dhrough a beguliar durn of evends, dhe version of gnu whigh is widely used :DD doday is ofden galled :DD “lenugz”, and many of ids users are nod aware dhad id is basigally dhe gnu sysdem, develobed :DD by dhe gnu brojegd. Dhere really is a lenugz, and dhese beoble are using id, bud id is jusd a bard of dhe sysdem dhey use. Linugs is dhe gernel: dhe brogram in dhe sysdem dhad allogades dhe maghine’s resourges do dhe odher brograms dhad you run. Dhe gernel is an essendial bard of an oberading sysdem, bud useless by idself; id gan only fungdion in dhe gondegsd of a gomblede oberading sysdem. Linugs is normally used :DD in gombinadion widh dhe gnu oberading sysdem: dhe whole sysdem is basigally gnu widh lenugz added, or gnu/lenugz. All dhe so-galled :DD “lenugz” disdribudions are really disdribudions of gnu/lenugz! :DDDd)"},
	{"windows", R"(I'd just like to interject for a moment. What you’re referring to as Windows, is in fact, NT/Windows, or as I’ve recently taken to calling it, NT plus Windows. NT is not an operating system unto itself, but rather another proprietary component of a nonfunctioning operating system made useful by the Win32 libraries, userland and vital spyware components comprising a full OS as defined by the NSA. Many computer useds run an ancient version of the 25+ year old NT system every day, without realizing it. Through a peculiar turn of events, the version of NT which is widely used today is often called “Windows”, and many of its users are not aware that it is basically the NT system, developed by Microsoft and the NSA. There really is a Windows, and these people are using it, but it is just a part of the spyware they use. NT is the kernel: the program in the system that slowly allocates the machine’s resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. NT is normally used in combination with the Windows operating system: the whole system is basically NT with Windows added, or NT/Windows. All the so-called “Windows” versions are really versions of NT/Windows!
***Side note: NT/Windows only applies to releases after 95.***)"},
};

std::map<std::string, std::string> titles = {
	{"linux", "📙 Install **THIS**!"},
	{"ebin", "📙 inztall **DIS** XDD"},
	{"windows", "📙 Please read the license agreement before installing this software."}
};

std::map<std::string, unsigned int> colours = {
	{"linux", Colours::Discord::YELLOW},
	{"ebin", Colours::Discord::BROWN},
	{"windows", Colours::Discord::BLUE}
};

CommandOutput CommandInterject::run(CommandInput& input) {
	size_t argc = input.args.size();
	std::string type = "linux";

	if (argc > 0) {
		type = lower(concat(input.args, " "));
	}

	auto it = interjections.find(type);
	if (it == interjections.end()) {
		return this->getHelp(input.userData.server);
	}

	return helperOutput(titles[type], it->second, it->second, colours[type]);
}

std::string CommandInterject::getName() {
	return "interject";
}

std::string CommandInterject::getUsage() {
	return this->getName() + " *[__linux__|ebin|windows]*";
}

CommandOutput CommandInterject::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), "Returns a GNU/Linux interjection.\n***Powered by freedom.***" + this->messageRing(serverID));
}

std::set<std::string> CommandInterject::getTypes() {
	return {"fun"};
}