#include <ciabot/commands.h>
#include <ciabot/permissions.h>

#include <fun_and_games/interject.h>
#include <fun_and_games/items.h>
#include <fun_and_games/minesweeper.h>
#include <fun_and_games/roll.h>
#include <fun_and_games/toilet.h>

extern "C" {
	int fun_and_games_load() {
		Permission none;
		Permission ring2(2);

		defaultPermissions["interject.run"] = none;

		defaultPermissions["minesweeper.run"] = none;

		defaultPermissions["roll.run"] = none;

		defaultPermissions["toilet.run"] = none;
		defaultPermissions["toilet.buy"] = none;
		defaultPermissions["toilet.list"] = none;
		defaultPermissions["toilet.rupee"] = none;
		defaultPermissions["toilet.sell"] = ring2;
		defaultPermissions["toilet.send"] = none;
		defaultPermissions["toilet.set"] = ring2;

		int code = readData("items.json", &items);

		loadCommand<CommandInterject>();
		loadCommand<CommandMinesweeper>();
		loadCommand<CommandRoll>();
		loadCommand<CommandToilet>();
		return code;
	}

	int fun_and_games_unload() {
		unloadCommand("interject");
		unloadCommand("minesweeper");
		unloadCommand("roll");
		unloadCommand("toilet");
		return writeData("items.json", &items);
	}

	const char* fun_and_games_getTitle() {
		return "Fun & Games v1.3.1";
	}
}
