#include <fun_and_games/toilet.h>

#include <fun_and_games/items.h>

#include <ciabot/util/generics.h>
#include <ciabot/util/strings.h>

#include <functional>
#include <map>
#include <vector>

// Subcommand errors
const std::string INVALID_USER = "pls try with a needful sir..";
const std::string UNKNOWN_USER = "this sir is not vry kind...";
const std::string ARGS = "pls kind sir use help...";
const std::string NOT_NUMBER = "sir is not kind number.";
const std::string UNKNOWN_ITEM = "kind sir but item is not needful...";
const std::string CANNOT_AFFORD = "sorry kind sir you need each and every needful rupee...";
const std::string NOT_SELF = "you are not very kindly sir....";

class SubcommandInput {
	public:
		CommandInput realInput;
		std::vector<std::string> args = {};
		size_t argc = 0;
		AwokenDiscord::Embed* embed;
		std::string* data;
};

typedef std::function<std::string(SubcommandInput&)> Subcommand;

std::map<std::string, Subcommand> subcommands = {
	{"rupee", [](SubcommandInput& input)->std::string {
		std::string id = "-";
		try {
			Command::parseUser(&id, input.args, 0);
		} catch (std::exception& e) {
			return INVALID_USER;
		}

		if (id == "-") {
			id = input.realInput.userData.user;
		}
		AwokenDiscord::User user;
		if (!getUser(id, &user)) {
			return UNKNOWN_USER;
		}

		std::string rupee = toString(rupees.get(id, input.realInput.userData.server));
		input.embed->title += " - Rupee";
		input.embed->description = "kindly sir **" + user.username + "** have **" + rupee + "** rupee";
		*input.data = rupee;
		return "";
	}},
	{"list", [](SubcommandInput& input)->std::string {
		std::string id = "-";
		if (input.argc > 0) {
			std::string raw = lower(input.args[0]);
			if (compare(raw, {"shop", "mart", "durga-mart"})) {
				id = "shop";
			} else {
				try {
					Command::parseUser(&id, input.args, 0);
				} catch (std::exception& e) {
					return INVALID_USER;
				}
			}
		}

		AwokenDiscord::User user;
		if (id == "-") {
			id = input.realInput.userData.user;
		}
		if (!(id == "shop" || getUser(id, &user))) {
			return UNKNOWN_USER;
		}

		auto list = items.get(id, input.realInput.userData.server);
		bool kind = false;
		input.embed->title += " - List";
		input.embed->description = "kind " + ((id == "shop") ? "durga mart" : "sir **" + user.username + "**") + " item:";
		for (auto it : list) {
			Item item = it.second;
			if (item.amount && item.name.size()) {
				std::string number = id == "shop" ? ((item.amount > 1 ? toString(item.amount) + " for " : "") + toString(item.price) + "₹") : toString(item.amount);
				if (list.size() > 25) {
					input.embed->description += "\n•" + number;
				} else {
					addField(input.embed->fields, item.name, number);
				}
				*input.data += number + ",";
				kind = true;
			}
		}

		if (!kind) {
			input.embed->description = "kind sir hav no item...";
			*input.data = "{}";
			return "";
		}

		*input.data = "{";
		input.data->pop_back();
		*input.data += "}";
		return "";
	}},
	{"buy", [](SubcommandInput& input)->std::string {
		if (input.argc < 1) {
			return ARGS;
		}

		size_t amount = 1, price = 0;
		try {
			amount = std::stoul(input.args[input.argc - 1]);
			input.args.pop_back();
		} catch (std::exception& e) {
			amount = 1;
		}

		std::string item = lower(concat(input.args, " "));
		std::string server = input.realInput.userData.server, user = input.realInput.userData.user;
		Item actualItem = items.get(item, "shop", server);
		if (actualItem.empty()) {
			return UNKNOWN_ITEM;
		}

		price = actualItem.price * amount;
		if (rupees.get(user, server) < price) {
			return CANNOT_AFFORD;
		}
		rupees.sub(price, user, server);

		actualItem.amount *= amount; // Buying 4x of an item that gives 2 per purchase gives 8
		items.add(actualItem, user, server);

		input.embed->title += " - Buy";
		input.embed->description = "tank u kind sir for buy " + toString(actualItem.amount) + " " + actualItem.name + " with needful " + toString(price) + " rupee...";
		return "";
	}},
	{"send", [](SubcommandInput& input)->std::string {
		if (input.argc < 2) {
			return ARGS;
		}

		std::string id = "-";
		size_t amount = 1;
		try {
			Command::parseUser(&id, input.args, 0);
		} catch (std::exception& e) {
			return INVALID_USER;
		}

		if (compare(id, {"-", input.realInput.userData.user})) {
			return NOT_SELF;
		}
		AwokenDiscord::User sir;
		if (!getUser(id, &sir)) {
			return UNKNOWN_USER;
		}

		try {
			amount = std::stoul(input.args[input.argc - 1]);
			input.args.pop_back();
		} catch (std::exception& e) {
			amount = 1;
		}

		input.args.erase(input.args.begin());
		std::string item = lower(concat(input.args, " "));

		std::string server = input.realInput.userData.server, kindSir = input.realInput.userData.user;
		input.embed->title += " - Send ";
		input.embed->description = "your kind sir ";
		if (compare(item, {"", "rupee", "rupees"})) {
			if (input.realInput.userData.rupees < amount) {
				return CANNOT_AFFORD;
			}

			std::string rupeeS = toString(rupees.sub(amount, kindSir, server));
			rupees.add(amount, id, server);
			input.embed->title += "Rupee";
			input.embed->description += "rupee now **" + rupeeS + "** rupee";
			*input.data = rupeeS;
			return "";
		}

		Item actualItem = items.get(item, kindSir, server);
		if (actualItem.empty() || actualItem.amount < amount) {
			return UNKNOWN_ITEM;
		}

		actualItem.amount = amount;
		items.add(actualItem, id, server);
		std::string amountS = toString(items.sub(actualItem, kindSir, server).amount);
		input.embed->title += "Item";
		input.embed->description += "**" + actualItem.name + "** now **" + amountS + "**.";
		*input.data = actualItem.id + " " + amountS;
		return "";
	}},
	{"sell", [](SubcommandInput& input)->std::string {
		if (input.argc < 1) {
			return ARGS;
		}

		size_t amount = 1;
		rupees_t price = 1;

		if (input.argc > 1) {
			// Adjust position of it properly
			bool useAmount = true;
			for (size_t i = 2; i > 0; i--) {
				try {
					price = std::stod(input.args[input.argc - i]);
					input.args.pop_back();
					useAmount = i == 2;
					break;
				} catch (std::exception& e) {
					price = 1;
				}
			}

			if (input.argc > 2 && useAmount) {
				try {
					amount = std::stoul(input.args[input.argc - 1]);
					input.args.pop_back();
				} catch (std::exception& e) {
					amount = 1;
				}
			}
		}

		std::string item = concat(input.args, " "), id = lower(item);
		std::string server = input.realInput.userData.server;
		if (!amount) {
			if (items.get(id, "shop", server).empty()) {
				return UNKNOWN_ITEM;
			}
		}
		Item actualItem(id, amount, price, item);
		items.set(actualItem, "shop", server);

		std::string amountS = toString(amount), priceS = toString(price);
		input.embed->title += "- Sell";
		if (amount) {
			input.embed->description = "durga mart do sell " + amountS + "x**" + item + "** with **" + priceS + "** rupee";
		} else {
			input.embed->description = "durga mart take away **" + item + "**...";
		}
		*input.data = actualItem.id + " " + priceS;
		return "";
	}},
	{"set", [](SubcommandInput& input)->std::string {
		if (input.argc < 2) {
			return ARGS;
		}

		std::string id = "-";
		size_t amount = 1;
		try {
			Command::parseUser(&id, input.args, 0);
		} catch (std::exception& e) {
			return INVALID_USER;
		}

		if (id == "-") {
			id = input.realInput.userData.user;
		}
		AwokenDiscord::User user;
		if (!getUser(id, &user)) {
			return UNKNOWN_USER;
		}

		try {
			amount = std::stoul(input.args[input.argc - 1]);
			input.args.pop_back();
		} catch (std::exception& e) {
			amount = 1;
		}

		input.args.erase(input.args.begin());
		std::string item = lower(concat(input.args, " "));

		std::string server = input.realInput.userData.server;
		input.embed->title += " - Set ";
		input.embed->description = "kind sir **" + user.username + "** ";
		if (compare(item, {"", "rupee", "rupees"})) {
			std::string rupeeS = toString(rupees.set(amount, id, server));
			input.embed->title += "Rupee";
			input.embed->description += "rupee now **" + rupeeS + "** rupee";
			*input.data = rupeeS;
			return "";
		}

		Item actualItem = items.get(item, "shop", server);
		if (actualItem.empty()) {
			return UNKNOWN_ITEM;
		}
		actualItem.amount = amount;
		items.set(actualItem, id, server);

		std::string amountS = toString(amount);
		input.embed->title += "Item";
		input.embed->description += "is **" + actualItem.name + "** now **" + amountS + "**.";
		*input.data = actualItem.id + " " + amountS;
		return "";
	}}
};

CommandOutput CommandToilet::run(CommandInput& input) {
	// Get subcommand mode
	std::string mode = "rupee";
	if (input.args.size()) {
		mode = lower(input.args[0]);
		input.args.erase(input.args.begin());
	}

	// Initialise output
	std::string data = "";
	AwokenDiscord::Embed embed = AwokenDiscord::Embed();
	embed.color = Colours::Discord::BROWN;
	embed.title = "🚽 Durga Mart";

	// Parse subcommands
	auto it = subcommands.find(mode);
	if (it == subcommands.end()) {
		return this->error("kind sir the subcommand is not needful...");
	}

	if (!this->can(input, mode)) {
		return this->permissionError(mode);
	}

	// TODO: Remove it at Year 0
	//if (input.userData.executionLevel) {
	//	return this->error("sorry kind sir but must wait until needful year of superpower 💪💪💪");
	//}

	SubcommandInput si;
	si.realInput = input;
	si.args = input.args;
	si.argc = input.args.size();
	si.data = &data;
	si.embed = &embed;
	std::string err = it->second(si);
	if (err.size()) {
		return this->error(err);
	}
	return CommandOutput("", data, embed);
}

std::string CommandToilet::getName() {
	return "toilet";
}

std::string CommandToilet::getUsage() {
	return this->getName() + " *[subcommand = rupee] [subcommand option...]*";
}

CommandOutput CommandToilet::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), R"(Home of INDIA.
needful subcommands:
`rupee` *[kind sir = you]*
> see a needful sir rupee)" + this->messageRing(serverID, "rupee", "\n> *kind sir need level of **Ring-__{RING}__***") + R"(
`list` *[needful sir = you | shop]*
> see the kind sir item or item sell in durga mart)" + this->messageRing(serverID, "list", "\n> *kind sir need level of **Ring-__{RING}__***") + R"(
`buy` **<needful item>** *[needful amount = 1]*
> buy the items from kind durga mart)" + this->messageRing(serverID, "buy", "\n> *kind sir need level of **Ring-__{RING}__***") + R"(
`send` **<needful sir> <needful item | rupee amount>** *[kind item amount]*
> give kind sir the item or rupee)" + this->messageRing(serverID, "send", "\n> *kind sir need level of **Ring-__{RING}__***") + R"(
`sell` **<needful item> <needful price>** *[needful amount = 1]*
> add item to sale in durga mart)" + this->messageRing(serverID, "sell", "\n> *kind sir need level of **Ring-__{RING}__***") + R"(
`set` **<needful sir> <needful rupee | needful item>** *[needful amount]*
> deposit the kind sir rupee or items)" + this->messageRing(serverID, "set", "\n> *kind sir need level of **Ring-__{RING}__***") + R"(
***For technical support of __CERTIFIED ORACLE REAL TIME PROFESSIONAL__ please do of the needful at __durgasoftonlinetraining@gmail.com__ M. Tech!***
tank u kind sirs)" + this->messageRing(serverID, "run", "\n> *kind sir need level of **Ring-__{RING}__** for durga mart...*"));
}

std::set<std::string> CommandToilet::getTypes() {
	return {
		"fun"
	};
}