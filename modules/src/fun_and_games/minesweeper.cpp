#include <fun_and_games/minesweeper.h>

#include <ciabot/random.h>
#include <ciabot/util/generics.h>

using namespace CIABot;

std::vector<std::string> emojis = {
	"zero",
	"one",
	"two",
	"three",
	"four",
	"five",
	"six",
	"seven",
	"eight"
};

class Vector {
public:
	Vector() {}
	Vector(long x, long y) {
		this->x = x;
		this->y = y;
	}

	inline bool operator==(const Vector& rhs) const {
		return this->x == rhs.x && this->y == rhs.y;
	}
	inline bool operator!=(const Vector& rhs) const {
		return !operator==(rhs);
	}
	inline bool operator>(const Vector& rhs) const {
		return this->x > rhs.x && this->y > rhs.y;
	}

	inline std::string toString() {
		return std::to_string(this->x) + "x" + std::to_string(this->y);
	}

	long x = 0;
	long y = 0;
};

Vector addBomb(Vector start, Vector size, std::vector<Vector> bombs) {
	Vector attempt;
	do {
		attempt.x = randomInt(0, size.x - 1);
		attempt.y = randomInt(0, size.y - 1);
	} while (attempt == start || contains(attempt, bombs)); // Don't overlap start point or existing bombs
	return attempt;
}

std::string generateBoard(Vector start, Vector size, std::vector<Vector> bombs) {
	std::string ret = "";
	for (long y = 0; y < size.y; y++) {
		for (long x = 0; x < size.x; x++) {
			Vector vec(x, y);
			if (vec != start) {
				ret += "||";
			}

			if (contains(Vector(x, y), bombs)) {
				ret += "💥";
			} else {
				// See $toilet, 4 nested for loops jesus christ I'm a pajeet
				size_t nearby = 0;
				Vector edgeX(
					x == 0 ? 0 : -1,
					x == (size.x - 1) ? 1 : 2
				);
				Vector edgeY(
					y == 0 ? 0 : -1,
					y == (size.y - 1) ? 1 : 2
				);
				for (long bX = edgeX.x; bX < edgeX.y; bX++) {
					for (long bY = edgeY.x; bY < edgeY.y; bY++) {
						Vector b(x + bX, y + bY);
						if (b != vec && contains(b, bombs)) {
							nearby++;
						}
					}
				}
				ret += ":" + emojis[nearby] + ":";
			}

			if (vec != start) {
				ret += "||";
			}
		}
		ret += '\n';
	}
	return ret;
}

CommandOutput CommandMinesweeper::run(CommandInput& input) {
	Vector start(3, 3); // Start at 3,3 by default
	Vector size(6, 6);
	long bombCount = 10;
	std::vector<Vector> bombs = {};

	size_t argn = 0;
	size_t argc = input.args.size();
	if (argc) {
		try {
			this->parseLong(&size.x, input.args, argn++);
			this->parseLong(&size.y, input.args, argn++);
			this->parseLong(&bombCount, input.args, argn++);
			this->parseLong(&start.x, input.args, argn++);
			this->parseLong(&start.y, input.args, argn++);
		} catch (std::exception& e) {
			return this->error("Invalid argument #" + toString(argn + 1) + ".");
		}
	}
	if (argc < 5) {
		start.y = randomInt(1, size.y);
		if (argc < 4) {
			start.x = randomInt(1, size.x);
		}
	}
	start.x--;
	start.y--;

	if (Vector(2, 2) > size) {
		return this->error("Board must be 2x2 or greater.");
	}
	if (Vector() > start) {
		return this->error("Start cannot be negative.");
	}
	if (bombCount < 1) {
		return this->error("Not enough bombs!");
	}
	if (size.x > 15 || size.y > 15) {
		return this->error("Board is too big, max is 15x15.");
	}
	if (start > size) {
		return this->error("You cannot start outside of the board!");
	}
	if (bombCount >= size.x * size.y) {
		return this->error("Too many bombs, max is " + toString(size.x * size.y - 1) + ".");
	}

	AwokenDiscord::Embed embed;
	embed.title = "💣 Minesweeper - " + size.toString() + " - " + toString(bombCount) + " bombs";
	embed.color = Colours::Discord::BLACK;
	for (long i = 0; i < bombCount; i++) {
		bombs.push_back(addBomb(start, size, bombs));
	}
	embed.description = generateBoard(start, size, bombs);
	return CommandOutput("", "haha no", embed);
}

std::string CommandMinesweeper::getName() {
	return "minesweeper";
}

std::string CommandMinesweeper::getUsage() {
	return this->getName() + " *[board w = 6] [board h = 6] [bombs = 10] [start x = 3] [start y = 3]*";
}

CommandOutput CommandMinesweeper::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), "Starts a game of minesweeper using spoilers." + this->messageRing(serverID));
}

std::set<std::string> CommandMinesweeper::getTypes() {
	return {
		"fun",
		"games"
	};
}