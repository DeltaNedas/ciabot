#include <utilities/retry.h>

#include <ciabot/info.h>
#include <ciabot/util/strings.h>

using namespace CIABot;

CommandOutput CommandRetry::run(CommandInput& input) {
	size_t argc = input.args.size();
	size_t argn = 0;
	if (argc == 0) {
		return this->getHelp(input.userData.server);
	}

	std::string messageID = "", channelID = "";
	try {
		this->parseSnowflake(&messageID, input.args, argn++);
		this->parseChannel(&channelID, input.args, argn++);
	} catch (std::exception& e) {
		return this->error("Invalid argument #" + std::to_string(argn) + " - " + e.what());
	}
	AwokenDiscord::Message message;

	if (messageID == input.userData.message) {
		return this->error("ok what the fuck you mind reader stop doing that"); // Hard but possible!
	}

	if (!getMessage(messageID, channelID, &message)) {
		return this->error("Invalid channel.");
	}

	if (std::string(message.serverID) != input.userData.server) {
		if (!this->can(input, "external")) {
			return this->permissionError("external");
		}
	}

	printInfo(InfoLevel::MESSAGE, "[%sMESSAGE%s] Retrying message %s...\n",
		Colours::GREEN, Colours::RESET,
		messageID.c_str());
	size_t old = timestamp();
	client->onMessage(message);
	size_t diff = timestamp() - old;

	AwokenDiscord::Embed embed;
	embed.color = Colours::Discord::DELTA;
	embed.title = "♻️ Retry";
	embed.description = "Retried that message in **" + timeToDate(diff, 2) + "**.";
	return CommandOutput("", std::to_string(diff), embed);
}

std::string CommandRetry::getName() {
	return "retry";
}

std::string CommandRetry::getUsage() {
	return this->getName() + " **<message>** *[channel = current]*";
}

CommandOutput CommandRetry::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), R"(Parses a message again.
Checks it against filters, runs any command again.)" + this->messageRing(serverID) + this->messageRing(serverID, "external", "\nRetrying messages in channels not on this server requires an execution level of **Ring-__{RING}__**."));
}

std::set<std::string> CommandRetry::getTypes() {
	return {"utility"};
}