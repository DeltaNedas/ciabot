#include <utilities/edit.h>

#include <ciabot/util/strings.h>

using namespace CIABot;

CommandOutput CommandEdit::run(CommandInput& input) {
	std::string message = "";
	std::string channel = "-";
	if (input.args.size() < 3) {
		return this->getHelp(input.userData.server);
	}

	try {
		this->parseSnowflake(&message, input.args, 0);
		input.args.erase(input.args.begin());
	} catch (std::exception& e) {
		return this->error("Invalid argument #1: " + std::string(e.what()));
	}

	try {
		this->parseChannel(&channel, input.args, 0);
		input.args.erase(input.args.begin());
	} catch (std::exception& ign) {}

	if (channel == "-") {
		channel = input.userData.channel;
	}
	AwokenDiscord::Channel c;
	if (!getChannel(channel, &c)) {
		return this->error("Unknown channel!");
	}

	std::string action = (c.serverID == input.userData.server) ? "run" : "external";
	if (action == "external" + !this->can(input, action)) {
		return this->error("Unknown channel!");
	}

	AwokenDiscord::Message m;
	if (!getMessage(message, channel, &m)) {
		return this->error("Unknown message!");
	}

	if (!editMessage(message, channel, concat(input.args, " "))) {
		return this->error("Failed to edit message!");
	}

	this->can(input, action, true);
	deleteMessage(input.message.ID, input.userData.channel); // Ignore permission error.
	return CommandOutput(); // Don't link it to sender or it may be deleted
}

std::string CommandEdit::getName() {
	return "edit";
}

std::string CommandEdit::getUsage() {
	return this->getName() + " **<message> [channel = current] <message>**";
}

CommandOutput CommandEdit::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), R"(Edits a message I sent.
	If the message is valid JSON, it will use:
	`{"content":"text outside of embed",
	"embed":{"title":"the embed itself"}}`
	If you do not want to use a JSON message escape the first bracket:
	`\{"not": "parsed as json!"}`)" + this->messageRing(serverID));
}

std::set<std::string> CommandEdit::getTypes() {
	return {"script", "utility"};
}