#include <utilities/shutdown.h>

#include <ciabot/events.h>
#include <ciabot/permissions.h>

using namespace CIABot;

CommandOutput CommandShutdown::run(CommandInput& input) {
	std::string action = "run";
	size_t time = 0;
	if (input.args.size() > 0) {
		if (!this->can(input, "delay")) {
			this->permissionError("delay");
		}

		try {
			time = stoul(input.args[0]);
		} catch (std::exception &e) {
			return this->error("Delay must be a number.");
		}

		if (time > 0) {
			if (input.userData.fake) {
				printf(">> Shutting down in %s%lu%s seconds...\n",
					Colours::BOLD, time, Colours::RESET);
			} else {
				sendMessage(input.userData.channel, helperOutput("💀 Shutting down in **" + std::to_string(time) + "** seconds... 💀"), input.userData.message);
			}
			action = "delay";
		}
	}

	if (input.userData.fake) {
		printInfo(InfoLevel::STOP, "[STOP] 💀 The sun has set on the CIA. 💀\n");
	} else {
		sendMessage(input.userData.channel, helperOutput("💀 The sun has set on the CIA. 💀"), input.userData.message);
	}

	this->can(input, action, true); // Pay for it if at all
	tasks->add([](size_t delta){
		quit();
	}, time * 1e9);

	return CommandOutput(); // Not sent
}

std::string CommandShutdown::getName() {
	return "shutdown";
}

std::string CommandShutdown::getUsage() {
	return this->getName() + " *[delay = 0 seconds]*";
}

CommandOutput CommandShutdown::getHelp(std::string serverID) {
	std::string runExec = std::to_string((int) permissions.get(this->getName(), serverID)->executionLevel);
	return helperHelp(this->getUsage(), "Shuts CIABot down after **x** seconds.\nRequires an execution level of **Ring-__" + runExec + "__.");
}

std::set<std::string> CommandShutdown::getTypes() {
	return {"owner"};
}