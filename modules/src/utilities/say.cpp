#include <utilities/say.h>

#include <ciabot/util/strings.h>

using namespace CIABot;

CommandOutput CommandSay::run(CommandInput& input) {
	if (input.args.empty()) {
		return this->getHelp(input.userData.server);
	}

	if (sendMessage(input.userData.channel, concat(input.args, " ")).empty()) {
		return this->error("Failed to send message!");
	}

	this->can(input, "run", true);
	deleteMessage(input.message.ID, input.userData.channel); // Ignore permission error.
	return CommandOutput(); // Don't link it to sender or it may be deleted
}

std::string CommandSay::getName() {
	return "say";
}

std::string CommandSay::getUsage() {
	return this->getName() + " **<message>**";
}

CommandOutput CommandSay::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), R"(Sends a message and deletes yours.
	If the message is valid JSON, it will use:
	`{"content":"text outside of embed",
	"embed":{"title":"the embed itself"},
	"tts": false}`
	If you do not want to send a JSON message escape the first bracket:
	`\{"not": "parsed as json!"}`)" + this->messageRing(serverID));
}

std::set<std::string> CommandSay::getTypes() {
	return {"script", "utility"};
}