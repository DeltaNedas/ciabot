#include <ciabot/commands.h>
#include <ciabot/permissions.h>

#include <utilities/edit.h>
#include <utilities/retry.h>
#include <utilities/say.h>
#include <utilities/shutdown.h>

extern "C" {
	int utilities_load() {
		Permission ring0(0);
		Permission ring2(2);

		defaultPermissions["edit.run"] = ring2;
		defaultPermissions["edit.external"] = ring2;

		defaultPermissions["retry.run"] = ring2;
		defaultPermissions["retry.external"] = ring0;

		defaultPermissions["say.run"] = ring2;

		defaultPermissions["shutdown.run"] = ring0;
		defaultPermissions["shutdown.delay"] = ring0;

		loadCommand<CommandEdit>();
		loadCommand<CommandRetry>();
		loadCommand<CommandSay>();
		loadCommand<CommandShutdown>();
		return 0;
	}

	const char* utilities_getTitle() {
		return "Utilities v1.1.0";
	}
}
