#include <information/question.h>

#include <ciabot/info.h>
#include <ciabot/util/strings.h>

#include <information/questions.h>

using namespace CIABot;

CommandOutput CommandQuestion::run(CommandInput& input) {
	if (input.args.empty()) {
		return this->getHelp(input.userData.server);
	}

	// Do nothing if user is blacklisted
	if (!questionsBlacklist.get(input.userData.user)) {
		AwokenDiscord::User user;
		if (getUser(input.userData.user, &user)) {
			std::string content = concat(input.args, " ");

			Question* question = questions.set(new Question(content, input.userData.message, input.userData.user));
			printInfo(InfoLevel::QUESTION, "[%sQUESTION%s] %s (%s) asked question #%lu: %s%s%s\n",
				Colours::BLUE, Colours::RESET,
				user.username.c_str(), input.userData.user.c_str(),
				question->index - 1,
				Colours::ITALIC, content.c_str(), Colours::RESET);

			AwokenDiscord::Embed embed;
			embed.title = "❔ **" + user.username + "**#**" + user.discriminator + "** *(" + input.userData.user
				+ ")* asked question #**" + std::to_string(question->index - 1) + "**:";
			embed.color = Colours::Discord::YELLOW;
			embed.description = content;
			std::string id = sendMessage("642793575557627904", json({{"embed", embed}, {"tts", true}}), input.userData.message); // Set to your own Q&A channel if you want.
			if (id.empty()) {
				return this->error("Failed to ask question! **Permissions are probably set up wrong.**\n");
			}
		}
	}
	return helperOutput("❔ Question", "Your question was asked, you will be DM'd the answer.");
}

std::string CommandQuestion::getName() {
	return "question";
}

std::string CommandQuestion::getUsage() {
	return this->getName() + " **<your question>**";
}

CommandOutput CommandQuestion::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), R"(Asks a question to CIABot's developers.
*__Please do not spam__ :)*)" + this->messageRing(serverID));
}

std::set<std::string> CommandQuestion::getTypes() {
	return {"info"};
}