#include <information/info.h>

#include <ciabot/util/generics.h>
#include <ciabot/util/strings.h>

#include <information/infoparts.h>

using namespace CIABot;

CommandOutput CommandInfo::run(CommandInput& input) {
	size_t argc = input.args.size();
	std::map<std::string, InfoPart*> parts = {};
	if (argc == 0) {
		parts = infoParts;
	} else {
		for (size_t i = 0; i < argc; i++) {
			std::string arg = lower(input.args[i]);
			if (arg == "list") {
				return helperOutput("📰 Information Types", "Available information types are: *" + concat(keys(infoParts), ", ")  + "*");
			}

			auto it = infoParts.find(arg);
			if (it == infoParts.end()) {
				return this->error("Unknown type #" + std::to_string(i) + " - \"" + arg + "\"!\n*See `info list` for available types.*");
			}

			InfoPart* part = it->second;
			auto existIt = parts.find(arg);
			if (existIt != parts.end()) {
				parts[arg] = part;
			}
		}
	}

	AwokenDiscord::Embed embed;
	embed.color = Colours::Discord::DELTA;
	embed.title = "📰 CIABot Info";
	std::vector<AwokenDiscord::EmbedField> fields;
	std::vector<std::string> data = {};

	for (auto it : parts) {
		std::string type = it.first;
		InfoPart* part = it.second;
		std::string name = "";
		std::string value = "";
		int code = part->process(&input, &name, &value, &embed);

		if (code) {
			fprintf(stderr, "[%sERROR%s] Info type \"%s\" returned exit code %s%d%s!\n",
				Colours::RED, Colours::RESET,
				type.c_str(),
				Colours::RED, code, Colours::RESET);
			return this->error("Failed to process info type \"" + type + "\"!");
		}

		if (!(name.empty() || value.empty())) {
			addField(fields, name, value);
			data.push_back(value);
		}
	}

	this->can(input, "run", true);

	embed.fields = fields;
	return CommandOutput("", concat(data, " "), embed);
}

std::string CommandInfo::getName() {
	return "info";
}

std::string CommandInfo::getUsage() {
	return this->getName() + "*[types = all]*";
}

CommandOutput CommandInfo::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), R"(Returns info on CIABot.
You can list available information types with `list`.)" + this->messageRing(serverID));
}

std::set<std::string> CommandInfo::getTypes() {
	return {"info"};
}