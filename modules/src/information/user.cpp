#include <information/user.h>

#include <ciabot/util/generics.h>
#include <ciabot/util/strings.h>

#include <information/infoparts.h>

using namespace CIABot;

CommandOutput CommandUser::run(CommandInput& input) {
	std::string id = "-";
	AwokenDiscord::User user;
	AwokenDiscord::Server server;
	size_t argc = input.args.size();
	std::map<std::string, UserInfoPart*> parts = userInfoParts;

	if (argc > 0) {
		try {
			this->parseUser(&id, input.args, 0);
		} catch (std::exception& e) {
			return this->error("First argument must be a user.");
		}

		if (argc > 1) {
			parts = {};
			for (size_t i = 1; i < argc; i++) {
				std::string arg = lower(input.args[i]);
				if (arg == "list") {
					return helperOutput("📰 User Info Types", "Available user info types are: *" + concat(keys(userInfoParts), ", ")  + "*");
				}

				auto it = userInfoParts.find(arg);
				if (it == userInfoParts.end()) {
					return this->error("Unknown type #" + std::to_string(i) + " - \"" + arg + "\"!\n*See `user list` for available types.*");
				}

				parts[arg] = it->second;
			}
		}
	}

	if (compare(id, {"-", "me"})) {
		id = input.userData.user;
	}
	if (!getUser(id, &user, false, (size_t) 15e9)) {
		return this->error("Invalid user.");
	}

	if (!(input.userData.dm || getServer(input.userData.server, &server))) {
		return this->error("Invalid server!? how??!??!?! >:S");
	}

	AwokenDiscord::Embed embed;
	embed.color = Colours::Discord::DELTA;
	embed.title = "📰 User Info";
	std::vector<AwokenDiscord::EmbedField> fields;
	std::vector<std::string> data = {};

	for (auto it : parts) {
		std::string type = it.first;
		UserInfoPart* part = it.second;
		std::string name = "";
		std::string value = "";
		int code = part->process(user, server, &name, &value, &embed);

		if (code) {
			if (code != 1) { // Set to 1 to use your own message
				fprintf(stderr, "[%sERROR%s] User Info type \"%s\" returned exit code %s%d%s!\n",
					Colours::RED, Colours::RESET,
					type.c_str(),
					Colours::RED, code, Colours::RESET);
			}
			return this->error("Failed to process user info type \"" + type + "\"!");
		}

		if (!(name.empty() || value.empty())) {
			addField(fields, name, value);
			data.push_back(value);
		}
	}

	this->can(input, "run", true);

	embed.fields = fields;
	return CommandOutput("", concat(data, " "), embed);
}

std::string CommandUser::getName() {
	return "user";
}

std::string CommandUser::getUsage() {
	return this->getName() + "**<user = you>** *[properties = all]*";
}

CommandOutput CommandUser::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), R"(Returns info on a user.
You can list available properties with `list`.)" + this->messageRing(serverID));
}

std::set<std::string> CommandUser::getTypes() {
	return {"info"};
}