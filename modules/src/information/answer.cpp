#include <information/answer.h>

#include <ciabot/random.h>
#include <ciabot/util/strings.h>

#include <information/questions.h>

using namespace CIABot;

std::vector<std::string> answerTitles = {
	"**{!ANSWERER} PROVIDES ANSWERS FOR THE GREAT QUESTIONS #{INDEX}**", // FORTUNE PROVIDES QUESTIONS FOR THE GREAT ANSWERS
	"{ANSWERER} answered your question.",
	"{USERNAME}, your question has an answer!"
};

std::string generateTitle() {
	return "❗ " + answerTitles[randomInt(0, answerTitles.size() - 1)];
}

CommandOutput CommandAnswer::run(CommandInput& input) {
	size_t argc = input.args.size();
	if (argc == 0) {
		return this->getHelp(input.userData.server);
	}

	size_t index;
	std::string indexS = input.args[0];
	try {
		index = std::stoul(indexS) - 1;
	} catch (std::exception& e) {
		return this->error("Index must be a number.");
	}

	Question* question = questions.get(index);
	if (question == nullptr) {
		return this->error("Question #**" + indexS + "** has not been asked.");
	}

	if (argc > 1) {
		AwokenDiscord::User user;
		if (!getUser(question->questionUser, &user)) {
			return this->error("Failed to get the question's asker! *Are they banned?*");
		}

		AwokenDiscord::User answerer;
		if (!getUser(input.userData.user, &answerer)) {
			return this->error("Failed to get your user!!? ;d");
			// Either nasty bug or it's moments before "ON THE GWOUND, FWUCKOWO! *nuzzles child*" (discord mods are pedo furries)
		}

		std::string title = replace(generateTitle(), {
			{"\\{ID\\}", user.username},
			{"\\{USERNAME\\}", user.username},
			{"\\{!USERNAME\\}", upper(user.username)},
			{"\\{ANSWERER\\}", answerer.username},
			{"\\{!ANSWERER\\}", upper(answerer.username)},
			{"\\{INDEX\\}", indexS}
		});

		input.args.erase(input.args.begin());
		std::string answer = concat(input.args, " ");

		AwokenDiscord::Embed embed;
		embed.color = Colours::Discord::DELTA;
		embed.title = title;

		std::vector<AwokenDiscord::EmbedField> fields;
		addField(fields, "Question:", question->question);
		addField(fields, "Answer:", answer);
		embed.fields = fields;
		if (sendMessage(question->questionUser, json({{"embed", embed}, {"tts", true}}), input.userData.message).empty()) {
			return this->error("Failed to DM the answer! Am I blocked? :(");
		}

		// Update it so it's saved on unload.
		question->answer = answer; // Will override any previous answers!
		question->answerMessage = input.userData.message;
		question->answerUser = input.userData.user;
	} else {
		questionsBlacklist.insert(question->questionUser);
	}

	return helperOutput("❗ Answer", "Answered question #**" + indexS + "**.", indexS);
}

std::string CommandAnswer::getName() {
	return "answer";
}

std::string CommandAnswer::getUsage() {
	return this->getName() + " **<index> <response>**"; // Response isn't actually required, it's absence = blacklist user.
}

CommandOutput CommandAnswer::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), "(Answers a question by its index." + this->messageRing(serverID));
}

std::set<std::string> CommandAnswer::getTypes() {
	return {"info", "owner"};
}