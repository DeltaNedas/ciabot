#include <information/offline.h>

using namespace CIABot;

CommandOutput CommandOffline::run(CommandInput& input) {
	if (client->offline) {
		return helperOutput("Offline", "All systems offline.");
	}
	return helperOutput("Offline", "Nope, I'm online!");
}

std::string CommandOffline::getName() {
	return "offline";
}

CommandOutput CommandOffline::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), "Test if I'm in the matrix." + this->messageRing(serverID));
}

std::set<std::string> CommandOffline::getTypes() {
	return {"utility"};
}
