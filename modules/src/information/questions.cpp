#include <information/questions.h>

#include <ciabot/util/json.h>

using namespace CIABot;

Question::Question(std::string question, std::string questionMessage, std::string questionUser,
	std::string answer, std::string answerMessage, std::string answerUser) {
	this->question = question;
	this->questionMessage = questionMessage;
	this->questionUser = questionUser;
	this->answer = answer;
	this->answerMessage = answerMessage;
	this->answerUser = answerUser;
}

json Questions::serialise() {
	json ret = emptyArray();

	for (Question* question : this->values) {
		json obj = emptyObject();

		json qObj = emptyObject();
		qObj["content"] = question->question;
		qObj["message"] = question->questionMessage;
		qObj["user"] = question->questionUser;
		obj["question"] = qObj;

		if (!question->answer.empty()) {
			json aObj;
			aObj["content"] = question->answer;
			aObj["message"] = question->answerMessage;
			aObj["user"] = question->answerUser;
			obj["answer"] = aObj;
		}

		delete question;
		ret.push_back(obj);
	}
	return ret;
}

int Questions::deserialise(json data) {
	if (!data.is_array()) {
		fprintf(stderr, "[%sERROR%s] questions.json is not a JSON array.\n",
			Colours::RED, Colours::RESET);
		return EINVAL;
	}

	for (size_t i = 0; i < data.size(); i++) {
		json jsonQuestion = data[i];
		if (!jsonQuestion.is_object()) {
			fprintf(stderr, "[%sERROR%s] Question %lu is not an object!\n",
				Colours::RED, Colours::RESET, i);
			return EINVAL;
		}

		std::string qContent= "", qMessage= "", qUser= "", aContent= "", aMessage= "", aUser= "";
		if (this->serialisePart(jsonQuestion, "question", qContent, qMessage, qUser)) {
			return EINVAL;
		}
		if (this->serialisePart(jsonQuestion, "answer", aContent, aMessage, aUser)) {
			return EINVAL;
		}

		Question* question = new Question(qContent, qMessage, qUser, aContent, aMessage, aUser);
		question->index = this->values.size();
		this->values.push_back(question);
	}

	return 0;
}

void Questions::fallback() {}

Question* Questions::erase(Question* erase) {
	for (size_t i = 0; i < this->values.size(); i++) {
		Question* compare = this->values[i];
		if (compare == erase) {
			this->erase(i);
			return compare;
		}
	}

	return nullptr;
}

Question* Questions::erase(size_t index) {
	if (index >= this->values.size()) {
		return nullptr;
	}

	Question* result = this->values[index];
	this->values.erase(this->values.begin() + index);
	return result;
}

Question* Questions::set(Question* set) {
	Question* result = set;
	result->index = this->values.size();
	this->values.push_back(result);
	return result;
}

Question* Questions::set(Question* set, size_t index) {
	Question* result = set;
	this->values[index] = result;
	result->index = index;
	return result;
}

Question* Questions::get(size_t index) {
	if (index >= this->values.size()) {
		return nullptr;
	}
	return this->values[index];
}

// Protected
int Questions::serialisePart(json question, std::string name, std::string& content, std::string& message, std::string& user) {
	auto it = question.find(name);
	if (it != question.end()) {
		json part = it.value();
		if (!part.is_object()) {
			fprintf(stderr, "[%sERROR%s] Question %s part is not an object!\n",
				Colours::RED, Colours::RESET, name.c_str());
			return EINVAL;
		}

		try {
			getJsonField(part, "content", content);
			getJsonField(part, "message", message);
			getJsonField(part, "user", user);
		} catch (std::exception& e) {
			fprintf(stderr, "[%sERROR%s] Question %s part is invalid!\n",
				Colours::RED, Colours::RESET, name.c_str());
			return EINVAL;
		}
	}
	return 0;
}

Questions questions;


json QuestionsBlacklist::serialise() {
	json ret = this->values;
	return ret;
}

int QuestionsBlacklist::deserialise(json data) {
	if (!data.is_array()) {
		fprintf(stderr, "[%sERROR%s] questions_blacklist.json is not a JSON array.\n",
			Colours::RED, Colours::RESET);
		return EINVAL;
	}

	for (size_t i = 0; i < data.size(); i++) {
		json user = data[i];
		if (!user.is_string()) {
			fprintf(stderr, "[%sERROR%s] User %lu for questions blacklist is not a string!\n",
				Colours::RED, Colours::RESET, i);
			return EINVAL;
		}

		this->values.push_back(user.get<std::string>());
	}

	return 0;
}

void QuestionsBlacklist::fallback() {}

std::string QuestionsBlacklist::insert(std::string insert) {
	std::string result = insert;
	this->values.push_back(insert);
	return result;
}

std::string QuestionsBlacklist::erase(std::string erase) {
	for (size_t i = 0; i < this->values.size(); i++) {
		std::string comp = this->values[i];
		if (comp == erase) {
			this->erase(i);
			return comp;
		}
	}
	return "";
}

std::string QuestionsBlacklist::erase(size_t index) {
	if (index >= this->values.size()) {
		return "";
	}

	std::string ret = this->values[index];
	this->values.erase(this->values.begin() + index);
	return ret;
}

std::string QuestionsBlacklist::set(std::string set, size_t index) {
	std::string result = set;
	this->values[index] = result;
	return result;
}

bool QuestionsBlacklist::get(std::string user) {
	for (std::string comp : this->values) {
		if (comp == user) {
			return true;
		}
	}
	return false;
}

std::string QuestionsBlacklist::get(size_t index) {
	if (index >= this->values.size()) {
		return "";
	}
	return this->values[index];
}

QuestionsBlacklist questionsBlacklist;