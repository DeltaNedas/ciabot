#include <information/invite.h>

#include <ciabot/random.h>

using namespace CIABot;

const std::string invite = "https://discordapp.com/api/oauth2/authorize?client_id=637355959886413834&permissions=0&scope=bot";

std::vector<std::string> inviteTexts = {
	"Liftoff!",
	"Your plague has infected a new country.",
	"Let'sa go!",
	"Onward!"
};

std::string randomInviteText() {
	return inviteTexts[randomInt(0, inviteTexts.size() - 1)];
}

CommandOutput CommandInvite::run(CommandInput& input) {
	AwokenDiscord::Embed embed;
	embed.color = Colours::Discord::PINK;
	embed.title = "💌 CIABot Invite Link";
	embed.description = randomInviteText();
	embed.url = invite;

	return CommandOutput("", invite, embed);
}

std::string CommandInvite::getName() {
	return "invite";
}

std::string CommandInvite::getUsage() {
	return this->getName();
}

CommandOutput CommandInvite::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), "Returns an invite link for CIABot to add to your server." + this->messageRing(serverID));
}

std::set<std::string> CommandInvite::getTypes() {
	return {"info"};
}