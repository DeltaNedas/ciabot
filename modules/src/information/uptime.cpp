#include <information/uptime.h>

#include <ciabot/client.h>
#include <ciabot/util.h>

using namespace CIABot;

CommandOutput CommandUptime::run(CommandInput& input) {
	int measurements = 3;
	if (input.args.size() > 0) {
		if (!this->can(input, "measurements")) {
			return this->permissionError("measurements");
		}
		try {
			measurements = stoi(input.args[0]);
		} catch (std::exception& e) {
			measurements = 0;
		}

		if (measurements < 1) {
			return this->error("Measurements must be a positive, non-zero number.");
		}
	}

	size_t diff = timestamp() - Client::startedAt;
	return helperOutput("🕜 Uptime", "CIABot has been up for **" + timeToDate(diff, measurements) + "**.", std::to_string(diff));
}

std::string CommandUptime::getName() {
	return "uptime";
}

std::string CommandUptime::getUsage() {
	return this->getName() + "[measurements = 3]";
}

CommandOutput CommandUptime::getHelp(std::string serverID) {
	return helperHelp(this->getUsage(), "Returns CIABot's uptime, to a number of significant measurements." + this->messageRing(serverID));
}

std::set<std::string> CommandUptime::getTypes() {
	return {"info"};
}