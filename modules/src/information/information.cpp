#include <ciabot/commands.h>
#include <ciabot/permissions.h>

#include <information/answer.h>
#include <information/info.h>
#include <information/infoparts.h>
#include <information/invite.h>
#include <information/offline.h>
#include <information/question.h>
#include <information/questions.h>
#include <information/uptime.h>
#include <information/user.h>

using namespace CIABot;

extern "C" {
	int information_load() {
		infoParts = {
			{"version", new VersionPart()},
			{"commands", new CommandsPart()},
			{"translation", new TranslationPart()}
		};
		userInfoParts = {
			{"id", new IDPart()},
			{"username", new UsernamePart()},
			{"discriminator", new DiscriminatorPart()},
			{"avatar", new AvatarPart()},
			{"joined", new JoinedPart()},
			{"created", new CreatedPart()},
			{"profile", new ProfilePart()},
			{"nitro", new NitroPart()},
			{"boosting", new BoostingPart()}
		};

		Permission ring0(0);
		Permission none;

		defaultPermissions["answer.run"] = ring0;

		defaultPermissions["info.run"] = none;

		defaultPermissions["invite.run"] = none;

		defaultPermissions["offline.run"] = none;

		defaultPermissions["question.run"] = none;

		defaultPermissions["uptime.run"] = none;
		defaultPermissions["uptime.measurements"] = none;

		defaultPermissions["user.run"] = none;

		readData("questions.json", &questions);
		readData("questions_blacklist.json", &questionsBlacklist);

		loadCommand<CommandAnswer>();
		loadCommand<CommandInfo>();
		loadCommand<CommandInvite>();
		loadCommand<CommandOffline>();
		loadCommand<CommandQuestion>();
		loadCommand<CommandUptime>();
		loadCommand<CommandUser>();
		return 0;
	}

	int information_unload() {
		for (auto it : infoParts) {
			delete it.second;
		}
		for (auto it : userInfoParts) {
			delete it.second;
		}

		unloadCommand("answer");
		unloadCommand("info");
		unloadCommand("invite");
		unloadCommand("offline");
		unloadCommand("question");
		unloadCommand("uptime");
		unloadCommand("user");
		return writeData("questions.json", &questions)
			|| writeData("questions_blacklist.json", &questionsBlacklist);
	}

	const char* information_getTitle() {
		return "Information v1.2.0";
	}
}
