#include <information/infoparts.h>

#include <ciabot/commands.h>
#include <ciabot/util.h>
#include <ciabot/util/discord.h>
#include <ciabot/util/generics.h>
#include <ciabot/util/strings.h>
#include <ciabot/version.h>

namespace CIABot {
	InfoPart::InfoPart() {}
	InfoPart::~InfoPart() {}

	int InfoPart::process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		return 0;
	}

	int VersionPart::process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		*name = "Version";
		*value = version;
		return 0;
	}

	int CommandsPart::process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		*name = "Commands";
		*value = std::to_string(commands.size());
		return 0;
	}

	int TranslationPart::process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		*name = "Translation API";
		*value = "Powered by **Yandex.Translate**.";
		return 0;
	}

	// User Info

	UserInfoPart::UserInfoPart() {}
	UserInfoPart::~UserInfoPart() {}

	int UserInfoPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		return 0;
	}

	int IDPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		*name = "User **ID**";
		*value = user.ID;
		return 0;
	}

	int UsernamePart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		std::string id(user.ID);
		std::string serverId(server.ID);
		std::string title = "Username";
		std::string username = user.username;

		AwokenDiscord::ServerMember member;
		if (getMember(id, serverId, &member) && member.nick.size() > 0) {
			title = "**Username** *(Nickname)*";
			username = "**" + username + "** *("+ member.nick + ")*";
		}
		*name = title;
		*value = username;
		return 0;
	}

	int DiscriminatorPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		*name = "Discriminator";
		*value = user.discriminator;
		return 0;
	}

	int AvatarPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		AwokenDiscord::EmbedThumbnail avatar;
		std::string url = "";
		if (user.avatar.empty()) {
			try {
				int discrim = stoi(user.discriminator);
				url = "https://cdn.discordapp.com/embed/avatars/" + std::to_string(discrim % 5) + ".png";
			} catch (std::exception& e) {
				return 1;
			}
		} else {
			url = "https://cdn.discordapp.com/avatars/" + std::string(user.ID) + "/" + user.avatar + std::string(match(user.avatar, "^a_").empty() ? ".png" : ".gif");
		}
		avatar.url = url;
		embed->thumbnail = avatar;
		return 0;
	}

	int JoinedPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		std::string id(user.ID);
		std::string serverId(server.ID);

		AwokenDiscord::ServerMember member;
		if (!getMember(id, serverId, &member) || member.joinedAt.empty()) {
			// User hasn't joined
			return 0;
		}

		size_t joined = dateToUnix(member.joinedAt);

		std::string str = timestampStr("At %Y/%m/%d - %I:%M:%S %p UTC", joined, 256);
		std::string ago = timeToDate(timestamp() - joined) + " ago";
		*name = "User **Joined**";
		*value = str + "\n" + ago;
		return 0;
	}

	int CreatedPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		size_t created = createdAt(user.ID);
		std::string str = timestampStr("At %Y/%m/%d - %I:%M:%S %p UTC", created, 256);
		std::string ago = timeToDate(timestamp() - created) + " ago";
		*name = "User **Created**";
		*value = str + "\n" + ago;
		return 0;
	}

	int ProfilePart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		std::string id(user.ID);
		*name = "**Profile** *(click)*";
		*value = "<@" + id + ">";
		return 0;
	}

	int NitroPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		if (user.premiumType) {
			*name = "**Nitro** type";
			std::string type = "Nitro Classic";
			switch (user.premiumType) {
				case 1: break;
				case 2:
					type = "Nitro + Games";
					break;
				default:
					fprintf(stderr, "[%sERROR%s] Unknown premium type %d for user %s%s%s (%s).\n",
						Colours::RED, Colours::RESET,
						user.premiumType,
						Colours::BOLD, user.username.c_str(), Colours::RESET,
						std::string(user.ID).c_str());
					return 1;
			}
		}
		return 0;
	}

	int BoostingPart::process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed) {
		std::string id(user.ID);
		std::string serverId(server.ID);

		AwokenDiscord::ServerMember member;
		if (getMember(id, serverId, &member) && member.boostedAt.size()) {
			size_t boosted = dateToUnix(member.boostedAt);
			std::string str = timestampStr("At %Y/%m/%d - %I:%M:%S %p UTC", boosted, 256);
			std::string ago = timeToDate(timestamp() - boosted) + " ago";
			*name = "Boosted";
			*value = str + "\n" + ago;
		}
		return 0;
	}

	std::map<std::string, InfoPart*> infoParts = {};
	std::map<std::string, UserInfoPart*> userInfoParts = {};
}