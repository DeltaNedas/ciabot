#include <ciabot/commands.h>
#include <ciabot/permissions.h>
#include <ciabot/util/discord.h>

using namespace CIABot;

class CommandBeep : public Command {
	public:
		CommandOutput run(CommandInput& input) {
			return CommandOutput("boop"); // Returned output is automatically sent to the caller
		}
		std::string getName() {
			return "beep"; // Used for easy command loading, permissions, etc.
		}
		CommandOutput getHelp() {
			return helperHelp(this->getUsage(), "Responds with \"boop\".");
		}
};

// In the case of a command lacking a default permission, it will always
// return "Permission... DENIED" and print an error in the log.
// Make sure to do your permissions properly!

extern "C" {
	int beep_load() {
		defaultPermissions["beep.run"] = Permission(); // No execution level required
		loadCommand<CommandBeep>(); // Easy one-liner to register a command
		return 0; // EXIT_SUCCESS, no errors
	}
	// Unload is not needed because all commands are deleted on shutdown, nothing special is done.

	const char* beep_getTitle() { // Only mandatory function, should not include "module"; $modules is pretty obvious as to what it lists.
		return "Beep v1.0";
	}
}
