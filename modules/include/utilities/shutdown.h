#pragma once
#include <ciabot/commands.h>

using namespace CIABot;

class CommandShutdown : public Command {
	public:
		CommandOutput run(CommandInput& input);
		std::string getName();
		std::string getUsage();
		CommandOutput getHelp(std::string serverID);
		std::set<std::string> getTypes();
};