#pragma once
#include <ciabot/userdata.h>

#include <set>

using namespace CIABot;

// TODO: Maybe lookup if that question has an answer, then say "Oooh it has an answer:\n*Answer*"
class Question {
	public:
		Question(std::string question = "", std::string questionMessage = "", std::string questionUser = "",
			std::string answer = "", std::string answerMessage = "", std::string answerUser = "");

		std::string question = "";
		std::string questionMessage = "";
		std::string questionUser = "";
		std::string answer = "";
		std::string answerMessage = "";
		std::string answerUser = "";
		size_t index = 0; // Set by Questions::deserialise()
};

class Questions : public JsonData {
	public:
		json serialise();
		int deserialise(json data);
		void fallback();
		std::vector<Question*> values = {};
		/*
		[
			{
				"question": {
					"content": "How do I do X?",
					"message": 12345,
					"user": 67890
				},
				"answer": {
					"content": "You do Y: `boost::y`.",
					"message": 01234,
					"user": 56789
				}
			},
			{
				"question": {
					"content": "How do I do X, without doing Y or Z?",
					"message": 12345,
					"user": 67890
				}
			}
		]
		*/

		Question* erase(Question* erase);
		Question* erase(size_t index);
		Question* set(Question* set);
		Question* set(Question* set, size_t index);
		Question* get(size_t index);
	protected:
		int serialisePart(json question, std::string name, std::string& content, std::string& message, std::string& user);
};

class QuestionsBlacklist : public JsonData {
	public:
		json serialise();
		int deserialise(json data);
		void fallback();
		std::vector<std::string> values = {};
		/*
		[
			"spammer_id"
		]
		*/

		std::string insert(std::string insert);
		std::string erase(std::string erase);
		std::string erase(size_t index);
		std::string set(std::string set, size_t index);
		bool get(std::string user);
		std::string get(size_t index);
};

extern Questions questions;
extern QuestionsBlacklist questionsBlacklist;