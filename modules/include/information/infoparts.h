#pragma once

#include <awoken_discord/embed.h>
#include <awoken_discord/server.h>
namespace CIABot {
	class CommandInput;

	// CIABot info

	class InfoPart {
		public:
			InfoPart();
			virtual ~InfoPart();
			virtual int process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class VersionPart : public InfoPart {
		public:
			int process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class CommandsPart : public InfoPart {
		public:
			int process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class TranslationPart : public InfoPart {
		public:
			int process(CommandInput* input, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	// User Info

	class UserInfoPart {
		public:
			UserInfoPart();
			virtual ~UserInfoPart();
			virtual int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class IDPart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class UsernamePart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class DiscriminatorPart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class AvatarPart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class JoinedPart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class CreatedPart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class ProfilePart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class NitroPart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	class BoostingPart : public UserInfoPart {
		public:
			int process(AwokenDiscord::User& user, AwokenDiscord::Server& server, std::string* name, std::string* value, AwokenDiscord::Embed* embed);
	};

	extern std::map<std::string, InfoPart*> infoParts;
	extern std::map<std::string, UserInfoPart*> userInfoParts;
}