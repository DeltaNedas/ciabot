#pragma once
#include <ciabot/commands.h>

using namespace CIABot;

class CommandOffline : public Command {
	public:
		CommandOutput run(CommandInput& input);
		std::string getName();
		CommandOutput getHelp(std::string serverID);
		std::set<std::string> getTypes();
};