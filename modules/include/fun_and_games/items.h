#pragma once
#include <ciabot/userdata.h>

#include <ciabot/util/json.h>

using namespace CIABot;

class Item {
	public:
		Item(std::string id = "", size_t amount = 0, rupees_t price = 0, std::string name = "");

		inline bool empty() {
			return id.empty() || amount == 0;
		}

		std::string id = "", name = ""; // Name only used in shop
		size_t amount = 0;
		rupees_t price = 0; // Price only used in shop
};

void to_json(json& to, const Item& from);

class Items : public JsonData {
	public:
		json serialise();
		int deserialise(json data);
		std::map<std::string, std::map<std::string, std::map<std::string, Item>>> values = {};
		void fallback();
		/*
		{
			"server-id": {
				"shop": {
					"id": {
						"name": "Display Name",
						"price": 0
					}
				},
				"user-id": {
					"id": 1
				}
			}
		}
		*/

		Item add(Item add, std::string user, std::string server);
		Item sub(Item sub, std::string user, std::string server);
		Item set(Item set, std::string user, std::string server);
		std::map<std::string, Item> set(std::map<std::string, Item> set, std::string user, std::string server);
		std::map<std::string, std::map<std::string, Item>> set(std::map<std::string, std::map<std::string, Item>> set, std::string server);
		Item get(std::string id, std::string user, std::string server);
		std::map<std::string, Item> get(std::string user, std::string server);
		std::map<std::string, std::map<std::string, Item>> get(std::string server);
};

extern Items items;