# Modules tutorial
## Info
Modules are a way to extend CIABot with C++ *(or, in the future, C, Lua, etc.)* code.
Some core commands like `$help`, `$permissions` and `$modules` are built in to CIABot, others may come in the form of modules.

They are linked as a shared library in the format `MODULENAME.so`.
A module **must** contain this function:
> const char* MODULENAME_getTitle()

Modules may contain the following optional functions:
> int MODULENAME_load()
> int MODULENAME_unload()

All of the above functions must be defined in `extern "C" { /*...*/ }"` as to not have C++ ABI break it.

Place the compiled library in `build/modules` (make it if it doesnt exist)
Enable modules are stored in `config/modules.json`.
Make sure to keep it writable to a CIABot-specific user only!

## Safety
MODULENAME_load() will only be called by CIABot after all the core data has been loaded.
* `enabledModules[MODULENAME]` will contain a pointer to the created Module object.
* The module object will contain a handle pointer from `dlopen()` in `m->handle`.
MODULENAME_unload() will only be called by CIABot before any core data is unloaded.
* `dlclose(m->handle)` is called after `MODULENAME_unload()`, but only before shutdown.
Modules are loaded in alphabetical order.

Core Data will always be available when modules load.
`dlclose()` is only called for the module's handler before CIABot shuts down.
Keep in mind that modules may be loaded at any point *(with $modules load name)* between loading and saving core data.

## Security
Modules are loaded by default which can be a security concern.
You can use `make NEW_MODULES_ENABLED=0` or edit your options.h and it will eliminate the risk of malicious code being automatically executed.

## Example module
You can try out the example module, [Beep](src/beep.cpp).
Test it with these commands:
`$ make build/beep.so`
`$ make install`
`$ ciabot/build/ciabot -i 259` *(259 - show all module-related messages)*
