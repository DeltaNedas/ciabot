#pragma once

#include <awoken_discord/channel.h>
#include <awoken_discord/message.h>
#include <awoken_discord/server.h>
#include <awoken_discord/user.h>

#include <utility> // std::pair

#include <info.h>
#include <threads.h>
#include <util.h>
#include <util/discord.h>

namespace CIABot {
	template <typename T>
	bool updateCache(T* value) {
		fprintf(stderr, "[%sWARN%s] Cache updated for unknown type %s%s%s!\n",
			Colours::YELLOW, Colours::RESET,
			Colours::BOLD, typeName(*value).c_str(), Colours::RESET);
		fprintf(stderr, "[%sWARN%s] Stack trace:\n%s\n",
			Colours::YELLOW, Colours::RESET,
			getBacktrace().c_str());
		return false;
	}

	bool updateCache(AwokenDiscord::Channel* value);
	bool updateCache(AwokenDiscord::Message* value);
	bool updateCache(AwokenDiscord::Server* value);
	bool updateCache(ServerMember* value);
	bool updateCache(AwokenDiscord::User* value);
	// Use this class to reduce network calls when necessary.
	template <typename T>
	class Cached {
		public:
			Cached() {}
			Cached(T value) {
				Cached(value, timestamp());
			}
			Cached(T value, size_t time) {
				this->value = value;
				this->time = time;
			}

			operator T&() {
				return this->get();
			}
			T& operator*() {
				return this->get();
			}

			T& get(size_t tolerance = DEFAULT_CACHE_LIFETIME) {
				size_t now = timestamp();
				size_t age = now - this->time;
				if (age > tolerance) {
					updateCache(&this->value);
					this->time = now;
					printInfo(InfoLevel::CACHE, "[%sCACHE%s] Updating cached %s%s%s took %s.\n",
						Colours::CYAN, Colours::RESET,
						Colours::BOLD, typeName(this->value).c_str(), Colours::RESET,
						timeToDate(timestamp() - now).c_str());
				}
				return this->value; // Return old value if updating failed
			}

			void update(T value) {
				this->value = value;
				this->time = timestamp();
			}

			T value;
			size_t time = 0;
	};
}