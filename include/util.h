#pragma once
#include <awoken_discord/channel.h>
#include <awoken_discord/server.h>
#include <awoken_discord/snowflake.h>
#include <awoken_discord/user.h>

#include <cxxabi.h>
#include <string>
#include <thread>
#include <typeinfo>
#include <utility>
#include <vector>

#include <colours.h>

namespace CIABot {
	int execute(std::string* output, std::string command);
	void sleep(int milliseconds);

#ifdef linux // Symlinks on BSD only but idk about includes and stuff
	int resolvePath(std::string path, std::string* resolved);
#endif
	int readFile(FILE* file, std::string* output);
	int readFile(std::string path, std::string* output);
	int writeFile(FILE* file, std::string data);
	int writeFile(std::string path, std::string data, bool append = false);

	size_t timestamp();
	std::string timestampStr(std::string format = "%Y/%m/%d - %I:%M:%S %p", size_t timestamp = 0, size_t length = 64);
	size_t dateToUnix(std::string date);
	std::string timeToDate(size_t time, size_t place = 3);

	void deleteThread(std::thread* pointer);

	// helper exists so you can do typeName(var) and not just typeName<MyType>();
	// No idea why i can set it to nullptr.
	template <typename T>
	std::string typeName(T& helper = nullptr) {
		int status = 0;
		char* buffer = abi::__cxa_demangle(typeid(T).name(), 0, 0, &status);
		switch (status) {
			case 0:
				break;
			case -1:
				fprintf(stderr, "[%sERROR%s] Failed to allocate enough memory to get type name!\n",
					Colours::RED, Colours::RESET);
			case -2:
				// Should be impossible?
				fprintf(stderr, "[%sERROR%s] Invalid type name?!?!\n",
					Colours::RED, Colours::RESET);
			case -3:
				// Let's hope its same as above
				fprintf(stderr, "[%sERROR%s] Invalid ABI demangle argument!\n",
					Colours::RED, Colours::RESET);
			default:
				return "";
		}

		std::string ret(buffer);
		free(buffer);
		return ret;
	}
}