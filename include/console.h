#pragma once

#include <string>

#include <parser.h>

namespace CIABot {
	class Console {
	public:
		Console(std::string status);

		bool start();
		bool stop();
		bool parse(std::string input);

		std::string status = "";
		bool closed = true; // Controlled by running
		bool running = false;
		AwokenDiscord::Channel channel;
		AwokenDiscord::Server server;
		AwokenDiscord::User user;
		UserData userData;
		bool canSay = true;
	};

	void consoleLoop(Console* console);

	extern Console* console;
}