#pragma once

namespace CIABot {
	namespace InfoLevel {
		const unsigned short NONE = 0;
		const unsigned short LOAD = 1 << 0;
		const unsigned short SAVE = 1 << 1;
		const unsigned short DATA_MODIFIED = 1 << 2;
		const unsigned short START = 1 << 3;
		const unsigned short STOP = 1 << 4;
		const unsigned short MESSAGE = 1 << 5;
		const unsigned short TIME = 1 << 6;
		const unsigned short COMMAND = 1 << 7;
		const unsigned short MODULE = 1 << 8;
		const unsigned short FEED = 1 << 9;
		const unsigned short QUESTION = 1 << 10;
		const unsigned short FILTER = 1 << 11;
		const unsigned short CACHE = 1 << 12;
		const unsigned short DISCORD = 1 << 13;
	}

	void printInfo(unsigned short flag, const char* format, ...);
	extern unsigned short infoLevel;
}
