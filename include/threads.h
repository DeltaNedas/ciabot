#pragma once

#include <thread>
#include <vector>

namespace CIABot {
	void quit(int code = 0, bool save = true);
	void handleSignal(int signo);
	void handleError(int signo); // For segfaults, etc.
	std::string getBacktrace(size_t limit = 10);

	extern std::thread* consoleThread;
	extern std::thread* clientThread;
	extern std::thread* tasksThread;

	extern std::vector<std::thread*> auxiliaryThreads;
}
