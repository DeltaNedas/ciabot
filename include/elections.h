#pragma once

#include <map>
#include <string>
#include <vector>

#include <awoken_discord/emoji.h>

namespace CIABot {
	class ElectionCandidate {
		public:
			ElectionCandidate() {}
			ElectionCandidate(std::string name, std::string emoji, size_t votes);

			std::string name = "", emoji = "";
			size_t votes = 0;
	};

	class Election {
		public:
			Election() {}
			Election(std::string channel, std::string message, std::string name,
				std::string title, size_t endsAfter, size_t started, size_t ended,
				std::vector<std::string> voters, std::map<std::string, ElectionCandidate> candidates);

			bool canVote();
			bool finish(bool postResults = true);
			bool addReactions();
			std::string getWinners();

			std::string channel = "", message = "", name = "", title = "";
			size_t endsAfter = 0, started = 0, ended = 0;
			std::vector<std::string> voters = {};
			std::map<std::string, ElectionCandidate> candidates = {};
	};


	std::string emojiToString(std::string id);
	std::string emojiToString(AwokenDiscord::Emoji emoji);
}