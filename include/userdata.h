#pragma once
#include <awoken_discord/channel.h>
#include <awoken_discord/server.h>
#include <awoken_discord/snowflake.h>
#include <awoken_discord/user.h>

#include <map>
#include <errno.h>
#include <stdio.h>

#include <client.h>
#include <colours.h>
#include <threads.h>
#include <types.h>
#include <options.h>
#include <util/json.h> // To make src/jsondata/*.cpp easier

namespace CIABot {
	class UserData {
		public:
			UserData() {}
			UserData(std::string user, std::string message, std::string channel, std::string server, bool fake = true);
			UserData(AwokenDiscord::Message* message, bool fake = false);

			std::string user = "";
			std::string message = "";
			std::string channel = "";
			std::string server = "";
			bool fake = true;
			bool dm = true;

			execution_level_t executionLevel = DEFAULT_EXECUTION_LEVEL;
			rupees_t rupees = DEFAULT_RUPEES;
	};


	class JsonData {
		public:
			JsonData();
			~JsonData();
			virtual json serialise();
			virtual int deserialise(json data);
			virtual void fallback();
	};

	class Tokens : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			void fallback();
			std::map<std::string, std::string> values = {};
			/*
			{
				"name": "token"
			}
			*/

			std::string set(std::string set, std::string name);
			std::string get(std::string name);
	};

	class Prefixes : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			std::map<std::string, std::string> values = {};
			void fallback();
			/*
			{
				"server_id": "prefix"
			}
			*/

			std::string set(std::string set, std::string server = "global");
			std::string get(std::string server = "global", bool serverOnly = false);
	};

	class Rupees : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			std::map<std::string, std::map<std::string, rupees_t>> values = {};
			void fallback();
			/*
			{
				"server": {
					"user": 0.0 (rupee balance)
				}
			}
			*/

			rupees_t add(rupees_t add, std::string user, std::string server);
			rupees_t sub(rupees_t sub, std::string user, std::string server);
			rupees_t set(rupees_t set, std::string user, std::string server);
			std::map<std::string, rupees_t> get(std::string server);
			rupees_t get(std::string user, std::string server);
			rupees_t get(std::string user, std::map<std::string, rupees_t> users);
	};

	class ExecutionLevels : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			void fallback();
			std::map<std::string, std::map<std::string, execution_level_t>> values = {};
			/*
			{
				"server_id": {
					"user_id": -1 (execution level)
				}
			}
			*/

			execution_level_t set(execution_level_t set, std::string user, std::string server = "global");
			std::map<std::string, execution_level_t> set(std::map<std::string, execution_level_t> set, std::string server = "global");
			execution_level_t get(std::string user, std::string server = "global", bool serverOnly = false);
			std::map<std::string, execution_level_t> get(std::string server = "global", bool serverOnly = false);
	};

	class Filter;
	class Filters : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			void fallback();
			std::map<std::string, std::map<std::string, Filter*>> values = {};
			/*
			{
				"server_id": {
					"filter_name": {
						"flags": {
							"bot": false,
							"user": true,
							"delete": false,
							"kick": false,
							"ban": false
						},
						"bonuses": {
							"rupees": 0.0 (rupees added to balance)
						},
						"input": "/custom regex (.+)/g",
						"output": "custom output {1}",
						"channel": "forced_output_channel_id"
					}
				}
			}
			*/

			bool remove(std::string name, std::string server = "global");
			Filter* set(Filter* set, std::string name, std::string server = "global");
			std::map<std::string, Filter*> set(std::map<std::string, Filter*> set, std::string server = "global");
			Filter* get(std::string name, std::string server);
			std::map<std::string, Filter*> get(std::string server = "global", bool serverOnly = false);
			// Filters are merged with global filters UNLESS:
			// > serverOnly is true
			// OR
			// > server is "global" (prevent infinite loop)
			// If merged, global filters will take priority over server-specific ones.
		protected:
			void deserialiseFlag(json jsonFlags, std::string name, unsigned char* flags, unsigned char flag, bool fallback = false);
			std::string currentFlag = "";
	};

	class Permission;
	class Permissions : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			void fallback();
			std::map<std::string, std::map<std::string, Permission*>> values = {};
			/*
			{
				"server_id": {
					"command.run": {
						"execution_level": -1, (-1 = anyone can run)
						"rupees": 0,
						"rupees_cost": 0,
						"created": 0, (minimum time since account creation, in seconds)
						"joined": 0 (minimum time since server join, in seconds),
						"blacklists": {
							"users": [
								"user_id" (will never have permission, checked before whitelist)
							]
						},
						"whitelists": {
							"users": [
								"user_id" (ignores other requirements and pays no rupees)
							]
						}
					}
				}
			}
			*/

			Permission* set(Permission* set, std::string name, std::string server = "global");
			std::map<std::string, Permission*> set(std::map<std::string, Permission*> set, std::string server = "global");
			Permission* get(std::string name, std::string server = "global", bool serverOnly = false);
			Permission* getDefault(std::string name);
			std::map<std::string, Permission*> get(std::string server = "global", bool serverOnly = false);

			bool has(std::string name, UserData userData, std::string server = "", bool apply = true);
			bool reset(std::string name, std::string server = "global");
	};

	class Message;
	class Messages : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			void fallback();
			std::map<std::string, std::map<std::string, Message*>> values = {};
			/*
			{
				"channel_id": {
					"message_id": {
						"history": [
							{
								"attachments": [
									"URL"
								],
								"content": "first version",
								"timestamp": 64 (64 bit timestamp)
							}
						],
						"responses": [
							"response_message_id"
						],
						"user": "user_id",
						"channel": "channel_id",
						"server": "server_id"
					}
				}
			}
			*/

			// Empty channel to search for message over all channels
			Message* set(Message* set, std::string message, std::string channel);
			Message* set(AwokenDiscord::Message set, std::string creator = "");
			std::map<std::string, Message*> set(std::map<std::string, Message*> set, std::string channel);
			Message* get(std::string message, std::string channel);
			Message* get(std::string message, std::map<std::string, Message*> messages);
			std::map<std::string, Message*> get(std::string channel);
	};

	class Modules : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			void fallback();
			std::map<std::string, bool> values = {};
			/*
			{
				"name": true/false
			}
			*/

			bool set(bool enabled, std::string name);
			bool get(std::string name);
			bool wereNewModulesFound();
		protected:
			bool newModulesFound = false;
	};

	class Election;
	class Elections : public JsonData {
		public:
			json serialise();
			int deserialise(json data);
			void fallback();
			std::map<std::string, std::map<std::string, Election*>> values = {};
			/*
			{
				"server_id": {
					"election_name": {
						"title": "Decide who does what",
						"ends": nanosecond_timestamp,
						"started": 0,
						"ended": 0,
						"channel": "",
						"message": "",
						"voters": [
							"user_id"
						],
						"candidates": {
							"emoji_id": {
								"name": "candidate name",
								"votes": 1
							}
						}
					}
				}
			}
			*/

			Election* set(Election* set, std::string server);
			std::map<std::string, Election*> set(std::map<std::string, Election*> set, std::string server);
			Election* remove(std::string name, std::string server);
			Election* get(std::string name, std::string server);
			std::map<std::string, Election*> get(std::string server);
	};

	extern Tokens tokens;
	extern Prefixes prefixes;
	extern ExecutionLevels executionLevels;
	extern Rupees rupees;
	extern Filters filters;
	extern Permissions permissions;
	extern Messages messages;
	extern Modules modules;
	extern Elections elections;

	int readData(std::string filename, JsonData* data, bool silent = false);
	bool loadData();

	int writeData(std::string filename, JsonData* data, bool silent = false);
	bool saveData();
}