#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

#include <options.h>

// Just a heads up, you don't need to touch this to make a module!
// Internally used by CIABot.

namespace CIABot {
	typedef int(*MODULE_INT)();
	typedef const char*(*MODULE_STRING)();

	class Module {
		public:
			std::string getName(); // Automatic
			std::string getTitle(); // Name and version e.g. Fun & Games v1.0.0

			// Wrappers for functions that may not always exist.
			int load();
			int unload();
			int reload();

			std::map<std::string, MODULE_STRING> stringFuncs = {};
			std::map<std::string, MODULE_INT> intFuncs = {};

			std::string name = "unknown";
			void* handle;
		protected:
			std::string stringCall(std::string type);
			int intCall(std::string type);

			std::set<std::string> registeredCommands = {};
	};

	extern std::map<std::string, Module*> loadedModules;

	int loadModule(std::string name);
	int loadModules();
	int unloadModule(std::string name);
	int unloadModules();
}
