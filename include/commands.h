#pragma once
#include <awoken_discord/embed.h>
#include <awoken_discord/message.h>

#include <set>
#include <string>
#include <vector>

#include <info.h>
#include <options.h>
#include <userdata.h>
#include <util.h>
#include <util/discord.h>

namespace CIABot {
	// Avoid include loop
	namespace FilterFlags {
		const unsigned char NONE = 0;
		const unsigned char BOT = 1 << 0;
		const unsigned char USER = 1 << 1;
		const unsigned char DELETE = 1 << 2;
		const unsigned char KICK = 1 << 3;
		const unsigned char BAN = 1 << 4;
		const unsigned char FORCE_ASCII = 1 << 5;
		const unsigned char FORCE_LOWER = 1 << 6;
	}

	class CommandInput {
		public:
			CommandInput() {}
			CommandInput(std::vector<std::string> args, AwokenDiscord::Message message, UserData userData, std::string raw);

			std::vector<std::string> args = {};
			AwokenDiscord::Message message;
			UserData userData;
			std::string raw = "";
	};
	class CommandOutput {
		public:
			CommandOutput() {}
			CommandOutput(
				std::string content,
				std::string data = "",
				AwokenDiscord::Embed embed = AwokenDiscord::Embed::Flag::INVALID_EMBED,
				bool tts = false);

			std::string content = "";
			std::string data = ""; // Used in $(sub-commands) for their output
			AwokenDiscord::Embed embed = AwokenDiscord::Embed::Flag::INVALID_EMBED;
			bool tts = false;
	};
	void to_json(json& to, const CommandOutput& from);
	void from_json(const json& from, CommandOutput& to);

	class Command {
		public:
			Command() {}
			virtual ~Command() {}

			virtual CommandOutput run(CommandInput& input);
			virtual bool canRun(CommandInput& input);
			virtual std::string getName();
			virtual std::string getUsage(); // One-liner for $help (category)
			virtual CommandOutput getHelp(std::string serverID); // Detailed info on a command, used in $help command
			virtual std::set<std::string> getTypes(); // Used for $help category listing

			// Can be used by commands to easily check permissions
			// Only set apply to true after any error checking
			virtual bool can(CommandInput& input, std::string action = "run", bool apply = false);
			virtual size_t getCooldown(CommandInput& input, std::string action = "run");
			virtual void setCooldown(CommandInput& input, std::string action = "run");
			virtual CommandOutput error(std::string error = "An unknown error occurred!");
			static CommandOutput error(std::string name, std::string error);
			virtual CommandOutput permissionError(std::string action = "run");
			virtual CommandOutput cooldownError(size_t wait);
			virtual std::string messageRing(std::string server = "global", std::string action = "run", std::string format = "\nAn execution level of **Ring-__{RING}__** is required.", std::string none = "");
			static std::string messageRing(execution_level_t level, std::string format = "An execution level of **Ring-__{RING}__** is required.", std::string none = "");

			// Utility functions for commands
			static void parseFlag(unsigned char* flags, unsigned char flag, std::vector<std::string> args, size_t index, bool fallback = false);
			static void parseBool(bool* value, std::vector<std::string> args, size_t index, bool fallback = false);
			static void parseDouble(double* value, std::vector<std::string> args, size_t index, double fallback = 0);
			static void parseLong(long* value, std::vector<std::string> args, size_t index, long fallback = 0);
			static void parseString(std::string* value, std::vector<std::string> args, size_t index, std::string fallback = "");
			static void parseRing(execution_level_t* value, std::vector<std::string> args, size_t index, execution_level_t fallback = DEFAULT_EXECUTION_LEVEL);
			static void parseFilterName(std::string* value, std::vector<std::string> args, size_t index);
			static void parseEmoji(std::string* value, std::vector<std::string> args, size_t index, std::string fallback = "");

			static void parseSnowflake(std::string* value, std::vector<std::string> args, size_t index, std::string fallback = "-");
			static void parseChannel(std::string* value, std::vector<std::string> args, size_t index, std::string fallback = "-");
			static void parseChannels(std::vector<std::string>* value, std::vector<std::string> args, size_t index, std::vector<std::string> fallback = {}, std::string channelFallback = "-");
			static void parseUser(std::string* value, std::vector<std::string> args, size_t index, std::string fallback = "-");
			static void parseUsers(std::vector<std::string>* value, std::vector<std::string> args, size_t index, std::vector<std::string> fallback = {}, std::string userFallback = "-");

			static void parseTime(size_t* seconds, std::vector<std::string> args, size_t index, size_t fallback = 0);
			static void addTime(size_t* ptr, size_t seconds, std::string match, std::string regex);
	};

	extern std::map<std::string, Command*> commands;

	template <class C>
	void loadCommand() {
		C* command = new C();
		commands[command->getName()] = command;
	}
	void unloadCommand(std::string name);

	void loadCommands();
	void unloadCommands();
}
