#pragma once
#include <awoken_discord/channel.h>
#include <awoken_discord/message.h>
#include <awoken_discord/user.h>

#include <string>
#include <vector>

#include <commands.h>

namespace CIABot {
	void runCommand(size_t index, std::string commandName, Command* command, CommandInput input, UserData userData, bool* after = NULL, std::string* data = NULL);

	int parseCommand(
		std::string content,
		UserData userData,
		AwokenDiscord::Message message = AwokenDiscord::Message(),
		bool* after = NULL,
		std::string* data = NULL);

	int parseCommand(AwokenDiscord::Message message, UserData userData, bool* after = NULL);
	int parseCommand(AwokenDiscord::Message message, bool* after = NULL);
}