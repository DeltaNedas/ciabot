#pragma once

#include <functional>
#include <list>
#include <map>
#include <vector>

namespace CIABot {
	typedef std::function<void(std::string)> EVENT_CALLBACK; // type
	typedef std::function<void(size_t)> TASK_CALLBACK; // delta

	class Events {
	public:
		void on(std::string type, EVENT_CALLBACK callback);
		void fire(std::string type);

		std::map<std::string, std::vector<EVENT_CALLBACK>> types = {};
	};

	class Task {
	public:
		Task(TASK_CALLBACK callback, size_t time, bool remove = true);

		TASK_CALLBACK callback = nullptr;
		size_t time = 0;
		bool remove = true;
		bool toRemove = false;
	};

	class Tasks {
	public:
		bool start();
		bool stop();
		void process();
		size_t add(Task task);
		size_t add(TASK_CALLBACK callback, size_t after);
		void remove(size_t index);

		bool closed = true; // Controlled by running
		bool running = false;

		std::vector<Task> tasks = {};
	};

	void tasksLoop(Tasks* tasks);

	extern Events* events;
	extern Tasks* tasks;
}