#pragma once

#include <string>
#include <vector>

namespace CIABot {
	class MessageHistory {
		public:
			MessageHistory(std::string content = "", size_t timestamp = 0, std::vector<std::string> attachments = {});
			std::string content;
			size_t timestamp;
			std::vector<std::string> attachments;
	};

	class Message {
		public:
			Message(std::string id, std::string channel, std::string server, std::string user,
				std::string creator, std::vector<std::string> responses, std::vector<MessageHistory*> history);
			~Message();

			std::string id, channel, server, user, creator;
			std::vector<MessageHistory*> history;
			std::vector<std::string> responses;
	};
}