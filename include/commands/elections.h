#pragma once
#include <commands.h>

namespace CIABot {
	class CommandElections : public Command {
		public:
			CommandOutput run(CommandInput& input);
			std::string getName();
			std::string getUsage();
			CommandOutput getHelp(std::string serverID);
			std::set<std::string> getTypes();
	};
}