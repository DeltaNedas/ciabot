#pragma once

#include <stddef.h>

namespace CIABot {
	void checkFeeds();
	void feedsLoop(size_t delta);
}