#pragma once

#include <string>
#include <regex>

#include <commands.h>

namespace CIABot {
	class Filter {
		public:
			Filter(std::string input, std::string output, unsigned char flags, std::string channel = "", std::string urlInput = "", std::string urlOutput = "");
			bool process(const AwokenDiscord::Message& message, UserData userData);
			std::string replaceOutput(std::string raw, std::smatch matched, const AwokenDiscord::User& user, std::string server, rupees_t rupees);
			std::string replaceInput(std::string input);

			std::string input = "";
			std::regex regex; // Generated with ^
			std::string output = "";
			unsigned char flags = FilterFlags::NONE;
			std::string channel = ""; // Send in current channel - empty
			std::string urlInput = "";
			std::string urlOutput = "";
			std::regex urlRegex;

			std::string name = ""; // For permissions, a-Z|0-9|_
	};
}