#pragma once

#include <string>

namespace CIABot {
	std::string compress(const std::string& data);
	std::string decompress(const std::string& data);
}