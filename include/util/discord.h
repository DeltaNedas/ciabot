#pragma once

#include <awoken_discord/channel.h>
#include <awoken_discord/embed.h>
#include <awoken_discord/message.h>
#include <awoken_discord/server.h>
#include <awoken_discord/user.h>

#include <string>

#include <colours.h>
#include <options.h>
#include <util/json.h>

namespace CIABot {
	class CommandOutput;
	using AwokenDiscord::CreateMessageParams;

	typedef struct {
		std::string server;
		AwokenDiscord::ServerMember member;
	} ServerMember;


	// Command helper functions TODO: Merge into Command class
	CommandOutput helperHelp(std::string usage, std::string info);
	CommandOutput helperOutput(std::string title, std::string text = "", std::string data = "", unsigned int colour = Colours::Discord::DELTA);

	// Try/Catch client->...
	// Also try to pull from cache if it exists
	bool getChannel(std::string id, AwokenDiscord::Channel* channel, bool silent = false, size_t cached = DEFAULT_CACHE_LIFETIME);
	bool getDirectMessages(std::string id, AwokenDiscord::Channel* channel, bool silent = false,  size_t cached = DEFAULT_CACHE_LIFETIME);
	bool getServer(std::string id, AwokenDiscord::Server* server, bool silent = false,  size_t cached = DEFAULT_CACHE_LIFETIME);
	bool getUser(std::string id, AwokenDiscord::User* user, bool silent = false,  size_t cached = DEFAULT_CACHE_LIFETIME);
	bool getMessage(std::string id, std::string channel, AwokenDiscord::Message* message, bool silent = false,  size_t cached = DEFAULT_CACHE_LIFETIME);
	bool getMember(std::string id, std::string server, AwokenDiscord::ServerMember* member, bool silent = false,  size_t cached = DEFAULT_CACHE_LIFETIME);

	std::string sendMessage(CreateMessageParams& params, const std::string& creator = "");
	std::string sendMessage(std::string id, const json& message, const std::string& creator = "");
	std::string sendMessage(std::string id, const std::string& message, const std::string& creator = "");

	bool editMessage(std::string id, CreateMessageParams& params, AwokenDiscord::Message* ptr = nullptr);
	bool editMessage(std::string id, std::string channel, const json& message, AwokenDiscord::Message* ptr = nullptr);
	bool editMessage(std::string id, std::string channel, const std::string& message, AwokenDiscord::Message* ptr = nullptr);

	bool deleteMessage(std::string id, std::string channel);

	bool updateStatus();
	bool updateStatus(std::string message);

	void addField(std::vector<AwokenDiscord::EmbedField>& fields, std::string name, std::string value, bool inLine = false);

	// Get timestamp of an id
	size_t createdAt(std::string snowflake);

	json serialiseMessage(const AwokenDiscord::Message& message); // Turn Message into CreateMessageParams and serialise.
}