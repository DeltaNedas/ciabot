#pragma once

#include <regex>
#include <string>
#include <vector>

namespace CIABot {
	bool compare(std::string base, std::vector<std::string> compare);
	std::vector<std::string> split(std::string string, std::string seperator = " ");
	std::string concat(std::vector<std::string> strings, std::string by = "");
	std::string escape(std::string unsafe);
	std::string clamp(std::string string, size_t len = 64);

	std::string replace(std::string string, std::string match, std::string replacement); // Replace in a single string
	std::string replace(std::string string, std::vector<std::pair<std::string, std::string>> matches);
	std::vector<std::string> replace(std::vector<std::string> strings, std::string match, std::string replacement); // Replace in multiple strings
	std::vector<std::string> replace(std::vector<std::string> strings, std::vector<std::pair<std::string, std::string>> matches);
	std::string match(std::string string, std::string pattern, std::string fallback = "", std::smatch* matched = nullptr);
	// Returns last match of a pattern in a string.
	// If not found then returns fallback

	std::string toBinary(size_t data);
	std::string upper(std::string string);
	std::string lower(std::string string);
	std::string count(size_t count, std::string thing, std::string format = "{COUNT} {THING}s", std::string single = "1 {THING}");
	std::string toAscii(std::string string);
}