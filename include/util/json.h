#pragma once
#include <awoken_discord/json_wrapper.h>

#include <map>
#include <string>
#include <vector>

namespace CIABot {
	using AwokenDiscord::json;
	using AwokenDiscord::addJsonField;
	using AwokenDiscord::getJsonField;
	using AwokenDiscord::REQUIRED;
	using AwokenDiscord::OPTIONAL;
	using AwokenDiscord::OPTIONAL_NULLABLE;
	using AwokenDiscord::NULLABLE;

	json emptyObject(); // I need to add this stuff because `json data = {}` will dump to "null" and make life painful.
	json emptyArray();
}