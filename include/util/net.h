#pragma once

#include <awoken_discord/client.h>

namespace CIABot {
	using AwokenDiscord::Response;
	typedef AwokenDiscord::RequestMethod Method;

	int downloadFile(std::string url, std::string* output = nullptr, Method m = AwokenDiscord::Get, Response* ptr = nullptr);
}