#pragma once

#include <algorithm>
#include <map>
#include <set>
#include <vector>

namespace CIABot {
	template <typename T>
	bool contains(T value, std::vector<T> array) {
		for (T object : array) {
			if (value == object) {
				return true;
			}
		}
		return false;
	}

	template <typename Key, typename Value>
	Value get(std::map<Key, Value>& map, Key& key, Value fallback = Value()) {
		auto it = map.find(key);
		if (it == map.end()) {
			return fallback;
		}
		return it->second;
	}

	template <typename T>
	std::vector<T> remove(std::vector<T> array, T value) {
		array.erase(std::remove(array.begin(), array.end(), value), array.end());
		return array;
	}

	template <typename T, class Sorter>
	std::vector<T> insertSorted(std::vector<T> sorted, T value, Sorter sorter) {
		auto it = std::upper_bound(sorted.begin(), sorted.end(), value, sorter);
		sorted.insert(it, value);
		return sorted;
	}
	template <typename T>
	std::vector<T> insertSorted(std::vector<T> sorted, T value) {
		auto it = std::upper_bound(sorted.begin(), sorted.end(), value);
		sorted.insert(it, value);
		return sorted;
	}

	template <typename T>
	std::vector<T> combine(std::vector<T> v1, std::vector<T> v2) {
		for (T object : v2) {
			v1.push_back(object);
		}
		return v1;
	}

	template <typename K, typename V>
	std::map<K, V> combine(std::map<K, V> base, std::map<K, V> replace) {
		for (auto& it : replace) {
			base[it.first] = it.second;
		}
		return base;
	}

	// Returns keys of values from m1 different or missing in m2
	template <typename Key, typename Value>
	std::set<Key> difference(std::map<Key, Value> m1, std::map<Key, Value> m2) {
		std::set<Key> ret = {};
		for (auto it : m1) {
			Key key = it.first;
			auto it2 = m2.find(key);
			if (it2 == m2.end()) {
				ret.insert(key);
			} else if (it2->second != it.second) {
				ret.insert(key);
			}
		}
		return ret;
	}

	template <typename Key, typename Value>
	std::vector<Key> keys(std::map<Key, Value> map) {
		std::vector<Key> ret = {};
		for (auto it : map) {
			ret.push_back(it.first);
		}
		return ret;
	}

	template <typename Key, typename Value>
	std::vector<Value> values(std::map<Key, Value> map) {
		std::vector<Value> ret = {};
		for (auto it : map) {
			ret.insert(it.second);
		}
		return ret;
	}

	// Remove trailing 0s and/or decimal.
	template <typename T>
	std::string toString(T t) {
		std::string ret = std::to_string(t);
		while (ret.find(".") != -1
			&& ret[ret.size() - 1] == '0'
			|| ret[ret.size() - 1] == '.') {
			ret.pop_back();
		}
		return ret;
	}
}