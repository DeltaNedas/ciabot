#pragma once

#include <map>
#include <string>

namespace CIABot {
	typedef std::string BUILD;
	typedef std::map<std::string, BUILD> MINOR;
	typedef std::map<std::string, MINOR> MAJOR;
	typedef std::map<std::string, MAJOR> CHANGELOG;

	extern CHANGELOG changelog;
	extern std::string version;
}