#pragma once
#include <awoken_discord/awoken_discord.h>
#include <awoken_discord/gateway.h>

#include <map>
#include <vector>

#include <cache.h>

namespace CIABot {
	class Client : public AwokenDiscord::DiscordClient {
		public:
			Client() {} // For offline mode
			using AwokenDiscord::DiscordClient::DiscordClient; // Proper constructor

			// Overrides
			void onMessage(AwokenDiscord::Message message);
			void onEditMessage(AwokenDiscord::MessageRevisions revisions);
			void onReady(AwokenDiscord::Ready readyData);
			void onServer(AwokenDiscord::Server server);
			void onChannel(AwokenDiscord::Channel channel);
			void onMember(AwokenDiscord::Snowflake<AwokenDiscord::Server> serverID, AwokenDiscord::ServerMember member);
			void onUser(AwokenDiscord::User user);
			void onRemoveMember(AwokenDiscord::Snowflake<AwokenDiscord::Server> serverID, AwokenDiscord::User user);
			void onDeleteMessages(AwokenDiscord::Snowflake<AwokenDiscord::Channel> channelID,
				std::vector<AwokenDiscord::Snowflake<AwokenDiscord::Message>> messages);
			void onReaction(AwokenDiscord::Snowflake<AwokenDiscord::User> userID, AwokenDiscord::Snowflake<AwokenDiscord::Channel> channelID,
				AwokenDiscord::Snowflake<AwokenDiscord::Message> messageID, AwokenDiscord::Emoji emoji);

			// Return average heartbeat ping in nanoseconds.
			size_t ping(size_t times = 1);

			std::map<std::string, Cached<AwokenDiscord::Server>> servers = {};
			std::map<std::string, Cached<AwokenDiscord::Channel>> channels = {};
			std::map<std::string, Cached<AwokenDiscord::Channel>> dms = {}; // Map user -> channel
			std::map<std::string, std::map<std::string, Cached<CIABot::ServerMember>>> members = {};
			std::map<std::string, std::vector<Cached<CIABot::ServerMember>*>> sortedMembers = {}; // Pointers to ones in map
			std::map<std::string, Cached<AwokenDiscord::User>> users = {};
			static size_t startedAt;
			bool offline = true;
	};

	extern Client* client;
	extern std::string rootPath;

	void startClient(bool createConsole = true, bool offline = false);
}
