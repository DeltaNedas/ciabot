#pragma once

#include <map>
#include <vector>
#include <stddef.h>

#include <types.h>
#include <options.h>

namespace CIABot {
	class Permission {
		public:
			Permission(execution_level_t executionLevel = DEFAULT_EXECUTION_LEVEL,
				rupees_t rupees = 0,
				rupees_t rupeesCost = 0,
				size_t deleteAfter = 0,
				cooldown_t cooldown = DEFAULT_COOLDOWN,
				size_t created = 0,
				size_t joined = 0,
				std::map<std::string, std::vector<std::string>> blacklists = {},
				std::map<std::string, std::vector<std::string>> whitelists = {});
			Permission(const Permission& rhs);
			// Get blacklist or whitelist for users/channels
			std::vector<std::string> getList(bool white, std::string type);
			cooldown_t getCooldown(std::string user);
			void setCooldown(std::string user);

			execution_level_t executionLevel = DEFAULT_EXECUTION_LEVEL;
			rupees_t rupees = 0, rupeesCost = 0;
			cooldown_t cooldown = DEFAULT_COOLDOWN;
			size_t deleteAfter = 0, created = 0, joined = 0;
			std::map<std::string, std::vector<std::string>> blacklists = {}, whitelists = {};

			// Not serialised
			std::map<std::string, cooldown_t> cooldowns = {};
	};

	void loadDefaultPermissions();

	extern std::map<std::string, Permission> defaultPermissions;
}