#pragma once

namespace Colours {
	extern const char* RESET;
	extern const char* BOLD;
	extern const char* LIGHT;
	extern const char* ITALIC;

	extern const char* UNDERLINE;
	extern const char* OVERLINE;
	extern const char* SLOW_BLINK;
	extern const char* FAST_BLINK;

	extern const char* CONCEAL;
	extern const char* REVEAL;
	extern const char* CROSSED;
	extern const char* UNCROSSED;

	extern const char* INVERT;

	extern const char* BLACK;
	extern const char* RED;
	extern const char* GREEN;
	extern const char* YELLOW;
	extern const char* BLUE;
	extern const char* MAGENTA;
	extern const char* CYAN;
	extern const char* WHITE;
	extern const char* CUSTOM; // Add "m" yourself
	extern const char* DEFAULT;

	extern const char* BLACK_BG;
	extern const char* RED_BG;
	extern const char* GREEN_BG;
	extern const char* YELLOW_BG;
	extern const char* BLUE_BG;
	extern const char* MAGENTA_BG;
	extern const char* CYAN_BG;
	extern const char* WHITE_BG;
	extern const char* CUSTOM_BG; // Add "m" yourself
	extern const char* DEFAULT_BG;

	namespace Discord {
		extern unsigned int BLACK;
		extern unsigned int BLUE;
		extern unsigned int BROWN;
		extern unsigned int DARK_BLUE;
		extern unsigned int GREEN;
		extern unsigned int PEPE;
		extern unsigned int YELLOW;
		extern unsigned int MAGENTA;
		extern unsigned int CYAN;
		extern unsigned int ORANGE;
		extern unsigned int PINK;
		extern unsigned int WHITE;
		extern unsigned int RED;

		extern unsigned int DELTA;
		extern unsigned int ANTI_DELTA;
	}
}